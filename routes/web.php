<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('/login');
Route::get('/', 'Admin\Auth\LoginController@showLoginForm')->name('/');
Route::post('/loggedin', 'Admin\Auth\LoginController@login');
Route::get('/password/reset', 'Admin\Auth\ForgotPasswordController@otpRequestForm')->name('password.reset');
Route::post('password/email', 'Admin\Auth\ForgotPasswordController@sendOTP')->name('password.email');
Route::get('password/otp', 'Admin\Auth\ForgotPasswordController@otpForm')->name('password.otp');
Route::get('password/otpvarify', 'Admin\Auth\ForgotPasswordController@otpVarification')->name('password.otpvarify');
Route::get('password/edit', 'Admin\Auth\ForgotPasswordController@passworUpdateForm')->name('password.edit');
Route::post('password/update', 'Admin\Auth\ForgotPasswordController@updatePassword')->name('password.update');
Route::get('email/verification/{param}', 'Api\Payment\PaymentController@verifyEmail')->name("email.verification");

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {

	Route::post('logout', 'Auth\LoginController@logout')->name('logout');

	Route::resource('dashboard', 'Dashboard\DashboardController');
	Route::get('dashboard/details/{id}/{param}', 'Dashboard\DashboardController@dashboardRequestDetails')->name('dashboard.details');

	Route::get('password', 'AdminController@password')->name('password');
	Route::post('password/update', 'AdminController@changePassword')->name('password.update');

	Route::get('profile', 'AdminController@profile')->name('profile');
	Route::post('profile/update', 'AdminController@profileUpdate')->name('profile.update');

	//RBAC
	Route::resource('sub-admin', 'RBAC\SubAdminController');
	Route::resource('permission', 'RBAC\PermissionController');
	Route::resource('role', 'RBAC\RoleController');
	Route::get('subadmin/active/{id}', 'RBAC\SubAdminController@changeAdminStatus')->name('subadmin.active');
	//end RBAC

	Route::resource('setting', 'Setting\SettingController');
	Route::get('privacy/policy', 'Setting\SettingController@privacyPolicy')->name('privacy.policy');
	Route::put('privacy-policy/update/{id}', 'Setting\SettingController@privacyPolicyUpdate')->name('privacy-policy.update');
	
	Route::put('settings/surgeprice', 'Setting\SettingController@surgePriceUpdate')->name('settings.surgeprice');
	Route::get('get/surgeprice', 'Setting\SettingController@getSurgePrice')->name('get.surgeprice');

	Route::resource('document', 'Document\DocumentController');
	Route::get('document/active/{id}', 'Document\DocumentController@changeStatus')->name('document.active');

	//lease...
	Route::get('lease/hourly', 'Lease\LeaseController@hourlyLease')->name('lease.hourly');
	Route::get('lease/daily', 'Lease\LeaseController@dailyLease')->name('lease.daily');
	Route::get('lease/longTime', 'Lease\LeaseController@longTimeLease')->name('lease.longTime');
	Route::get('lease/view/{id}/{param}', 'Lease\LeaseController@showLeaseDetails')->name('lease.view');

	//service
	Route::resource('service', 'Service\ServiceTypeController');

	//vehicle
	Route::resource('vehicle', 'Vehicle\VehicleController');
	Route::post('vehicles','Vehicle\VehicleController@getVehicles')->name('dataProcessing');

	//city
	Route::resource('city', 'City\CityController');

	//referral amount
	Route::resource('referral-amount', 'Referral\ReferralController');

	//taxi dispatch
	Route::resource('taxi-dispatch', 'TaxiDispatch\TaxiDispatchController');

	Route::resource('passenger', 'Passenger\PassengerController');
	Route::post('ajax-passenger','Passenger\PassengerController@ajaxPassenger')->name('ajax-passenger');
	Route::get('passengers/search', 'Passenger\PassengerController@passengerSearch')->name('passengers.search');
	Route::get('passenger/activation/{id}', 'Passenger\PassengerController@passengerActivationProcess')->name('passenger.activation');

	Route::get('passengers/remove', 'Passenger\PassengerController@passengerRemoveProcess')->name('passengers.remove');

	
	Route::get('get/vehicle', 'Driver\DriverController@getVehicle')->name('get.vehicle');

	Route::resource('driver', 'Driver\DriverController');
	Route::post('ajax-driver','Driver\DriverController@ajaxDriver')->name('ajax-driver');
	Route::get('drivers/search', 'Driver\DriverController@driverSearch')->name('drivers.search');

	Route::get('drivers/document/{id}', 'Driver\DriverController@listDriverDocument')->name('drivers.document');
	Route::get('drivers/service-type/{id}', 'Driver\DriverController@listDriverServiceType')->name('drivers.service-type');
	Route::get('driver/document/verification/{id}', 'Driver\DriverController@driverDocumentVerification')->name('driver.document.verification');
	Route::get('driver/activation/{id}', 'Driver\DriverController@driverActivationProcess')->name('driver.activation');
	Route::get('drivers/remove', 'Driver\DriverController@driverRemoveProcess')->name('drivers.remove');


	//tracking driver
	Route::get('drivers/tracking/{id}', 'Driver\DriverController@trackingDriver')->name('drivers.tracking');
	Route::get('drivers/tracking/random/{id}', 'Driver\DriverController@trackingDriverRandomly')->name('drivers.tracking.random');

	//review rating
	Route::get('drivers/review-rating/{id}', 'Driver\DriverController@driverRatingReview')->name('drivers.review-rating');
	Route::get('passenger/review-rating/{id}', 'Passenger\PassengerController@passengerRatingReview')->name('passenger.review-rating');

	Route::put('drivers/femalefriendly', 'Driver\DriverController@isFemaleFriendly')->name('drivers.femalefriendly');

	//report
	Route::get('payment/{type}', 'DriveReport\DriveReportController@allRequest')->name('payment');

	Route::post('ajax-all-request', 'DriveReport\DriveReportController@ajaxAllRequest')->name('ajax-all-request');
	Route::get('request/nofound', 'DriveReport\DriveReportController@noServiceFoundRequest')->name('request.nofound');
	Route::post('ajax-noservice-found', 'DriveReport\DriveReportController@ajaxNoServiceFoundRequest')->name('ajax-noservice-found');

	Route::get('request/rejected', 'DriveReport\DriveReportController@rejectedRequest')->name('request.rejected');
	Route::post('ajax-rejected-request', 'DriveReport\DriveReportController@ajaxRejectedRequest')->name('ajax-rejected-request');

	Route::get('request/schedule', 'DriveReport\DriveReportController@scheduleRequest')->name('request.schedule');
	Route::post('ajax-schedule-request', 'DriveReport\DriveReportController@ajaxScheduleRequest')->name('ajax-schedule-request');

	Route::get('request/details/{id}/{param}', 'DriveReport\DriveReportController@requestDetails')->name('request.details');

	//chat support
	Route::get('passenger/chat/support', 'Chat\ChatSupportController@passengerChatSupport')->name('passenger.chat.support');

	Route::post('chat/message/support', 'Chat\ChatSupportController@sendChatForSupport')->name('chat.message.support');

	Route::post('chat/message/support/get', 'Chat\ChatSupportController@getChatForSupport')->name('chat.message.support.get');

	Route::get('driver/chat/support', 'Chat\ChatSupportController@driverChatSupport')->name('driver.chat.support');

	Route::get('driver/get/byid', 'Chat\ChatSupportController@getDriver')->name('driver.get.byid');
	Route::get('passenger/get/byid', 'Chat\ChatSupportController@getPassenger')->name('passenger.get.byid');

	//make model year
	Route::get('get/admin/make', 'Driver\DriverController@getAdminMake')->name("get.admin.make");
	Route::post('get/admin/model', 'Driver\DriverController@getAdminModel')->name("get.admin.model");
	Route::post('get/admin/year', 'Driver\DriverController@getAdminYear')->name("get.admin.year");

	 //passenger trip
	Route::get('passenger/trip/history/{id}', 'Passenger\TripController@tripIndex')->name("passenger.trip.history");
	Route::post('ajax-passenger-trips', 'Passenger\TripController@ajaxPassengerTrips')->name('ajax-passenger-trips');
	Route::get('passenger/trip/history/details/{id}/{param}', 'Passenger\TripController@tripDetails')->name('passenger.trip.history.details');

	 //Driver trip
	Route::get('driver/trip/history/{id}', 'Driver\DriverTripController@tripIndex')->name("driver.trip.history");
	Route::post('ajax-driver-trips', 'Driver\DriverTripController@ajaxDriverTrips')->name('ajax-driver-trips');
	Route::get('driver/trip/history/details/{id}/{param}', 'Driver\DriverTripController@tripDetails')->name('driver.trip.history.details');
	 //transaction
	Route::get('driver/transaction/{id}', 'Transaction\TransactionController@index')->name('driver.transaction');
	Route::post('driver/transaction', 'Transaction\TransactionController@transactionPayout')->name('driver.transaction');
	//get beneficiary
	Route::post('driver/get/beneficiary', 'Transaction\TransactionController@getDriverBeneficiary')->name('driver.get.beneficiary');
	//transfer beneficiary
	Route::post('driver/create/paymentgateway-transfer-beneficiary','Transaction\TransactionController@transferPayoutToBebeficiary')->name('driver.create.paymentgateway-transfer-beneficiary');
	//get flutterwave admin balance
	Route::get('driver/get/paymentgateway-balance','Transaction\TransactionController@getAdminFlutterWaveBalance')->name('driver.get.paymentgateway-balance');

	Route::get('passenger/transaction/{id}', 'Transaction\TransactionController@passengerTransaction')->name('passenger.transaction');
	Route::get('passenger/split/transaction/{id}', 'Transaction\TransactionController@passengerSplitTransaction')->name('passenger.split.transaction');

	Route::get('passenger/transac/list', 'Transaction\TransactionController@passengerTransactionList')->name('passenger.transac.list');

	Route::get('earning/report', 'Transaction\TransactionController@adminEarning')->name('earning.report');

	Route::resource('promocode', 'Promocode\PromocodeController');

	Route::get('promocodes/search', 'Promocode\PromocodeController@promocodeSearch')->name('promocodes.search');

	Route::get('passenger/cancel/report', 'CancelCharge\CancelChargeController@passengerCancelReport')->name('passenger.cancel.report');
	Route::get('driver/cancel/report', 'CancelCharge\CancelChargeController@driverCancelReport')->name('driver.cancel.report');
	//log report
	Route::get('subadmin/log', 'Log\SubAdminLogController@getLogReport')->name('subadmin.log');
	Route::post('subadmin/log/report','Log\SubAdminLogController@getSubadminLog')->name('subadmin.log.report');

	//send push notification
	Route::get('send/push-notification','PushNotification\PushNotificationController@index')->name('send.push-notification');
	Route::post('get/users','PushNotification\PushNotificationController@getUser')->name('get.users');
	Route::post('send/push-notification','PushNotification\PushNotificationController@adminSendPushNotification')->name('send.push-notification');
	
	//Search Payment by date range
	Route::get('payment/search/daterange', 'DriveReport\DriveReportController@searchPaymentReport')->name('payment.search.daterange');
	
	Route::resource('cancel-reason', 'CancelReason\CancelReasonController');

	Route::get('review/driver', 'Review\ReviewController@index')->name('review.driver');

	Route::post('ajax-review','Review\ReviewController@ajaxReview')->name('ajax-review');
	
	Route::get('review/passenger', 'Review\ReviewController@indexPassenger')->name('review.passenger');

	Route::post('ajax-reviewPassenger','Review\ReviewController@ajaxReviewPassenger')->name('ajax-reviewPassenger');

	//report issue
	Route::get('reportIssue/driver/subject/list', 'ReportIssue\ReportController@driverSubjectList')->name('reportIssue.driver.subject.list');
	Route::post('reportIssue/driver/subject/add', 'ReportIssue\ReportController@driverSubjectAdd')->name('reportIssue.driver.subject.add');
	Route::get('reportIssue/driver/subject/edit/{id}', 'ReportIssue\ReportController@driverSubjectEdit')->name('reportIssue.driver.subject.edit');
	Route::put('reportIssue/driver/subject/update/{id}', 'ReportIssue\ReportController@driverSubjectUpdate')->name('reportIssue.driver.subject.update');
 
	Route::get('reportIssue/customer/subject/list', 'ReportIssue\ReportController@customerSubjectList')->name('reportIssue.customer.subject.list');
	Route::post('reportIssue/customer/subject/add', 'ReportIssue\ReportController@customerSubjectAdd')->name('reportIssue.customer.subject.add');
	Route::get('reportIssue/customer/subject/edit/{id}', 'ReportIssue\ReportController@customerSubjectEdit')->name('reportIssue.customer.subject.edit');
	Route::put('reportIssue/customer/subject/update/{id}', 'ReportIssue\ReportController@customerSubjectUpdate')->name('reportIssue.customer.subject.update');

	Route::get('reportIssue/driver/list', 'ReportIssue\ReportController@driverReportIssueList')->name('reportIssue.driver.list');
	Route::get('reportIssue/customer/list', 'ReportIssue\ReportController@customerReportIssueList')->name('reportIssue.customer.list');
	Route::get('reportIssue/details/{id}/{param}', 'ReportIssue\ReportController@reportIssueDetails')->name('reportIssue.details');
	//report issue end
});