<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['ChangeLanguage']], function () {
    Route::post('driver/signup', 'Api\Driver\RegistrationController@register')->name("driver.registration");
    Route::post('driver/signup/otp', 'Api\Driver\RegistrationController@onlyOTP')->name("driver.registration.onlyOTP");
    Route::post('driver/login', 'Api\Driver\AuthController@login')->name("Driver.login");
    Route::post('driver/forget/password', 'Api\Driver\AuthController@forgetPassword')->name("Driver.forgetPassword");
    Route::post('driver/reset/password', 'Api\Driver\AuthController@resetPassword')->name("Driver.resetPassword");
    Route::group(['middleware' => ['auth:api','scope:temporary-customer-service']], function () {
        //Mobile no update and verification
        Route::post('driver/mobile', 'Api\Driver\ProfileController@updateMobileNo')->name("driver.mobile");
        Route::put('driver/mobile/verify', 'Api\Driver\ProfileController@verifyMobileNo')->name("driver.mobileVerify");
        Route::get('driver/mobile/reverify', 'Api\Driver\ProfileController@resetMobileVerificationOtp')->name("driver.mobileReVerify");
    });
    Route::group(['middleware' => ['auth:api','scope:driver-service']], function () {
        //logout
        Route::get('driver/logout', 'Api\Driver\AuthController@logout')->name("driver.logout");

        Route::post('driver/share/admin', 'Api\Driver\ProfileController@shareWithAdmin')->name("driver.share.admin");
        //email update
        Route::post('driver/email', 'Api\Driver\ProfileController@updatEmail')->name("driver.email");
        //profile images update
        Route::post('driver/profile/image', 'Api\Driver\ProfileController@profileImageUpdate')->name("driver.profileImage");
        //profile update  getProfile
        Route::get('driver/profile', 'Api\Driver\ProfileController@getProfile')->name("driver.profile");
        Route::post('driver/profile/update', 'Api\Driver\ProfileController@updateProfile')->name("driver.updateProfile");
        Route::put('driver/on/off/update', 'Api\Driver\ProfileController@onOff')->name("driver.onOff");
        // document update
        Route::get('driver/document/list', 'Api\Driver\DocumentController@list')->name("driver.document.list");
        Route::post('driver/document/upload/{document_id}', 'Api\Driver\DocumentController@uploadDocument')->name("driver.document.upload");
        // Route::get('driver/document/completion/percentage', 'Api\Driver\StatusController@DocumentCompletion')->name("DocumentCompletion");
        // password
        Route::post('driver/change/password', 'Api\Driver\ProfileController@changePassword')->name("driver.changePassword");
        // background api
        Route::post('driver/background/data', 'Api\Background\DriversBackgroundController@get')->name("driver.backgroundDb");
        //request action
        Route::post('driver/request/reject', 'Api\Driver\TripController@reject')->name("driver.request.reject");
        Route::post('driver/trip/control', 'Api\Driver\TripController@tripControl')->name("driver.trip.control");
        //message
        Route::post('driver/on/ride/message', 'Api\Driver\MessageController@sendMessage')->name("driver.trip.sendMessage");
        Route::post('driver/on/ride/get/message', 'Api\Driver\MessageController@getMessage')->name("driver.trip.getMessage");
        
        Route::post('driver/admin/chat/support', 'Api\Driver\MessageController@sendMessageForSupport')->name("passenger.sendMessageForSupport");
        Route::post('driver/admin/chat/support/get/message', 'Api\Driver\MessageController@getMessageForSupport')->name("driver.tripgetMessageForSupport");

        // Trip History
        Route::get('driver/trip/history', 'Api\Driver\TripController@tripHistory')->name("driver.trip.tripHistory");
        Route::post('driver/ride/details', 'Api\Driver\TripController@tripDetails')->name("driver.trip.tripHistory");

        // report issue
        Route::post('driver/report/issue','Api\ReportIssue\ReportIssueController@reportIssue')->name('driver.report.issue');
        Route::get('report/issue/driver/subjects','Api\ReportIssue\ReportIssueController@reportIssueDriverSubjectList')->name('report.issue.driver.subjects');

        // ride cancelation
        Route::post('driver/ride/cancel', 'Api\Driver\TripController@cancelRide')->name("driver.cancelRide");
        // wallet
        Route::get('driver/get/payment/statistics','Api\Payment\PaymentController@getDriverPaymentStatactics')->name('driver.getDriverPaymentStatactics');
        Route::post('driver/get/transaction/list', 'Api\Payment\PaymentController@getDriverTransaction')->name("passenger.getDriverTransaction");
        Route::get('driver/get/summery/statistics','Api\Payment\PaymentController@getDriverSummaryStatactics')->name('driver.getDriverSummaryStatactics');
        Route::post('driver/get/summery/yearly','Api\Payment\PaymentController@getDriverYearlyStatactics')->name('driver.getDriverYearlyStatactics');
        Route::post('driver/get/date/waise/summary','Api\Payment\PaymentController@getDriverdateStatactics')->name('driver.getDriverdateStatactics');

        // update langage seletion
        //Route::put('driver/update/lang', 'Api\Driver\ProfileController@updateLang')->name("driver.lang");

        //get referral code
        Route::get('driver/get/referral', 'Api\Driver\ReferralController@getReferralCode')->name("driver.get.referral");

        //use referral code
        Route::post('driver/use/referral/code', 'Api\Driver\ReferralController@useReferralCode')->name("driver.use.referral.code");

        //===========not used===============================
        //create payment gateway sub account route
        //Route::post('driver/create/paymentgateway-subaccount','Api\Driver\SubaccountController@createSubaccount')->name('driver.create.paymentgateway-subaccount');
        //get payment gateway sub account route
        //Route::get('driver/get/paymentgateway-subaccount','Api\Driver\SubaccountController@getSubaccount')->name('driver.get.paymentgateway-subaccount');
        //update payment gateway sub account route
        //Route::post('driver/update/paymentgateway-subaccount','Api\Driver\SubaccountController@updatePaymentGatewaySubaccount')->name('driver.update.paymentgateway-subaccount');
        //==================================================

        //create payment gateway beneficiary route
        Route::post('driver/create/paymentgateway-beneficiary','Api\Driver\SubaccountController@createBeneficiary')->name('driver.create.paymentgateway-beneficiary');
        //get payment gateway beneficiary route
        Route::get('driver/get/paymentgateway-beneficiary','Api\Driver\SubaccountController@getBeneficiary')->name('driver.get.paymentgateway-beneficiary');

        //notification list
        Route::post('driver/notification/list', 'Api\ReportIssue\ReportIssueController@notificationList')->name("driver.notification.list");
        

    });
});
