@extends('admin.layout.base')

@section('title', 'Rider Cancel Report')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Rider Cancel Report</h5>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>Rider Name</th>
                        <th>Booking Number</th>
                        <th>Booking Date</th>
                        <th>Payment Status</th>
                        <th>Cancel Charge</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($passengerReports as $index => $passengerReport)
                    <tr>
                        <td>{{ $passengerReport->first_name }} {{ $passengerReport->last_name }}</td>
                        <td>{{ $passengerReport->request_no }}</td>
                        <td>{{date('Y-m-d h:i A',strtotime($passengerReport->created_at)) }}</td>
                        <td>{{ $passengerReport->status }}</td>
                        <td>{{ $passengerReport->currency }} {{ $passengerReport->cost }}</td>
                        <td style="line-height: 34px;">
                            <a href="{{ route('admin.request.details', [$passengerReport->request_id,'all']) }}" class="btn btn-info">View</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Rider Name</th>
                        <th>Booking Number</th>
                        <th>Booking Date</th>
                        <th>Payment Status</th>
                        <th>Cancel Charge</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection