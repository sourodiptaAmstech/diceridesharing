@extends('admin.layout.base')

@section('title', 'Driver Tracking')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Driver Tracking: <span class="text-info">{{$UserDevices->first_name}} {{$UserDevices->last_name}}</span></h5>
            <a href="{{ route('admin.driver.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <div class="row">
                <div class="col-md-12">
                    <div id="map-canvas"></div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
@section('styles')
<style type="text/css">
    #map-canvas {
        height: 450px;
    }
</style>
@endsection
@section('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&libraries=&v=weekly"></script>
<script type="text/javascript">

    function BackgroundRecursive(){
        var user_id = '{{$UserDevices->user_id}}';
        $.ajax({
            url: "random/"+user_id,
            method:"GET",
            success: function(response){
                console.log(response.userLocation);
                drawMap(new google.maps.LatLng(response.userLocation.latitude,response.userLocation.longitude));
                setTimeout(function(){
                    BackgroundRecursive();

                }, 30000);
            },
            error: function(response){
                console.log(response);
                setTimeout(function(){
                    BackgroundRecursive();
                }, 30000);
            }
        });
    }

    
    var mapElement=document.querySelector("#map-canvas");

    googleMap=new google.maps.Map(mapElement);

    var base_url = window.location.origin;
    var iconCar = {
        url: base_url+'/debarati/diceridesharing/public/asset/img/Car@3x.png',
        scaledSize: new google.maps.Size(35, 35),
    };

    var drawMap=(position)=>{
        googleMap.setOptions({
            center: position,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        new google.maps.Marker({ position, map: googleMap, icon: iconCar });
        // directionsRenderer.setMap(googleMap);
    }

    // drawMap(new google.maps.LatLng(location.latitude,location.longitude));
    BackgroundRecursive();
   

    // var map;
    // var zoomLevel = 5;

    // function initMap(){

    //     map = new google.maps.Map(document.getElementById('map-canvas'));
    //     var base_url = window.location.origin;
    //     var marker = new google.maps.Marker({
    //         map: map,
    //         icon: base_url+'/debarati/diceridesharing/public/asset/img/marker-start.png',
    //         anchorPoint: new google.maps.Point(0, -29)
    //     });

    //     var markerSecond = new google.maps.Marker({
    //         map: map,
    //         icon: base_url+'/debarati/diceridesharing/public/asset/img/marker-end.png',
    //         anchorPoint: new google.maps.Point(0, -29)
    //     });

    //     var bounds = new google.maps.LatLngBounds();
    //     var waypoints=[];

    
    //     source = new google.maps.LatLng(22.9867569, 87.8549755);

    //     destination = new google.maps.LatLng(22.9867887, 87.8546541);

    //     marker.setPosition(source);
    //     markerSecond.setPosition(destination);

    //     var directionsService = new google.maps.DirectionsService;
    //     var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true, preserveViewport: true});
    //     directionsDisplay.setMap(map);

    //     directionsService.route({
    //         origin: source,
    //         waypoints:waypoints,
    //         destination: destination,
    //         travelMode: google.maps.TravelMode.DRIVING
    //     }, function(result, status) {
    //         if (status == google.maps.DirectionsStatus.OK) {
    //            console.log(result);
    //             directionsDisplay.setDirections(result);

    //            marker.setPosition(result.routes[0].legs[0].start_location);
    //            markerSecond.setPosition(result.routes[0].legs[0].end_location);
    //         }
    //     });

    //     bounds.extend(marker.getPosition());
    //     bounds.extend(markerSecond.getPosition());
    //     map.fitBounds(bounds);
    // }

</script>
{{-- <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&libraries=places&callback=initMap" async defer></script> --}}

@endsection