@extends('admin.layout.base')

@section('title', 'Drivers ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Drivers
            </h5>
            <div class="row mb-1">
              <div class="col-lg-12 col-12">
                <div class="row">
                    <div class="col-lg-6 col-6">
                    </div>
                    <div class="col-lg-5 col-5 pr-0">
                        <input type="text" onfocus="(this.type='date')" onblur="(this.type='')"  name="from_date" id="from_date" class="pull-left form-control" style="width:49%;" placeholder="From Date"/>
                        <input type="text" onfocus="(this.type='date')" onblur="(this.type='')" name="to_date" id="to_date" class="pull-right form-control" style="width:49%;" placeholder="To Date"/>
                    </div>
                    <div class="col-lg-1 col-1">
                        <button type="button"  name="filter" id="filter" class="btn btn-info btn-block btn-sm pull-left" style="padding: 5px;">Filter</button>
                    </div>
                </div>
                </div>
            </div>
            @can('driver_create')
            <a href="{{ route('admin.driver.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Driver</a>
            @endcan
            <table class="table table-striped table-bordered dataTable" id="table-driv">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Driver Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Sign Up Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="TBody">
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Driver Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Sign Up Date</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

{{-- ..........Document Modal................. --}}
<div class="modal fade" id="driverModel" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content" style="background-color: lavender;">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
      </div>
      <div class="modal-body">
      
        <div class="d-flex justify-content-center">
            <div class="col-lg-12">

                <div class="alert alert-danger reason-danger" role="alert" style="display: none; height: 42px;">
                    <p class="reason-error" style="color: red; padding: 0px;"></p>
                </div>

                <form>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label text-dark">Reason</label>
                                <textarea name="reason" id="reason" rows="4" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            {{-- <input type="submit" class="btn btn-info float-left" id="saveReason" value="Submit"> --}}
                            <a href="javascript:void(0)" class="btn btn-info float-left" id="saveReason" style="width: 70px;">
                            <span id="submit">Submit</span>
                            <i id="submit_spin" class="fa fa-circle-o-notch fa-spin" style="font-size: 20px; display: none;"></i>
                            </a>
                            <a href="" class="btn btn-secondary float-right">Close</a>
                        </div>
                    </div>
                </form>
                 
            </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
{{-- end Document Modal --}}
@endsection
@section('scripts')
<script>
    $(document).ready(function(){

        $('#table-driv').DataTable({
            "order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.ajax-driver')}}",
                "dataType":"json",
                "type":"POST",
                "data":{"_token":"<?= csrf_token() ?>"}
            },
            "columns":[
                {"data":"id"},
                {"data":"driver_name"},
                {"data":"email"},
                {"data":"mobile"},
                {"data":"created_at"},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        });

        // $(document).on('click','.driver-status',function (e) {
        //     e.preventDefault();
        //     var href = $(this).attr('href');
        //     var value = $(this).data('value');
        //     var id = $(this).data("id");
        //     if (value == 'Activate') {
        //     bootbox.confirm('Do you really want to '+value+' driver?', function (res) {
        //     if (res){
        //         window.location = href;
        //     }  
        //     });
        //   }
        //   if (value == 'Inactivate') {
        //     $('#driverModel').modal('show');
        //     $('.modal-title').html('Driver Inactivate');
        //     $('#saveReason').click(function(e){
        //       e.preventDefault()
        //       $.ajaxSetup({
        //         headers:{
        //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //       });
        //       var reason = $('#reason').val();
        //       $('#submit').css({'display':'none'});
        //       $('#submit_spin').css({'display':'block'});
        //       $.ajax({
        //         url: '',
        //         method: 'post',
        //         data: { id:id, reason:reason },
        //         success: function(response){
        //             if (response.success) {
        //               $('#submit').css({'display':'block'});
        //               $('#submit_spin').css({'display':'none'});
        //               $('#driverModel').modal('hide');
        //               window.location = href;
        //             }
        //         },
        //         error: function(response){
        //           $('#submit').css({'display':'block'});
        //           $('#submit_spin').css({'display':'none'});
        //           $('.reason-danger').show();
        //           $('.reason-error').html(response.responseJSON.errors.reason);
        //         }
        //       });

        //     });
        //   }

        // });

    $('#filter').click(function(){
        var base_url = "{{URL::asset('/')}}";
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        if(from_date != '' &&  to_date != '')
        {
            $('#from_date').css({'border':''});
            $('#to_date').css({'border':''});
            var dataObject={"data":[]};
            $.ajax({
                url: '{{ route("admin.drivers.search") }}',
                method: 'GET',
                data: { from_date: from_date, to_date: to_date },
                success: function(response){
                    console.log(response);
                    var objectLength=Object.keys(response.profiles).length;
                    console.log(objectLength)
                    if (objectLength>0) {
                        for(var i=0; i<objectLength; i++){

                            if(response.profiles[i].service_status == "INACTIVE" || response.profiles[i].service_status == "BLOCK"){
                                var activationButton = '<a href="driver/activation/'+response.profiles[i].user_id+'" class="btn btn-danger driver-status" data-toggle="tooltip" data-value="Activate" data-id="'+response.profiles[i].user_id+'" title="Click to Active">Inactive</a>';
                            }else if (response.profiles[i].service_status == "REQUESTED"){
                                var activationButton = '<a href="javascript:void(0)" class="btn btn-warning">Requested</a>';
                            }else if (response.profiles[i].service_status == "ONRIDE"){
                                var activationButton = '<a href="javascript:void(0)" class="btn btn-warning">Onride</a>';
                            }else{
                                var activationButton = '<a href="driver/activation/'+response.profiles[i].user_id+'" class="btn btn-success driver-status" data-toggle="tooltip" data-value="Inactivate" data-id="'+response.profiles[i].user_id+'" title="Click to Inactive">Active</a>';
                            }

                            dataObject.data.push(
                            [
                                response.profiles[i].user_id,
                                response.profiles[i].first_name+" "+response.profiles[i].last_name,
                                response.profiles[i].email_id,
                                response.profiles[i].isd_code+"-"+response.profiles[i].mobile_no,
                                moment(response.profiles[i].created_at).format('YYYY-MM-DD'),
                                '<span style="line-height:33px;"><a href="driver/'+response.profiles[i].user_id+'/edit" class="btn btn-info">Edit</a> <a href="drivers/document/'+response.profiles[i].user_id+'" class="btn btn-info"> Document</a> <a href="drivers/service-type/'+response.profiles[i].user_id+'" class="btn btn-info"> Vehicle</a> <a href="driver/transaction/'+response.profiles[i].user_id+'" class="btn btn-info">Transaction</a> <a href="driver/trip/history/'+response.profiles[i].user_id+'" class="btn btn-info">Trip History</a> <a href="drivers/review-rating/'+response.profiles[i].user_id+'" class="btn btn-info"> Review/Rating</a> '+activationButton+' <a href="javascript:void(0)" class="btn btn-danger remove" data-id="'+response.profiles[i].user_id+'" data-toggle="tooltip" title="Click to Remove">Remove</a> <a href="drivers/tracking/'+response.profiles[i].user_id+'" class="btn btn-info">Tracking</a></span>'
                            
                            ]);
                        }
                        $('#table-driv').DataTable().clear().destroy();
                        $('#table-driv').DataTable({
                            responsive: true,
                            dom: 'Bfrtip',
                            bFilter: false,
                            bInfo: false,
                            buttons: [
                                'copyHtml5',
                                'excelHtml5',
                                'csvHtml5',
                                'pdfHtml5'
                                ],
                            "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [5] }
                            ],
                            "bFilter":true,
                            "bInfo" : true,
                            'data': dataObject.data
                        });
                    } else {
                        
                        $('#table-driv').DataTable().clear().destroy();
                        $('#table-driv').DataTable({
                        responsive: true,
                        dom: 'Bfrtip',
                        bFilter: false,
                        bInfo: false,
                        buttons: [
                            'copyHtml5',
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5'
                            ],
                        "bFilter":true,
                        "bInfo" : true
                        });
                        $('#TBody').css('text-align','center').html('<tr><td colspan="10">No Result Found</td></tr>');
                    }
                }
            });
        } else {
            $('#from_date').css({'border':'1px solid red'});
            $('#to_date').css({'border':'1px solid red'});
        }
    });

    
    $(document).on('click','.remove',function(e){
        e.preventDefault();
        var user_id=$(this).data('id');
       
        bootbox.confirm('Are you sure to remove this driver?', function(result)
        {
            if(result){
                $.ajax({
                    url: '{{ route("admin.drivers.remove") }}',
                    method: 'GET',
                    data: { user_id: user_id },
                    success: function(response){
                        location.reload();
                    },
                    error: function (response){
                        $('.inactive_doc').css("display", "block");
                        $('.inactive_msg').text(response.responseJSON.message);
                    }
                });
            }
        });
    });

});
</script>
@endsection