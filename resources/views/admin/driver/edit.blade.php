@extends('admin.layout.base')

@section('title', 'Update Driver ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    	    <a href="{{ route('admin.driver.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Driver</h5>

            <form class="form-horizontal" action="{{route('admin.driver.update', $driver->id )}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	<input type="hidden" name="_method" value="PATCH">
				<div class="form-group row">
					<label for="first_name" class="col-xs-2 col-form-label">First Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $driver->driver_profile->first_name }}" name="first_name" required id="first_name" placeholder="First Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="last_name" class="col-xs-2 col-form-label">Last Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $driver->driver_profile->last_name }}" name="last_name" required id="last_name" placeholder="Last Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="email" class="col-xs-2 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" value="{{ $driver->driver_profile->email_id }}" name="email" id="email" placeholder="Email">
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-2 col-form-label">Picture</label>
					<div class="col-xs-10">
					@if(isset($driver->driver_profile->picture))
                    	@if(File::exists(storage_path('app/public' .str_replace("storage", "", $driver->driver_profile->picture))))
                            <img style="height: 90px; margin-bottom: 15px;" src="{{URL::asset($driver->driver_profile->picture)}}">
                        @else
                            <img src="{{$driver->driver_profile->picture}}" style="height: 60px; margin-bottom: 15px;">
                        @endif
                    @else
                     	<img style="height: 60px; margin-bottom: 15px;" src="{{URL::asset('asset/NO_IMG.png')}}">
                    @endif
						<input type="file" accept="image/*" name="picture" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="mobile" class="col-xs-2 col-form-label">Mobile</label>
					<div class="col-xs-10">
						<input class="form-control" type="tel" value="{{ $driver->driver_profile->mobile_no }}" name="mobile" id="mobile" placeholder="Mobile No" required>
						<input type="hidden" name="code" id="code" value="{{$driver->driver_profile->isd_code}}">
					</div>
				</div>

				<div class="form-group row">
					<label for="dob" class="col-xs-2 col-form-label">Date of Birth</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" name="dob" value="{{ $driver->driver_profile->dob }}" id="dob">
					</div>
				</div>
				<div class="form-group row">
					<label for="gender" class="col-xs-2 col-form-label">Gender</label>
					<div class="col-xs-10">
						<input type="radio" id="Male" name="gender" value="Male" {{ $driver->driver_profile->gender == 'Male' ? 'checked' : ''}}>
						<label for="Male">Male</label><br>
						<input type="radio" id="Female" name="gender" value="Female" {{ $driver->driver_profile->gender == 'Female' ? 'checked' : ''}}>
						<label for="Female">Female</label>
					</div>
				</div>

				<div class="form-group row">
					<label for="city" class="col-xs-2 col-form-label">City</label>
					<div class="col-xs-10">
						<select name="city" id="city" class="form-control" required>
							<option value="">Select City</option>
							@foreach($cities as $city)
							<option value="{{$city->city_id}}" {{ $city->city_id == $driver->driver_profile->city ? 'selected' : '' }}>{{$city->name}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label for="service_type_id" class="col-xs-2 col-form-label">Service Type</label>
					<div class="col-xs-10">
						<select name="service_type_id" id="service_type_id" class="form-control" required>
						<option value="">Select Service Type</option>
						@foreach($ServiceTypeMstDatas as $ServiceTypeMstData)
							<option value="{{$ServiceTypeMstData['id']}}" {{ $driver->driver_service->service_type_id == $ServiceTypeMstData['id'] ? 'selected' : '' }}>{{$ServiceTypeMstData['name']}}</option>
						@endforeach
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="car_number" class="col-xs-2 col-form-label">Car Number</label>
					<div class="col-xs-10">
						<input type="text" name="car_number" id="car_number" class="form-control" value="{{ $driver->driver_service->registration_no }}" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="vehicle_color" class="col-xs-2 col-form-label">Vehicle Color</label>
					<div class="col-xs-10">
						<input type="text" name="vehicle_color" id="vehicle_color" class="form-control" placeholder="Vehicle Color" value="{{ $driver->driver_service->color }}" required>
					</div>
				</div>

			{{-- 	
				<div class="form-group row">
					<label for="car_plate_number" class="col-xs-2 col-form-label">Car Plate Number</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="car_plate_number" value="{{ $driver->driver_service->car_plate_number }}" id="car_plate_number" placeholder="Car Plate Number" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="car_number_expire_date" class="col-xs-2 col-form-label">Car Number Plate Expiry Date</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" name="car_number_expire_date" value="{{ $driver->driver_service->car_number_expire_date }}" id="car_number_expire_date" placeholder="Car Number Expire Date" required>
					</div>
				</div>
			@endif --}}
				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Driver</button>
						<a href="{{route('admin.driver.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
 <style type="text/css">.iti { width: 100%; }</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
 <script>
    	jq = jQuery.noConflict();
        use :
       		var s = jq("#mobile").intlTelInput({
       			autoPlaceholder: 'polite',
       			separateDialCode: true,
       			formatOnDisplay: true,
       			initialCountry: 'ng',
       			preferredCountries:["ng"]
       		});
       	insteadof :
       		var countryData = window.intlTelInputGlobals.getCountryData();
       		var iso2;
			var isdcode = jq("#code").val();
			for (var i = 0; i < countryData.length; i++) {
				if (countryData[i].dialCode == parseInt(isdcode)){
					iso2 = countryData[i].iso2;
					break;
				}
			}
			if(iso2){
				jq("#mobile").intlTelInput("setCountry", iso2);
			}
			jq(document).on('countrychange', function (e, countryData) {
            	jq("#code").val((jq("#mobile").intlTelInput("getSelectedCountryData").dialCode));
        	});
</script>
@endsection
{{-- @section('scripts')
<script>
	$('.spin').hide()
	var make = '{{$driver->driver_service->car_make}}';
	var model = '{{$driver->driver_service->car_model}}';
	if (make) {
	$('.spin').show()
	$.ajax({
        url: 'https://carmakemodeldb.com/api/v1/car-lists/get/all/models/'+make+'?api_token=8upB6H0FZ0ZCCRxmFEeurmKGYbZiyaGB0061nzhFv1Pkc4XZ1vfHguosd0TN',
        method: 'GET',
        success: function(response){
        	$('.spin').hide()
           	$('#car_type').empty();
           	var options = '';
			$.each(response, function(key, value) {
				options += '<option value="'+value.model+'">'+value.model+'</option>';
			});
			$('#car_type').append(options);
			$('#car_type option[value="'+model+'"]').attr('selected','selected');
        },
        error: function(response){
        	console.log(response);
        }
    });
	}
	$('#car_make').change(function(){
    	var make = $(this).val();
    	if (!make) {
    		make = ' ';
    	}
    	$('.spin').show()
    	$.ajax({
            url: 'https://carmakemodeldb.com/api/v1/car-lists/get/all/models/'+make+'?api_token=8upB6H0FZ0ZCCRxmFEeurmKGYbZiyaGB0061nzhFv1Pkc4XZ1vfHguosd0TN',
            method: 'GET',
            success: function(response){
            	$('.spin').hide()
               	$('#car_type').empty();
               	var options = '';
				$.each(response, function(key, value) {
				  options += '<option value="'+value.model+'">'+value.model+'</option>';
				});

				$('#car_type').append(options);

            },
            error: function(response){
            	console.log(response);
            }
        });
    });
</script>
@endsection --}}
