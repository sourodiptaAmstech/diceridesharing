@extends('admin.layout.base')

@section('title', 'Driver Reviews')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Driver Reviews</h5>
            <p>
                <a href="{{ route('admin.review.driver') }}" class="{{ Request::is('admin/review/driver') ? 'actives' : '' }}">
                    <span class="sort">Driver</span>
                </a>
                 | 
                <a href="{{ route('admin.review.passenger') }}" class="{{ Request::is('admin/review/passenger') ? 'actives' : '' }}">
                    <span class="sort">Rider</span>
                </a>
            </p>
            <table class="table table-striped table-bordered dataTable" id="table-review">
                <thead>
                    <tr>
                        <th>Ride Number</th>
                        <th>Driver Name</th>
                        <th>Rider Name</th>
                        <th>Rate</th>
                        <th>Date</th>
                        <th>Comments</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Ride Number</th>
                        <th>Driver Name</th>
                        <th>Rider Name</th>
                        <th>Rate</th>
                        <th>Date</th>
                        <th>Comments</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<style>
    .sort{
        color: #3a8e88;
        cursor: pointer;
    }
    .sort:hover{
        font-weight: bold;
    }
    .actives{
        text-decoration: underline;
        color: #3a8e88;
    }
</style>
@endsection
@section('scripts')
    <script>
    $(document).ready(function(){
        $('#table-review').DataTable({
            "order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.ajax-review')}}",
                "dataType":"json",
                "type":"POST",
                "data":{"_token":"<?= csrf_token() ?>"}
            },
            "columns":[
                {"data":"booking_number"},
                {"data":"customer_name"},
                {"data":"driver_name"},
                {"data":"rating_by_passenger"},
                {"data":"created_at"},
                {"data":"comment_by_passenger"}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        });
    });
    </script>
@endsection