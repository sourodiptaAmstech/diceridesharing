@extends('admin.layout.base')

@section('title', 'Add Cancel Reason')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.cancel-reason.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Add Cancellation Reason</h5>

            <form class="form-horizontal" action="{{route('admin.cancel-reason.store')}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="reason" class="col-xs-2 col-form-label">Reason</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('reason') }}" name="reason" required id="reason" placeholder="Reason">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="reason_given_by" class="col-xs-2 col-form-label">Reason Reported By</label>
                    <div class="col-xs-10">
                        <select name="reason_given_by" class="form-control">
                            <option value="">Select reporter</option>
                            <option value="passenger">Rider</option>
                            {{-- <option value="driver_for_reject">Driver for reject</option> --}}
                            <option value="driver">Driver</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Add Reason</button>
                        <a href="{{route('admin.cancel-reason.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
