@extends('admin.layout.base')

@section('title', 'Reason ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Cancellation Reasons</h5>
                <a href="{{ route('admin.cancel-reason.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Reason</a>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Reasons</th>
                            <th>Report By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($documents as $index => $document)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$document->reason}}</td>
                            <td>
                            @if ($document->reason_given_by=='passenger')
                                Rider
                            @endif
                            @if ($document->reason_given_by=='driver')
                                Driver
                            @endif
                            @if ($document->reason_given_by=='driver_for_reject')
                                Driver for reject
                            @endif
                            </td>
                            <td style="line-height: 34px;">
                            <a href="{{ route('admin.cancel-reason.edit', $document->cancel_reason_id) }}" class="btn btn-info">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Reasons</th>
                            <th>Report By</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
