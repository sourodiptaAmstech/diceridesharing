@extends('admin.layout.base')

@section('title', 'Cities ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Cities
            </h5>
            @can('city_create')
            <a href="{{ route('admin.city.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New City</a>
            @endcan
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cities as $index => $city)
                        <tr>
                            <td>{{ $city->name }}</td>
                            
                            <td>
                            @can('city_edit')
                                <a href="{{ route('admin.city.edit', $city->city_id) }}" class="btn btn-info">Edit</a>
                            @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection