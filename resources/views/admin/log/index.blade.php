@extends('admin.layout.base')

@section('title', 'Sub-admin Log ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Sub-admin Logs
            </h5>
            <table class="table table-striped table-bordered dataTable" id="table-log">
                <thead>
                    <tr>
                        <th>Action Type</th>
                        <th>Message</th>
                        <th>Created Date</th>
                    </tr>
                </thead>
              
                <tfoot>
                    <tr>
                        <th>Action Type</th>
                        <th>Message</th>
                        <th>Created Date</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#table-log').DataTable( {
            "order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.subadmin.log.report')}}",
                "dataType":"json",
                "type":"POST",
                "data":{"_token":"<?= csrf_token() ?>"}
            },
            "columns":[
                {"data":"flag"},
                {"data":"message"},
                {"data":"created_at"}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        } );
    </script>
@endsection