@extends('admin.layout.base')

@section('title', 'Roles')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Roles
            </h5>
            @can('rbac_management')
            <a href="{{ route('admin.role.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Roles</a>
            @endcan
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Roles</th>
                        <th>Permissions</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles as $index => $role)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $role->title }}</td>
                        @if(count($role->permissions)>0)
                        <td>
                            @foreach($role->permissions as $permission)
                            {{ $permission->title }},
                            @endforeach
                        </td>
                        @else
                        <td></td>
                        @endif
                        <td>
                            @can('rbac_management')
                                @if($role->title=='Admin')
                                    
                                @else
                                    <a href="{{ route('admin.role.edit', $role->id) }}" class="btn btn-info">Edit</a>
                                @endif
                            @endcan
                            {{-- <form action="{{ route('admin.passenger.destroy', $passenger->id) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                            </form> --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Roles</th>
                        <th>Permissions</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection