@extends('admin.layout.base')

@section('title', 'Update Role')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    		<a href="{{ route('admin.role.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Role</h5>

            <form class="form-horizontal" action="{{route('admin.role.update',$role->id)}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">

				<div class="form-group row">
					<label for="role" class="col-xs-2 col-form-label">Role</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $role->title }}" name="role" id="role" placeholder="Role" required>
					</div>
				</div>

                <div class="form-group row">
                    <label for="permissions" class="col-xs-2 col-form-label">Permissions
                    </label>
                    <div class="col-xs-10">
                        <div class="mb-1">
                            <span class="btn btn-info btn-xs select-all">Select All</span>
                            <span class="btn btn-info btn-xs deselect-all">Deselect All</span>
                        </div>
                        <select name="permissions[]" id="permissions" class="form-control select2" multiple="multiple" required>
                            @foreach($permissions as $id => $permissions)
                                <option value="{{ $id }}" {{ (in_array($id, old('permissions', [])) || isset($role) && $role->permissions->contains($id)) ? 'selected' : '' }}>{{ $permissions }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Role</button>
						<a href="{{route('admin.role.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
@endsection
