@extends('admin.layout.base')

@section('title', 'Riders')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Riders
            </h5>
            <div class="row mb-1">
                <div class="col-lg-12 col-12">
                    <div class="row">
                        <div class="col-lg-6 col-6">
                        </div>
                        <div class="col-lg-5 col-5 pr-0">
                            <input type="text" onfocus="(this.type='date')" onblur="(this.type='')"  name="from_date" id="from_date" class="pull-left form-control" style="width:49%;" placeholder="From Date"/>
                            <input type="text" onfocus="(this.type='date')" onblur="(this.type='')" name="to_date" id="to_date" class="pull-right form-control" style="width:49%;" placeholder="To Date"/>
                        </div>
                        <div class="col-lg-1 col-1">
                            <button type="button"  name="filter" id="filter" class="btn btn-info btn-block btn-sm pull-left" style="padding: 5px;">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            @can('passenger_create')
            <a href="{{ route('admin.passenger.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Rider</a>
            @endcan
            <table class="table table-striped table-bordered dataTable" id="table-pass">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Rider Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Sign Up Date</th>
                        <th>Eligible for Referral</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="TBody">
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Rider Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Sign Up Date</th>
                        <th>Eligible for Referral</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#table-pass').DataTable( {
            "order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.ajax-passenger')}}",
                "dataType":"json",
                "type":"POST",
                "data":{"_token":"<?= csrf_token() ?>"}
            },
            "columns":[
                {"data":"id"},
                {"data":"passenger_name"},
                {"data":"email"},
                {"data":"mobile"},
                {"data":"created_at"},
                {"data":"referral","searchable":false,"orderable":false},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        });

        $('#filter').click(function(){
            var base_url = "{{URL::asset('/')}}";
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if(from_date != '' &&  to_date != '')
            {
                $('#from_date').css({'border':''});
                $('#to_date').css({'border':''});
                var dataObject={"data":[]};
                $.ajax({
                    url: '{{ route("admin.passengers.search") }}',
                    method: 'GET',
                    data: { from_date: from_date, to_date: to_date },
                    success: function(response){
                        console.log(response);
                        var objectLength=Object.keys(response.profiles).length;
                        console.log(objectLength)
                        if (objectLength>0) {
                            for(var i=0; i<objectLength; i++){

                                if (response.profiles[i].status == 'BLOCK') {
                                    var activationButton = '<a href="passenger/activation/'+response.profiles[i].user_id+'" class="btn btn-danger driver-status" data-toggle="tooltip" title="Click to Active">Block</a>';
                                }
                                if (response.profiles[i].status == 'ACTIVE') {
                                    var activationButton = '<a href="passenger/activation/'+response.profiles[i].user_id+'" class="btn btn-success driver-status" data-toggle="tooltip" title="Click to Block">Active</a>';
                                }

                                if (response.profiles[i].completeRequest<=0) {
                                    var referral = "Eligible";
                                } else {
                                    var referral = "Not Eligible";
                                }

                                dataObject.data.push(
                                [
                                    response.profiles[i].user_id,
                                    response.profiles[i].first_name+" "+response.profiles[i].last_name,
                                    response.profiles[i].email_id,
                                    response.profiles[i].isd_code+"-"+response.profiles[i].mobile_no,
                                    moment(response.profiles[i].created_at).format('YYYY-MM-DD'),
                                    referral,
                                    '<span style="line-height:33px;"><a href="passenger/'+response.profiles[i].user_id+'/edit" class="btn btn-info">Edit</a> <a href="passenger/transaction/'+response.profiles[i].user_id+'" class="btn btn-info">Transaction</a> <a href="passenger/trip/history/'+response.profiles[i].user_id+'" class="btn btn-info">Trip History</a> <a href="passenger/review-rating/'+response.profiles[i].user_id+'" class="btn btn-info"> Review/Rating</a> <a href="passenger/split/transaction/'+response.profiles[i].user_id+'" class="btn btn-info">Split Transaction</a> '+activationButton+' <a href="javascript:void(0)" class="btn btn-danger remove" data-id="'+response.profiles[i].user_id+'" data-toggle="tooltip" title="Click to Remove">Remove</a></span>'
                                    
                                ]);
                            }
                            $('#table-pass').DataTable().clear().destroy();
                            $('#table-pass').DataTable({
                                responsive: true,
                                dom: 'Bfrtip',
                                bFilter: false,
                                bInfo: false,
                                buttons: [
                                    'copyHtml5',
                                    'excelHtml5',
                                    'csvHtml5',
                                    'pdfHtml5'
                                    ],
                                "aoColumnDefs": [
                                    { "bSortable": false, "aTargets": [6] }
                                ],
                                "bFilter":true,
                                "bInfo" : true,
                                'data': dataObject.data
                            });
                        } else {
                            
                            $('#table-pass').DataTable().clear().destroy();
                            $('#table-pass').DataTable({
                            responsive: true,
                            dom: 'Bfrtip',
                            bFilter: false,
                            bInfo: false,
                            buttons: [
                                'copyHtml5',
                                'excelHtml5',
                                'csvHtml5',
                                'pdfHtml5'
                                ],
                            "bFilter":true,
                            "bInfo" : true
                            });
                            $('#TBody').css('text-align','center').html('<tr><td colspan="10">No Result Found</td></tr>');
                        }
                    }
                });
            } else {
                $('#from_date').css({'border':'1px solid red'});
                $('#to_date').css({'border':'1px solid red'});
            }
        });
        
    $(document).ready(function(){
        $(document).on('click','.remove',function(e){
            e.preventDefault();
            var user_id=$(this).data('id');
           
            bootbox.confirm('Are you sure to remove this rider?', function(result)
            {
                if(result){
                    $.ajax({
                        url: '{{ route("admin.passengers.remove") }}',
                        method: 'GET',
                        data: { user_id: user_id },
                        success: function(response){
                            location.reload();
                        },
                        error: function (response){
                            $('.inactive_doc').css("display", "block");
                            $('.inactive_msg').text(response.responseJSON.message);
                        }
                    });
                }
            });

        });
    });

    </script>
@endsection