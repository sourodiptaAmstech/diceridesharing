@extends('admin.layout.base')
@section('title', 'Transactions')
@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
        <h5 class="mb-1">Transactions</h5>
        <a href="{{ route('admin.passenger.index') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> Back</a>
        <table class="table table-striped table-bordered dataTable" id="table-transac">
            <thead>
                <tr>
                    <th>Request Number</th>
                    <th>Booking Date</th>
                    <th>Payment Method</th>
                    <th>Tax(₦)</th>
                    <th>Insurance(₦)</th>
                    <th>Previous Trip Cancel Amount(₦)</th>
                    <th>Total Amount(₦)</th>
                </tr>
            </thead>
            <tbody>
            @foreach($serviceRequest as $index => $service)
                <tr>
                    <td class="nr">{{$service->request_no}}</td>
                    <td class="nr">{{date('Y-m-d h:i A',strtotime($service->created_at)) }}</td>
                    <td class="nr">{{$service->payment_method}}</td>
                    <td class="nr">{{$service->tax}}</td>
                    <td class="nr">{{$service->ride_insurance}}</td>
                    <td class="nr">{{$service->passengerCancelAmount}}</td>
                    <td class="nr">{{$service->cost - $service->promo_code_value - $service->referral_value - $service->splitedAmount}}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                  <th>Request Number</th>
                  <th>Booking Date</th>
                  <th>Payment Method</th>
                  <th>Tax(₦)</th>
                  <th>Insurance(₦)</th>
                  <th>Previous Trip Cancel Amount(₦)</th>
                  <th>Total Amount(₦)</th>
                </tr>
            </tfoot>
        </table>
		</div>
    </div>
</div>
@endsection
@section('scripts')
<script>
  $('#table-transac').DataTable({
      responsive: true,
      dom: 'Bfrtip',
      buttons: [
          'copyHtml5',
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
      ],
  });
  
</script>
@endsection