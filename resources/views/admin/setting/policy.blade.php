@extends('admin.layout.base')

@section('title', 'Privacy Policy ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Rider's Privacy Policy</h5>

            @foreach($policies as $policy)
            @if($policy->key == 'page_privacy')
            <div className="row">
                <form action="{{ route('admin.privacy-policy.update',$policy->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor">{{$policy->value}}</textarea>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            @can('passenger_privacy_policy_update')
                            <button type="submit" class="btn btn-primary">Update</button>
                            @endcan
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Driver's Privacy Policy</h5>
            @foreach($policies as $policy)
            @if($policy->key == 'page_privacy_driver')
            <div className="row">
                <form action="{{ route('admin.privacy-policy.update',$policy->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor2">{{$policy->value}}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            @can('driver_privacy_policy_update')
                            <button type="submit" class="btn btn-primary">Update</button>
                            @endcan
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Rider's Terms and Conditions</h5>

            @foreach($policies as $policy)
            @if($policy->key == 'condition_privacy')
            <div className="row">
                <form action="{{ route('admin.privacy-policy.update',$policy->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor1">{{$policy->value}}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            @can('passenger_term_condition_update')
                            <button type="submit" class="btn btn-primary">Update</button>
                            @endcan
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Driver's Terms and Conditions</h5>

            @foreach($policies as $policy)
            @if($policy->key == 'condition_privacy_driver')
            <div className="row">
                <form action="{{ route('admin.privacy-policy.update',$policy->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor3">{{$policy->value}}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            @can('driver_term_condition_update')
                            <button type="submit" class="btn btn-primary">Update</button>
                            @endcan
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('myeditor');
    CKEDITOR.replace('myeditor1');
    CKEDITOR.replace('myeditor2');
    CKEDITOR.replace('myeditor3');
</script>
@endsection