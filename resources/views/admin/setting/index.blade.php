@extends('admin.layout.base')

@section('title', 'Site Setting')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Site Setting</h5>
            <table class="table table-striped table-bordered dataTable" id="table-setting">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($site_settings as $index => $site_setting)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>
                            @if($site_setting->key == 'site_title')
                                Title
                            @endif
                            @if($site_setting->key == 'site_logo')
                                Logo
                            @endif
                            @if($site_setting->key == 'site_email_logo')
                                Email Logo
                            @endif
                            @if($site_setting->key == 'site_icon')
                                Icon
                            @endif
                            @if($site_setting->key == 'site_copyright')
                                Copyright
                            @endif
                            @if($site_setting->key == 'surge_price')
                                Surge Price
                            @endif
                            @if($site_setting->key == 'commission')
                                Commission Charge
                            @endif
                            @if($site_setting->key == 'passenger_cancellation_charge')
                                Passenger Cancellation Charge
                            @endif
                            @if($site_setting->key == 'driver_cancellation_charge')
                                Driver Cancellation Charge
                            @endif
                            {{-- @if($site_setting->key == 'web_site')
                                Website
                            @endif --}}
                            @if($site_setting->key == 'contact_number')
                                Contact Number
                            @endif
                            @if($site_setting->key == 'contact_email')
                                Contact Email
                            @endif
                            @if($site_setting->key == 'sos_number')
                               Passenger SOS Number
                            @endif
                            @if($site_setting->key == 'dsos_number')
                               Driver SOS Number
                            @endif
                        </td>
                        <td>
                            {{-- ||$site_setting->key == 'web_site'
                            --}}

                            @if($site_setting->key == 'site_title'||$site_setting->key == 'site_copyright'||$site_setting->key == 'contact_number'||$site_setting->key == 'contact_email'||$site_setting->key == 'sos_number'||$site_setting->key == 'dsos_number'||$site_setting->key == 'passenger_cancellation_charge'||$site_setting->key == 'driver_cancellation_charge')
                                {{ $site_setting->value }}
                            @elseif($site_setting->key == 'commission')
                                {{ $site_setting->value }} %
                            @elseif($site_setting->key == 'site_logo'||$site_setting->key == 'site_email_logo'||$site_setting->key == 'site_icon')
                                @if(isset($site_setting->value))
                                    @if(File::exists(storage_path('app/public' .str_replace("storage", "", $site_setting->value))))
                                    <img src="{{URL::asset($site_setting->value)}}" style="height: 60px;">
                                    @else
                                    <img src="{{URL::asset('asset/NO_IMG.png')}}" style="height: 50px; width: 65px;">
                                    @endif
                                @else
                                    <img src="{{URL::asset('asset/NO_IMG.png')}}" style="height: 50px; width: 65px;">
                                @endif
                    
                            @elseif($site_setting->key == 'surge_price')
                                <span id="femaleFriend">{{ $site_setting->value }} %</span>
                            @endif
                        </td>
                        <td style="line-height: 40px;">
                            @if($site_setting->key == 'surge_price')
                                @can('site_setting_edit')
                                    <a href="{{ route('admin.setting.edit', $site_setting->id) }}" class="btn btn-info">Edit</a>
                                @endcan

                                <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox stripe_check" id="myonoffswitch" tabindex="0" onchange="cardselect()">
                                    <label class="onoffswitch-label" for="myonoffswitch">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            @else
                                @can('site_setting_edit')
                                    <a href="{{ route('admin.setting.edit', $site_setting->id) }}" class="btn btn-info">Edit</a>
                                @endcan
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<style>
    .onoffswitch {
        position: relative; width: 90px;
        -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
    }
    .onoffswitch-checkbox {
        position: absolute;
        opacity: 0;
        pointer-events: none;
    }
    .onoffswitch-label {
        display: block; overflow: hidden; cursor: pointer;
        border: 2px solid #999999; border-radius: 20px;
    }
    .onoffswitch-inner {
        display: block; width: 200%; margin-left: -100%;
        transition: margin 0.3s ease-in 0s;
    }
    .onoffswitch-inner:before, .onoffswitch-inner:after {
        display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
        font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
        box-sizing: border-box;
    }
    .onoffswitch-inner:before {
        content: "ON";
        padding-left: 10px;
        background-color: #0da612; color: #FFFFFF;
    }
    .onoffswitch-inner:after {
        content: "OFF";
        padding-right: 10px;
        background-color: dimgrey; color: linen;
        text-align: right;
    }
    .onoffswitch-switch {
        display: block;
        width: 18px;
        background: #FFFFFF;
        position: absolute;
        top: 0;
        bottom: 0;
        border: 2px solid #999999; border-radius: 20px;
        transition: all 0.3s ease-in 0s;

        margin-bottom: 5px;
        margin-top: 3px;
        margin-right: 6px;
        margin-left: 6px;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
        margin-left: 0;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
        right: 0px;
    }
</style>
@endsection
@section('scripts')
<script>
function cardselect(){
    if($('.stripe_check').is(":checked")){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var status = 'YES';
        $.ajax({
            url: "{{ route('admin.settings.surgeprice') }}",
            method:"PUT",
            data:{ status: status },
            success: function(response){
                $('#femaleFriend').text(response.data.value+" %")
            },
            error: function(response){
            }
        });

    } else {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var status = 'NO';
        $.ajax({
            url: "{{ route('admin.settings.surgeprice') }}",
            method:"PUT",
            data:{ status: status },
            success: function(response){
                $('#femaleFriend').text(response.data.value+" %")
            },
            error: function(response){
            }
        });
    }
}
$(document).ready(function(){
    $('#table-setting').DataTable( {
        responsive: true,
        dom: 'Bfrtip',
        buttons: []
    });

    $.ajax({
        url: "{{ route('admin.get.surgeprice') }}",
        method:"GET",
        data:{ },
        success: function(response){
            var status = response.data.status;
            if (status=='Y') {
                $('.stripe_check').attr('checked','checked');
            }
            if (status=='N') {
                $('.stripe_check').removeAttr('checked');
            }
        },
        error: function(response){
        }
    });
});
</script>
@endsection