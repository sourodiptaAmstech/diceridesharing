@extends('admin.layout.base')
@section('title', 'Push Notification')
@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    		<a href="{{ route('admin.dashboard.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Push Notification</h5>

            <form class="form-horizontal" action="{{route('admin.send.push-notification')}}" method="POST" role="form">
            	{{csrf_field()}}

            	<div class="form-group row">
					<label for="user_type" class="col-xs-2 col-form-label">Select User Type</label>
					<div class="col-xs-10">
                        <select name="user_type" id="user_type" class="form-control" required>
                            <option value="">Select User Type</option>
                            <option value="rider">Rider</option>
                            <option value="driver">Driver</option>
                        </select>
					</div>
				</div>

				<div class="form-group row">
					<label for="user" class="col-xs-2 col-form-label">Select User</label>
					<div class="col-xs-10">
						<select name="user" id="user" class="form-control" required>
                        </select>
					</div>
				</div>

                <div class="form-group row">
					<label for="title" class="col-xs-2 col-form-label">Title</label>
					<div class="col-xs-10">
                        <input type="text" class="form-control" name="title" id="title" placeholder="Title" required/>
					</div>
				</div>

				<div class="form-group row">
					<label for="message" class="col-xs-2 col-form-label">Message</label>
					<div class="col-xs-10">
                        <textarea class="form-control" name="message" id="message" placeholder="Write some message..." required></textarea>
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Send Push-Notification</button>
						<a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$(document).ready(function(){
    $('#user_type').change(function(){
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var user_type = $(this).val();
        $.ajax({
            url: "{{route('admin.get.users')}}",
            method: "POST",
            data: { user_type: user_type },
            success: function(response){
                console.log(response);
                $('#user').empty();
                if(user_type=='rider'){
                    var options = '<option value="">Select Rider</option>';
                }else if(user_type=='driver'){
                    var options = '<option value="">Select Driver</option>';
                }else{
                    var options = '';
                }
                $.each(response.data, function(key, value) {
                    options += '<option value="'+value.user_id+'">'+value.first_name+' '+value.last_name+'</option>';
                });
                $('#user').append(options);
            }
        })
    })
})
</script>
@endsection