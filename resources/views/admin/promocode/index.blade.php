@extends('admin.layout.base')

@section('title', 'Promocodes ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Promocodes</h5>

                <div class="row mb-1">
                    <div class="col-lg-12 col-12">
                        <div class="row">
                            <div class="col-lg-6 col-6">
                            </div>
                            <div class="col-lg-5 col-5 pr-0">
                                <input type="text" onfocus="(this.type='date')" onblur="(this.type='')"  name="from_date" id="from_date" class="pull-left form-control" style="width:49%;" placeholder="From Date"/>
                                <input type="text" onfocus="(this.type='date')" onblur="(this.type='')" name="to_date" id="to_date" class="pull-right form-control" style="width:49%;" placeholder="To Date"/>
                            </div>
                            <div class="col-lg-1 col-1">
                                <button type="button"  name="filter" id="filter" class="btn btn-info btn-block btn-sm pull-left" style="padding: 5px;">Filter</button>
                            </div>
                        </div>
                    </div>
                </div>

                @can('promocode_create')
                <a href="{{ route('admin.promocode.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Promocode</a>
                @endcan

                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Promocode </th>
                            <th>Discount </th>
                            <th>Activation</th>
                            <th>Expiration</th>
                            <th>Validity</th>
                            <th>Status</th>
                            <th>Used Count</th>
                            <th>Promocode Type</th>
                            <th>Ride Count</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="TBody">
                    @foreach($promocodes as $index => $promo)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$promo->promo_code}}</td>
                            <td>{{$promo->discount}}</td>
                            <td>
                                {{date('d-m-Y',strtotime($promo->activation))}}
                            </td>
                            <td>
                                {{date('d-m-Y',strtotime($promo->expiration))}}
                            </td>
                            <td>{{
                                round( ( strtotime(date('d-m-Y',strtotime($promo->expiration))) - strtotime(date('d-m-Y',strtotime($promo->activation))) ) / (60 * 60 * 24) )
                            }} days</td>
                            <td>
                                @if(date("Y-m-d") <= $promo->expiration)
                                    <span class="tag tag-success">Valid</span>
                                @else
                                    <span class="tag tag-danger">Expiration</span>
                                @endif
                            </td>
                            <td>
                               {{App\Http\Controllers\Admin\Promocode\PromocodeController::promo_used_count($promo->promocodes_id)}}
                            </td>
                            <td>{{$promo->promocode_type}}</td>
                            <td>{{$promo->rideCount}}</td>
                            <td style="line-height: 34px;">
                            @can('promocode_edit')
                                <a href="{{ route('admin.promocode.edit', $promo->promocodes_id) }}" class="btn btn-info"> Edit</a>
                            @endcan
                                <form action="{{ route('admin.promocode.destroy', $promo->promocodes_id) }}" method="POST" style="line-height: 30px;" class="promo-delete">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    
                                @if(date("Y-m-d") <= $promo->expiration)
                                @else
                                    @if(App\Http\Controllers\Admin\Promocode\PromocodeController::promo_used_count($promo->promocodes_id)==0)
                                    @can('promocode_delete')
                                        <button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                    @endcan
                                    @endif
                                @endif 
                                </form>
                                {{-- <a href="{{ route('admin.promocode.show', $promo->promocodes_id) }}" class="btn btn-info"> Show</a> --}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Promocode </th>
                            <th>Discount </th>
                            <th>Activation</th>
                            <th>Expiration</th>
                            <th>Validity</th>
                            <th>Status</th>
                            <th>Used Count</th>
                            <th>Promocode Type</th>
                            <th>Ride Count</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $(document).on('click','.promo-delete',function (e) {
            e.preventDefault();
            var form=$(this);
            bootbox.confirm('Do you really want to delete?', function (res) {
            if (res){
               form.submit();
            }
            });
        });

        $('#filter').click(function(){
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if(from_date != '' &&  to_date != '')
            {
                $('#from_date').css({'border':''});
                $('#to_date').css({'border':''});
                var dataObject={"data":[]};
                $.ajax({
                    url: '{{ route("admin.promocodes.search") }}',
                    method: 'GET',
                    data: { from_date: from_date, to_date: to_date },
                    success: function(response){
                        console.log(response);
                        var objectLength=Object.keys(response.promocodes).length;
                        console.log(objectLength)
                        if (objectLength>0) {
                            for(var i=0; i<objectLength; i++){

                                var now = new Date();
                                var dateString = moment(now).format('YYYY-MM-DD');

                                if(dateString <= response.promocodes[i].expiration){
                                    var validStatus='<span class="tag tag-success">Valid</span>';
                                }
                                else{
                                    var validStatus='<span class="tag tag-danger">Expiration</span>';
                                }

                                dataObject.data.push(
                                [
                                    i+1,
                                    response.promocodes[i].promo_code,
                                    response.promocodes[i].discount,
                                    moment(response.promocodes[i].activation).format('DD-MM-YYYY'),
                                    moment(response.promocodes[i].expiration).format('DD-MM-YYYY'),
                                    // round(Date.parse( ( moment(response.promocodes[i].expiration).format('DD-MM-YYYY') - moment(response.promocodes[i].activation).format('DD-MM-YYYY') ) / (60 * 60 * 24) ))+" days",
                                    "-",
                                    validStatus,
                                    "-",

                                    response.promocodes[i].promocode_type,
                                    response.promocodes[i].rideCount,

                                    '<a href="promocode/'+response.promocodes[i].promocodes_id+'/edit" class="btn btn-info"> Edit</a>'
                                    
                                ]);
                            }
                            $('#table-2').DataTable().clear().destroy();
                            $('#table-2').DataTable({
                                responsive: true,
                                dom: 'Bfrtip',
                                bFilter: false,
                                bInfo: false,
                                buttons: [
                                    'copyHtml5',
                                    'excelHtml5',
                                    'csvHtml5',
                                    'pdfHtml5'
                                    ],
                                "aoColumnDefs": [
                                    { "bSortable": false, "aTargets": [10] }
                                ],
                                "bFilter":true,
                                "bInfo" : true,
                                'data': dataObject.data
                            });
                        } else {
                            
                            $('#table-2').DataTable().clear().destroy();
                            $('#table-2').DataTable({
                            responsive: true,
                            dom: 'Bfrtip',
                            bFilter: false,
                            bInfo: false,
                            buttons: [
                                'copyHtml5',
                                'excelHtml5',
                                'csvHtml5',
                                'pdfHtml5'
                                ],
                            "bFilter":true,
                            "bInfo" : true
                            });
                            $('#TBody').css('text-align','center').html('<tr><td colspan="10">No Result Found</td></tr>');
                        }
                    }
                });
            } else {
                $('#from_date').css({'border':'1px solid red'});
                $('#to_date').css({'border':'1px solid red'});
            }
        });

    });
</script>
@endsection