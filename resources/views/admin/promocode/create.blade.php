@extends('admin.layout.base')

@section('title', 'Add Promocode ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.promocode.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Promocode</h5>

            <form class="form-horizontal" action="{{route('admin.promocode.store')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
				<div class="form-group row">
					<label for="promo_code" class="col-xs-2 col-form-label">Promocode</label>
					<div class="col-xs-10">
						<input class="form-control" autocomplete="off"  type="text" value="{{ old('promo_code') }}" name="promo_code" required id="promo_code" placeholder="Promocode">
					</div>
				</div>
				<div class="form-group row">
					<label for="discount" class="col-xs-2 col-form-label">Discount</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{ old('discount') }}" name="discount" required id="discount" placeholder="Discount">
					</div>
				</div>

				<div class="form-group row">
					<label for="activation" class="col-xs-2 col-form-label">Activation</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" value="{{ old('activation') }}" name="activation" required id="activation" placeholder="Activation">
					</div>
				</div>

				<div class="form-group row">
					<label for="expiration" class="col-xs-2 col-form-label">Expiration</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" value="{{ old('expiration') }}" name="expiration" required id="expiration" placeholder="Expiration">
					</div>
				</div>

				<div class="form-group row">
					<label for="promocode_type" class="col-xs-2 col-form-label">Promocode Type</label>
					<div class="col-xs-10">
						<select name="promocode_type" id="promocode_type" class="form-control" required>
							<option value="">Select Promocode Type</option>
							<option value="firstTime">First Time</option>
							<option value="anyTime">Any Time</option>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label for="rideCount" class="col-xs-2 col-form-label">Ride Count</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" name="rideCount" id="rideCount" placeholder="Ride Count Number Only" required oninput="this.value=(parseInt(this.value)||1)">
					</div>
				</div>


				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Promocode</button>
						<a href="{{route('admin.promocode.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection
@section('scripts')
<script>
	$('#promocode_type').change(function() {
		if($(this).val()=='firstTime'){
			$('#rideCount').val(0);
			$('#rideCount').attr('readonly','readonly');
		} else if($(this).val()=='anyTime'){
			$('#rideCount').val(1);
			$('#rideCount').removeAttr("readonly");
			$('#rideCount').attr('min','1');
		} else {
			$('#rideCount').val('');
			$('#rideCount').removeAttr("readonly");
		}

	});
</script>
@endsection
