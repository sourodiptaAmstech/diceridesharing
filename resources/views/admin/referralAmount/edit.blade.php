@extends('admin.layout.base')
@section('title', 'Update Referral Amount')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.referral-amount.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>
			<h5 style="margin-bottom: 2em;">Update Referral Amount</h5>

            <form class="form-horizontal" action="{{route('admin.referral-amount.update', $referralAmount->referral_amount_id)}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	<input type="hidden" name="_method" value="PATCH">
				<div class="form-group row">
					<label for="user_type" class="col-xs-2 col-form-label">User Type</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{$referralAmount->user_type}}" name="user_type" id="user_type" readonly>
					</div>
				</div>
                <div class="form-group row">
					<label for="referral_amount_sent_by" class="col-xs-2 col-form-label">Referral Amount Sent By(₦)</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{$referralAmount->referral_amount_sent_by}}" name="referral_amount_sent_by" required id="referral_amount_sent_by" placeholder="Referral amount sent by">
					</div>
				</div>
                <div class="form-group row">
					<label for="referral_amount_used_by" class="col-xs-2 col-form-label">Referral Amount Used By(₦)</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{$referralAmount->referral_amount_used_by}}" name="referral_amount_used_by" required id="referral_amount_used_by" placeholder="Referral amount used by">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update</button>
						<a href="{{route('admin.referral-amount.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
@endsection
