@extends('admin.layout.base')
@section('title', 'Referral Amounts')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Referral Amounts
            </h5>
            <table class="table table-striped table-bordered dataTable" id="table-amount">
                <thead>
                    <tr>
                        <th>User Type</th>
                        <th>Referral Amount Send By(₦)</th>
                        <th>Referral Amount Use By(₦)</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($referralAmounts as $index => $referralAmount)
                        <tr>
                            <td>{{ $referralAmount->user_type }}</td>
                            <td>{{ $referralAmount->referral_amount_sent_by }}</td>
                            <td>{{ $referralAmount->referral_amount_used_by }}</td>
                            <td>
                            @can('referral_amount_edit')
                                <a href="{{ route('admin.referral-amount.edit', $referralAmount->referral_amount_id) }}" class="btn btn-info">Edit</a>
                            @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              
                <tfoot>
                    <tr>
                        <th>User Type</th>
                        <th>Referral Amount Send By(₦)</th>
                        <th>Referral Amount Use By(₦)</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
 $(document).ready(function() {
    $('#table-amount').DataTable({
        bFilter: false,
        bInfo: false,
        bPaginate: false,
    });
 });
</script>
@endsection