@extends('admin.layout.base')
@section('title', 'Add Taxi Dispatch')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.taxi-dispatch.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Taxi Dispatch</h5>

            <form class="form-horizontal" action="{{route('admin.taxi-dispatch.store')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}

				<div class="form-group row">
					<label for="user_name" class="col-xs-2 col-form-label">User Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('user_name') }}" name="user_name" required id="user_name" placeholder="User Name">
					</div>
				</div>

                <div class="form-group row">
					<label for="contact_number" class="col-xs-2 col-form-label">Contact Number</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('contact_number') }}" name="contact_number" required id="contact_number" placeholder="Contact Number">
					</div>
				</div>

                <div class="form-group row">
					<label for="email_address" class="col-xs-2 col-form-label">Email Address</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" value="{{ old('email_address') }}" name="email_address" required id="email_address" placeholder="Email Address">
					</div>
				</div>

                <div class="form-group row">
					<label for="pickup_location" class="col-xs-2 col-form-label">Pickup Location</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('pickup_location') }}" name="pickup_location" required id="pickup_location" placeholder="Pickup Location" onFocus="initMap('pickup_location')">
					</div>
				</div>

                <div class="form-group row">
					<label for="drop_location" class="col-xs-2 col-form-label">Drop Location</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('drop_location') }}" name="drop_location" required id="drop_location" placeholder="Drop Location" onFocus="initMap('drop_location')">
					</div>
				</div>

                <div class="form-group row">
					<label for="amount" class="col-xs-2 col-form-label">Amount(₦)</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{ old('amount') }}" name="amount" required id="amount" placeholder="Amount">
					</div>
				</div>

                <div class="form-group row">
					<label for="driver_service" class="col-xs-2 col-form-label">Driver Service</label>
					<div class="col-xs-10">
                        <select name="driver_service" id="driver_service" class="form-control">
                            <option value="">Select driver service</option>
                        @foreach ($driver_services as $driver_service)
                           
                            <option value="{{$driver_service->user_id}}">{{$driver_service->first_name}} {{$driver_service->last_name}} ({{$driver_service->make}}-{{$driver_service->model}})</option>
                        @endforeach
                        </select>
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Taxi Dispatch</button>
						<a href="{{route('admin.taxi-dispatch.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
<script>
function initMap(fieldID) {
    var input = document.getElementById(fieldID);
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
    });
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}&libraries=places"></script>
@endsection
