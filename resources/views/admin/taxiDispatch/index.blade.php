@extends('admin.layout.base')

@section('title', 'Taxi Dispatch')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Taxi Dispatch
            </h5>
            @can('taxi_dispatch_create')
            <a href="{{ route('admin.taxi-dispatch.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Taxi Dispatch</a>
            @endcan
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>User Name</th>
                        <th>Contact Number</th>
                        <th>Email Address</th>
                        <th>Pickup Location</th>
                        <th>Drop Location</th>
                        <th>Amount(₦)</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($taxiDispatchs as $index => $taxiDispatch)
                        <tr>
                            <td>{{ $taxiDispatch->user_name }}</td>
                            <td>{{ $taxiDispatch->contact_number }}</td>
                            <td>{{ $taxiDispatch->email_address }}</td>
                            <td>{{ $taxiDispatch->pickup_location }}</td>
                            <td>{{ $taxiDispatch->drop_location }}</td>
                            <td>{{ $taxiDispatch->amount }}</td>
                            <td>
                            @can('taxi_dispatch_edit')
                                <a href="{{ route('admin.taxi-dispatch.edit', $taxiDispatch->taxi_dispatch_id) }}" class="btn btn-info">Edit</a>
                            @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              
                <tfoot>
                    <tr>
                        <th>User Name</th>
                        <th>Contact Number</th>
                        <th>Email Address</th>
                        <th>Pickup Location</th>
                        <th>Drop Location</th>
                        <th>Amount(₦)</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection