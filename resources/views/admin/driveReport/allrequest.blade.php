@extends('admin.layout.base')

@section('title', 'Payment Report')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Payment Report</h5>
                <input type="hidden" value="{{$type}}" id="type_hidden">

                <p><a href="{{ route('admin.payment','all') }}" class="{{ Request::is('admin/payment/all') ? 'actives' : '' }}"><span class="sort">Over All</span></a> | <a href="{{ route('admin.payment','today') }}" class="{{ Request::is('admin/payment/today') ? 'actives' : '' }}"><span class="sort">Today</span></a> | <a href="{{ route('admin.payment','yesterday') }}" class="{{ Request::is('admin/payment/yesterday') ? 'actives' : '' }}"><span class="sort">Yesterday</span></a> | <a href="{{ route('admin.payment','current-week') }}" class="{{ Request::is('admin/payment/current-week') ? 'actives' : '' }}"><span class="sort">Current Week</span></a> | <a href="{{ route('admin.payment','previous-week') }}" class="{{ Request::is('admin/payment/previous-week') ? 'actives' : '' }}"><span class="sort">Previous Week</span></a> | <a href="{{ route('admin.payment','current-month') }}" class="{{ Request::is('admin/payment/current-month') ? 'actives' : '' }}"><span class="sort">Current Month</span></a> | <a href="{{ route('admin.payment','previous-month') }}" class="{{ Request::is('admin/payment/previous-month') ? 'actives' : '' }}"><span class="sort">Previous Month</span></a> | <a href="{{ route('admin.payment','current-year') }}" class="{{ Request::is('admin/payment/current-year') ? 'actives' : '' }}"><span class="sort">Current Year</span></a> | <a href="{{ route('admin.payment','previous-year') }}" class="{{ Request::is('admin/payment/previous-year') ? 'actives' : '' }}"><span class="sort">Previous Year</span></a></p>

                <div class="row my-2">
                    <div class="col-lg-12 col-12">
                        <div class="row">
                            <div class="col-lg-5 col-5 pr-0">
                                <input type="text" onfocus="(this.type='date')" onblur="(this.type='')"  name="from_date" id="from_date" class="pull-left form-control" style="width:49%;" placeholder="From Date"/>
                                <input type="text" onfocus="(this.type='date')" onblur="(this.type='')" name="to_date" id="to_date" class="pull-right form-control" style="width:49%;" placeholder="To Date"/>
                            </div>
                            <div class="col-lg-1 col-1">
                                <button type="button"  name="filter" id="filter" class="btn btn-info btn-block btn-sm pull-left" style="padding: 5px;">Search</button>
                            </div>
                        </div>
                    </div>
                </div>


                <table class="table table-striped table-bordered dataTable" id="table-allrequest">
                    <thead>
                        <tr>
                            <th>Driver Name</th>
                            <th>Rider Name</th>
                            <th>Booking Number</th>
                            <th>Trip Date</th>
                            <th>Total Fare(₦)</th>
                            <th>Promo Code Discount(₦)</th>
                            <th>Payment Method</th>
                            <th>Payment Status</th>
                            <th>Booking Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="TBody"></tbody>
                    <tfoot>
                        <tr>
                            <th>Driver Name</th>
                            <th>Rider Name</th>
                            <th>Booking Number</th>
                            <th>Trip Date</th>
                            <th>Total Fare(₦)</th>
                            <th>Promo Code Discount(₦)</th>
                            <th>Payment Method</th>
                            <th>Payment Status</th>
                            <th>Booking Status</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
    <style>
        .sort{
            color: #3a8e88;
            cursor: pointer;
        }
        .sort:hover{
            font-weight: bold;
        }
        .actives{
            text-decoration: underline;
            color: #3a8e88;
        }
    </style>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        var type = $('#type_hidden').val();
        $('#table-allrequest').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.ajax-all-request')}}",
                "dataType":"json",
                "type":"POST",
                "data":{ "_token":"<?= csrf_token() ?>", "type": type},
            },
            "columns":[
                {"data":"driver_name"},
                {"data":"customer_name"},
                {"data":"booking_number"},
                {"data":"created_at"},
                {"data":"cost"},
                {"data":"promo_code_value"},
                {"data":"payment_method"},
                {"data":"payment_status"},
                {"data":"booking_status"},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        });

        $('#filter').click(function(){
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if(from_date != '' &&  to_date != '')
            {
                $('#from_date').css({'border':''});
                $('#to_date').css({'border':''});
                var dataObject={"data":[]};
                $.ajax({
                    url: '{{ route('admin.payment.search.daterange') }}',
                    method: 'GET',
                    data: { from_date: from_date, to_date: to_date },
                    success: function(response){
                        console.log(response)
                        var objectLength=Object.keys(response.serviceRequests).length;
                        console.log(objectLength)
                        if (objectLength>0) {
                            for(var i=0; i<objectLength; i++){

                                dataObject.data.push(
                                [
                                response.serviceRequests[i].dfirst_name+' '+response.serviceRequests[i].dlast_name,
                                response.serviceRequests[i].first_name+' '+response.serviceRequests[i].last_name,
                                response.serviceRequests[i].request_no,
                                moment(response.serviceRequests[i].created_at).format('YYYY-MM-DD, hh:mm A'),
                                response.serviceRequests[i].cost,
                                response.serviceRequests[i].promo_code_value,
                                response.serviceRequests[i].payment_method,
                                response.serviceRequests[i].payment_status,
                                '<div style="background-color: #91fc6a;"><p style="color:black;">'+response.serviceRequests[i].request_status+'</p></div>',
                                '<a href="details/'+response.serviceRequests[i].request_id+'/all" class="btn btn-info"> View</a>'
                                ]);
                            }
                            $('#table-allrequest').DataTable().clear().destroy();
                            $('#table-allrequest').DataTable({
                                responsive: true,
                                dom: 'Bfrtip',
                                bFilter: false,
                                bInfo: false,
                                buttons: [
                                    'copyHtml5',
                                    'excelHtml5',
                                    'csvHtml5',
                                    'pdfHtml5'
                                ],
                                "aoColumnDefs": [
                                    { "bSortable": false, "aTargets": [9] }
                                ],
                                "bFilter":true,
                                "bInfo" : true,
                                'data': dataObject.data
                            });
                        } else {
                            $('#table-allrequest').DataTable().clear().destroy();
                            $('#table-allrequest').DataTable({
                                responsive: true,
                                dom: 'Bfrtip',
                                bFilter: false,
                                bInfo: false,
                                buttons: [
                                    'copyHtml5',
                                    'excelHtml5',
                                    'csvHtml5',
                                    'pdfHtml5'
                                ],
                                "aoColumnDefs": [
                                    { "bSortable": false, "aTargets": [9] }
                                ],
                                "bFilter":true,
                                "bInfo" : true
                            });
                            $('#TBody').css('text-align','center').html('<tr><td colspan="10">No Result Found</td></tr>');
                        }
                    }
                });
            } else {
              $('#from_date').css({'border':'1px solid red'});
              $('#to_date').css({'border':'1px solid red'});
            }
        });

    });

</script>
@endsection