<style>
.with-sub a ~ ul li a{
	position:relative !important;
}
.with-sub a ~ ul li a:after {
    content: "";
    width: 10px;
    height: 10px;
    background: #989898;
    z-index: 999;
    position: absolute;
    border-radius: 50%;
    left: 30px;
    top: 12px;
}
.site-sidebar .sidebar-menu > li.with-sub > a .s-caret{
	transform: rotate(0deg);
}
.site-sidebar .sidebar-menu > li.with-sub.active > a .s-caret{
	transform: rotate(180deg);
}
</style>
<div class="site-sidebar">
	<div class="custom-scroll custom-scroll-light">
		<ul class="sidebar-menu">
			@can('dashboard_access')
				<li class="menu-title">Admin Dashboard</li>
				<li>
					<a href="{{ route('admin.dashboard.index') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="fas fa-tachometer-alt"></i></span>
						<span class="s-text">Dashboard</span>
					</a>
				</li>
			@endcan
				
			@can('rbac_management')
				<li class="menu-title">RBAC Management</li>

				<li class="with-sub">
					<a href="javascript:void(0)" class="waves-effect  waves-light">
						<span class="s-caret"><i class="fa fa-angle-down"></i></span>
						{{-- <span class="s-icon"><i class="ti-crown"></i></span> --}}
						<span class="s-icon"><i class="fas fa-user-alt"></i></span>
						
						<span class="s-text">Admin</span>
					</a>
					<ul>
						<li><a href="{{ route('admin.sub-admin.index') }}">Admin Users</a></li>
						<li><a href="{{ route('admin.sub-admin.create') }}">Add New Admin Group</a></li>
					</ul>
				</li>
				<li class="with-sub">
					<a href="javascript:void(0)" class="waves-effect  waves-light">
						<span class="s-caret"><i class="fa fa-angle-down"></i></span>
						<span class="s-icon"><i class="fab fa-critical-role"></i></span>
						<span class="s-text">Roles</span>
					</a>
					<ul>
						<li><a href="{{ route('admin.role.index') }}">List Roles</a></li>
						<li><a href="{{ route('admin.role.create') }}">Add New Roles</a></li>
					</ul>
				</li>
				
			@endcan

				<li class="menu-title">Members</li>
			@can('passenger_access')
				<li class="with-sub">
					<a href="javascript:void(0)" class="waves-effect  waves-light">
						<span class="s-caret"><i class="fa fa-angle-down"></i></span>
						<span class="s-icon"><i class="fas fa-user-alt"></i></span>
						<span class="s-text">Rider</span>
					</a>
					<ul>
					@can('passenger_list')
						<li><a href="{{ route('admin.passenger.index') }}">List Riders</a></li>
					@endcan
					@can('passenger_create')
						<li><a href="{{ route('admin.passenger.create') }}">Add New Rider</a></li>
					@endcan	
					</ul>
				</li>
			@endcan

			@can('driver_access')
				<li class="with-sub">
					<a href="javascript:void(0)" class="waves-effect  waves-light">
						<span class="s-caret"><i class="fa fa-angle-down"></i></span>
						<span class="s-icon"><i class="fas fa-user-alt"></i></span>
						<span class="s-text">Driver</span>
					</a>
					<ul>
					@can('driver_list')
						<li><a href="{{ route('admin.driver.index') }}">List Drivers</a></li>
					@endcan
					@can('driver_create')
						<li><a href="{{ route('admin.driver.create') }}">Add New Driver</a></li>
					@endcan
					</ul>
				</li>
			@endcan
				
			<li class="menu-title">General</li>
			<li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="fa fa-bar-chart" aria-hidden="true"></i></span>
					<span class="s-text">Report</span>
				</a>
				<ul>
				@can('all_request_access')
					<li><a href="{{ route('admin.payment','all') }}">Payment</a></li>
				@endcan	
				@can('no_service_found_access')
					<li><a href="{{ route('admin.request.nofound') }}">No Service Found</a></li>
				@endcan	
				@can('rejected_request_access')
					<li><a href="{{ route('admin.request.rejected') }}">Rejected Request</a></li>
				@endcan
				@can('ride_later_bookings_access')
					<li><a href="{{ route('admin.request.schedule') }}">Ride Later Bookings</a></li>
				@endcan
				@can('cancel_by_passenger_access')
					<li><a href="{{ route('admin.passenger.cancel.report') }}">Cancel By Rider</a></li>
				@endcan
				@can('cancel_by_driver_access')
					<li><a href="{{ route('admin.driver.cancel.report') }}">Cancel By Driver</a></li>
				@endcan
					<li><a href="{{ route('admin.earning.report') }}">Admin Earning</a></li>
				</ul>
			</li>

			@can('vehicle_access')
				<li class="with-sub">
					<a href="#" class="waves-effect  waves-light">
						<span class="s-caret"><i class="fa fa-angle-down"></i></span>
						<span class="s-icon"><i class="ti-car"></i></span>
						<span class="s-text">Vehicle</span>
					</a>
					<ul>
					@can('vehicle_list')
						<li><a href="{{ route('admin.vehicle.index') }}">List Vehicles</a></li>
					@endcan
					@can('vehicle_create')
						<li><a href="{{ route('admin.vehicle.create') }}">Add New Vehicle</a></li>
					@endcan
					</ul>
				</li>
			@endcan
			@can('city_access')
				<li class="with-sub">
					<a href="#" class="waves-effect  waves-light">
						<span class="s-caret"><i class="fa fa-angle-down"></i></span>
						<span class="s-icon"><i class="fas fa-city"></i></span>
						<span class="s-text">City</span>
					</a>
					<ul>
					@can('city_list')
						<li><a href="{{ route('admin.city.index') }}">List City</a></li>
					@endcan
					@can('city_create')
						<li><a href="{{ route('admin.city.create') }}">Add New City</a></li>
					@endcan
					</ul>
				</li>
			@endcan	
			@can('service_type_access')
				<li class="with-sub">
					<a href="javascript:void(0)" class="waves-effect  waves-light">
						<span class="s-caret"><i class="fa fa-angle-down"></i></span>
						<span class="s-icon"><i class="ti-view-grid"></i></span>
						<span class="s-text">Service Types</span>
					</a>
					<ul>
					@can('service_type_list')
						<li><a href="{{ route('admin.service.index') }}">List Service Types</a></li>
					@endcan
					@can('service_type_create')
						<li><a href="{{ route('admin.service.create') }}">Add New Service Type</a></li>
					@endcan
					</ul>
				</li>
			@endcan
			@can('document_access')
				<li class="with-sub">
					<a href="javascript:void(0)" class="waves-effect  waves-light">
						<span class="s-caret"><i class="fa fa-angle-down"></i></span>
						<span class="s-icon"><i class="ti-layout-tab"></i></span>
						<span class="s-text">Documents</span>
					</a>
					<ul>
					@can('document_list')
						<li><a href="{{ route('admin.document.index') }}">List Documents</a></li>
					@endcan
					@can('document_create')
						<li><a href="{{ route('admin.document.create') }}">Add New Document</a></li>
					@endcan
					</ul>
				</li>
			@endcan
			
			@can('promocode_access')
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="fa fa-usd" aria-hidden="true"></i></span>
					<span class="s-text">Promocodes</span>
				</a>
				<ul>
				@can('promocode_list')
					<li><a href="{{ route('admin.promocode.index') }}">List Promocodes</a></li>
				@endcan
				@can('promocode_create')
					<li><a href="{{ route('admin.promocode.create') }}">Add New Promocode</a></li>
				@endcan
				</ul>
			</li>
			@endcan
			@can('referral_amount_access')
				<li class="with-sub">
					<a href="#" class="waves-effect  waves-light">
						<span class="s-caret"><i class="fa fa-angle-down"></i></span>
						<span class="s-icon"><i class="fa fa-usd" aria-hidden="true"></i></span>
						<span class="s-text">Referral Amount</span>
					</a>
					<ul>
					@can('referral_amount_list')
						<li><a href="{{ route('admin.referral-amount.index') }}">List Referral Amount</a></li>
					@endcan
					</ul>
				</li>
			@endcan
			@can('taxi_dispatch_access')
				<li class="with-sub">
					<a href="#" class="waves-effect  waves-light">
						<span class="s-caret"><i class="fa fa-angle-down"></i></span>
						<span class="s-icon"><i class="ti-car"></i></span>
						<span class="s-text">Taxi Dispatch</span>
					</a>
					<ul>
					@can('taxi_dispatch_list')
						<li><a href="{{ route('admin.taxi-dispatch.index') }}">List Taxi Dispatch</a></li>
					@endcan
					@can('taxi_dispatch_create')
						<li><a href="{{ route('admin.taxi-dispatch.create') }}">Add New Taxi Dispatch</a></li>
					@endcan
					</ul>
				</li>
			@endcan
			@can('cancel_reason')
			<li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-layout-tab"></i></span>
					<span class="s-text">Cancel Reasons</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.cancel-reason.index') }}">List Reasons</a></li>
					<li><a href="{{ route('admin.cancel-reason.create') }}">Add New Reason</a></li>
				</ul>
			</li>
			@endcan

			<li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-layout-tab"></i></span>
					<span class="s-text">Report an issue</span>
				</a>
				<ul>
					@can('report_subject')
					<li class="with-sub">
						<a href="javascript:void(0)" class="waves-effect  waves-light">
						
							<span class="s-text">Create Subject ?</span>
							<span class="s-caret" style="float: right;"><i class="fa fa-angle-down"></i></span>
						</a>
						<ul>
							<li><a href="{{route('admin.reportIssue.customer.subject.list')}}">Rider</a></li>
							<li><a href="{{route('admin.reportIssue.driver.subject.list')}}">Driver</a></li>
						</ul>
					</li>
					@endcan
					@can('report_issue')
					<li class="with-sub">
						<a href="javascript:void(0)" class="waves-effect  waves-light">
							
							<span class="s-text">View Report an issue</span>
							<span class="s-caret" style="float: right;"><i class="fa fa-angle-down"></i></span>
						</a>
						<ul>
							<li><a href="{{route('admin.reportIssue.customer.list')}}">By Rider</a></li>
							<li><a href="{{route('admin.reportIssue.driver.list')}}">By Driver</a></li>
						</ul>
					</li>
					@endcan
				</ul>
			</li>

			@can('review_management')
			<li>
				<a href="{{ route('admin.review.driver') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="fa fa-file-text" aria-hidden="true"></i></span>
					<span class="s-text">Review Management</span>
				</a>
			</li>
			@endcan
			@can('subadmin_log')
			<li>
				<a href="{{ route('admin.subadmin.log') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="fa fa-file-text" aria-hidden="true"></i></span>
					<span class="s-text">Sub-admin Log</span>
				</a>
			</li>
			@endcan
			
			@can('site_setting_access')
			<li class="menu-title">Settings</li>
			<li>
				<a href="{{ route('admin.setting.index') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-settings"></i></span>
					<span class="s-text">Site Settings</span>
				</a>
			</li>
			@endcan

			@can('cms_management_access')
			<li class="menu-title">Others</li>
			<li>
				<a href="{{ route('admin.privacy.policy') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="fa fa-file-text" aria-hidden="true"></i></span>
					<span class="s-text">CMS Management</span>
				</a>
			</li>
			@endcan
			@can('push_notification')
			<li>
				<a href="{{ route('admin.send.push-notification') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="fa fa-bell" aria-hidden="true"></i></span>
					<span class="s-text">Push Notification</span>
				</a>
			</li>
			@endcan

			@can('passenger_chat_support')
			<li>
				<a href="{{ route('admin.passenger.chat.support') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="fa fa-bell" aria-hidden="true"></i></span>
					<span class="s-text">Passenger Chat Support</span>
				</a>
			</li>
			@endcan

			{{-- <li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="far fa-comment-dots"></i></span>
					<span class="s-text">Chat Support</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.passenger.chat.support') }}">Chat With Passenger</a></li>
					<li><a href="{{ route('admin.driver.chat.support') }}">Chat With Driver</a></li>
				</ul>
			</li> --}}

			<li class="menu-title">Account</li>
			@can('admin_profile_access')
			<li>
				<a href="{{ route('admin.profile') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="fa fa-wrench" aria-hidden="true"></i></span>
					<span class="s-text">Account Settings</span>
				</a>
			</li>
			@endcan
			@can('admin_change_password')
			<li>
				<a href="{{ route('admin.password') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="fa fa-key" aria-hidden="true"></i></span>
					<span class="s-text">Change Password</span>
				</a>
			</li>
			@endcan
			<li class="compact-hide">
				<a href="{{ url('/admin/logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
					<span class="s-icon"><i class="ti-power-off"></i></span>
					<span class="s-text">Logout</span>
                </a>

                <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
			</li>
		</ul>
	</div>
</div>
