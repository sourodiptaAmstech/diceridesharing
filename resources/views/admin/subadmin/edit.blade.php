@extends('admin.layout.base')

@section('title', 'Update Admin User ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    		<a href="{{ route('admin.sub-admin.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Admin User</h5>

            <form class="form-horizontal" action="{{route('admin.sub-admin.update',$sub_admin->id)}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
				<div class="form-group row">
					<label for="first_name" class="col-xs-2 col-form-label">First Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $sub_admin->admin_profile->first_name }}" name="first_name" id="first_name" placeholder="First Name" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="last_name" class="col-xs-2 col-form-label">Last Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $sub_admin->admin_profile->last_name }}" name="last_name" required id="last_name" placeholder="Last Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="email" class="col-xs-2 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" required name="email" value="{{ isset($sub_admin->admin_profile->email_id) ? $sub_admin->admin_profile->email_id : '' }}" id="email" placeholder="Email">
					</div>
				</div>

				<div class="form-group row">
					<label for="mobile" class="col-xs-2 col-form-label">Mobile</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{$sub_admin->admin_profile->mobile_no}}" name="mobile" required id="mobile" placeholder="Mobile">
						<input type="hidden" name="code" id="code" value="{{$sub_admin->admin_profile->isd_code}}">
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-2 col-form-label">Picture</label>
					<div class="col-xs-10">
						@if(isset($sub_admin->admin_profile->picture))
	                    	@if(File::exists(storage_path('app/public' .str_replace("storage", "", $sub_admin->admin_profile->picture))))
                                <img style="height: 90px; margin-bottom: 15px;" src="{{URL::asset($sub_admin->admin_profile->picture)}}">
                            @else
                                <img style="height: 60px; margin-bottom: 15px;" src="{{URL::asset('asset/NO_IMG.png')}}">
                            @endif
	                   	@else
                     		<img style="height: 60px; margin-bottom: 15px;" src="{{URL::asset('asset/NO_IMG.png')}}">
	                    @endif
						<input type="file" accept="image/*" name="picture" class="dropify form-control-file" aria-describedby="fileHelp">
					</div>
				</div>

                <div class="form-group row">
                    <label for="roles" class="col-xs-2 col-form-label">Roles
                    
                    </label>
                    <div class="col-xs-10">
						<div class="mb-1">
							<span class="btn btn-info btn-xs select-all">Select All</span>
							<span class="btn btn-info btn-xs deselect-all">Deselect All</span>
						</div>
                        <select name="roles[]" id="roles" class="form-control select2" multiple="multiple" required>
                            @foreach($roles as $id => $roles)
                                <option value="{{ $id }}" {{ (in_array($id, old('roles', [])) || isset($sub_admin) && $sub_admin->roles->contains($id)) ? 'selected' : '' }}>{{ $roles }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Admin User</button>
						<a href="{{route('admin.sub-admin.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
<style type="text/css">.iti { width: 100%; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
<script>
	jq = jQuery.noConflict();
    use :
   		var s = jq("#mobile").intlTelInput({
   			autoPlaceholder: 'polite',
   			separateDialCode: true,
   			formatOnDisplay: true,
   			initialCountry: 'ng',
   			preferredCountries:["ng"]
   		});
   	insteadof :
   		var countryData = window.intlTelInputGlobals.getCountryData();
   		var iso2;
		var isdcode = jq("#code").val();
		for (var i = 0; i < countryData.length; i++) {
			if (countryData[i].dialCode == parseInt(isdcode)){
				iso2 = countryData[i].iso2;
				break;
			}
		}
		if(iso2){
			jq("#mobile").intlTelInput("setCountry", iso2);
		}
		jq(document).on('countrychange', function (e, countryData) {
        	jq("#code").val((jq("#mobile").intlTelInput("getSelectedCountryData").dialCode));
    	});
</script>
@endsection
