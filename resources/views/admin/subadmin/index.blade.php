@extends('admin.layout.base')

@section('title', 'Admin Users')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Admin Users
            </h5>
            @can('rbac_management')
            <a href="{{ route('admin.sub-admin.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Admin Group</a>
            @endcan
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sub_admins as $index => $sub_admin)
                    <tr>
                        <td>{{ $index + 1 }}</td>

                        <td>{{ $sub_admin->first_name }} {{ $sub_admin->last_name }}</td>
                        <td>{{ $sub_admin->email_id }}</td>
                        <td>{{ $sub_admin->isd_code }}-{{ $sub_admin->mobile_no }}</td>

                        {{-- <td>{{ $sub_admin->admin_profile->first_name }} {{ $sub_admin->admin_profile->last_name }}</td>
                        <td>{{ $sub_admin->admin_profile->email_id }}</td>
                        <td>{{ $sub_admin->admin_profile->isd_code }}-{{ $sub_admin->admin_profile->mobile_no }}</td> --}}

                        @if(count($sub_admin->user->roles)>0)
                        <td>
                            @foreach($sub_admin->user->roles as $role)
                            {{ $role->title }},
                            @endforeach
                        </td>
                        @else
                        <td></td>
                        @endif
                        <td>
                            @can('rbac_management')
                                <a href="{{ route('admin.sub-admin.edit', $sub_admin->user_id) }}" class="btn btn-info">Edit</a>
                                @if ($sub_admin->user->admin_user_status == "ACTIVE")
                                    {{-- <div id="admin-active{{$sub_admin->user_id}}"> --}}
                                    <a class="adminStatus btn btn-success btn-xs" data-id="{{$sub_admin->user_id}}" data-value="block" href="javascript:viod(0)"
                                        data-toggle="tooltip" title="Click to Block">Active</a>
                                    {{-- </div> --}}
                                @else
                                    {{-- <div id="admin-block{{$sub_admin->user_id}}"> --}}
                                    <a class="adminStatus btn btn-danger btn-xs" data-id="{{$sub_admin->user_id}}" data-value="activate" href="javascript:viod(0)"
                                        data-toggle="tooltip" title="Click to Active">Block</a>
                                    {{-- </div> --}}
                                @endif
                            @endcan
                            {{-- <form action="{{ route('admin.passenger.destroy', $passenger->id) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                            </form> --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $(document).on('click','.adminStatus', function(e){
            e.preventDefault();
            var id = $(this).data("id");
            var value = $(this).data("value");
            bootbox.confirm('Are you sure to '+value+' sub-admin?',function(result){
                if (result) {
                $.ajax({
                    url: "subadmin/active/"+id,
                    type: 'GET',
                    data: { "id": id, 'value': value },
                    success: function (response){
                        location.reload();
                        //console.log(response.subadmin);
                        //if(response.subadmin.admin_user_status=="ACTIVE"){
                            //alert('active');
                            //$('.mb-1').html('<p>hrlollo</p>');
                            //$('#admin-active'+response.subadmin.id).html('');
                            //var buttonActive = `<a class="adminStatus btn btn-success btn-xs" data-id="`+response.subadmin.id+`" data-value="block" href="javascript:viod(0)"
                                       // data-toggle="tooltip" title="Click to Block">Active</a>`;
                            //alert(buttonActive);
                            //$('#admin-active'+response.subadmin.id).html(buttonActive);
                       // }
                       // if(response.subadmin.admin_user_status=="BLOCK"){
                            //alert('block');
                            //$('#admin-block'+response.subadmin.id).html('');
                            //var buttonBlock = `<a class="adminStatus btn btn-danger btn-xs" data-id="`+response.subadmin.id+`" data-value="activate" href="javascript:viod(0)"
                                       // data-toggle="tooltip" title="Click to Active">Block</a>`;
                            //alert(buttonBlock);
                           // $('#admin-block'+response.subadmin.id).html(buttonBlock);
                       // }
                    },
                    error: function (response){
                        //$('.msg_err').hide();
                        //$('.msg_suc').hide();
                        //$('.inactive_doc').show();
                        //$('.inactive_msg').html(response.responseJSON.message);
                    }
                });
              }
            });
        });
    });
</script>
@endsection