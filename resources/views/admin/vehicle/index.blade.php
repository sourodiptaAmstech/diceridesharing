@extends('admin.layout.base')

@section('title', 'Vehicles ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Vehicles
            </h5>
            @can('vehicle_create')
            <a href="{{ route('admin.vehicle.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Vehicle</a>
            @endcan
            <table class="table table-striped table-bordered dataTable" id="table-id">
                <thead>
                    <tr>
                        <th>Year</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Action</th>
                    </tr>
                </thead>
              
                <tfoot>
                    <tr>
                        <th>Year</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#table-id').DataTable( {
            "order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.dataProcessing')}}",
                "dataType":"json",
                "type":"POST",
                "data":{"_token":"<?= csrf_token() ?>"}
            },
            "columns":[
                {"data":"year"},
                {"data":"make"},
                {"data":"model"},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        } );

    $(document).ready(function(){
        $(document).on('click','.deleteV', function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id = $(this).data("id");
            bootbox.confirm('Do you really want to delete?',function(result){
                if (result) {
                    $.ajax({
                        url: "vehicle/"+id,
                        type: 'DELETE',
                        data: {
                                "id": id
                            },
                        success: function (response){
                            location.reload();
                        },
                    });
                }
            });
        });
    });

    </script>

@endsection