<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email template</title>
</head>
<body>
    <table style="width: 600px; box-sizing: border-box; border-spacing: 0px; border: 1px solid #ccc;">
        <thead>
            <tr>
                <td style="text-align: left; padding: 10px  20px; border-bottom: 2px solid #ff9800; ">
                    <img style="max-width: 50px;" src="{{env("baseURL")}}asset/img/dice_logo.jpeg" alt="">
                </td>
                <td style="text-align: right; padding: 10px 20px; border-bottom: 2px solid #ff9800;  color: #777;">
                    {{$invoice->date}}
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="font-size: 14px; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">{{$invoice->request_no}}</td>
                <td style="font-size: 40px; text-align: right; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">
                    {{$invoice->currency}}{{$invoice->total}}
                </td>
            </tr>
            <tr>
                <td style="font-size: 14px; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">Total <br>
                <span style="color: #777;">
                    Discount Code
                </span></td>
                <td style="font-size: 14px; text-align: right; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">
                    {{$invoice->currency}}{{$invoice->total}}<br>
                <span style="color: #777;">
                   - {{$invoice->currency}}{{$invoice->promo_discount}}
                </span></td>
            </tr>
            <tr>
                {{-- &#8358; --}}
                <td style="font-size: 14px; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">Charged</td>
                <td style="font-size: 14px; text-align: right; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">
                     {{$invoice->currency}}{{$invoice->total}}</td>
            </tr>
            <tr>
                <td style="font-size: 14px; padding: 10px 20px; color: #777; border-bottom: 1px solid #f2f2f2">Ride Distance  <br>
                <span style="color: #777;">
                    Ride Duration 
                </span></td>
                <td style="font-size: 14px; text-align: right; padding: 10px 20px; color: #777; border-bottom: 1px solid #f2f2f2">
                    {{$invoice->distance}} km<br>
                    <span style="color: #777;">
                       {{$invoice->duration_hr}}:{{$invoice->duration_min}}:{{$invoice->duration_sec}}
                    </span>
                </td>
            </tr>
            <tr>
                <td style="font-size: 14px; padding: 10px 20px; color: #777; border-bottom: 1px solid #f2f2f2">Payment Method</td>
                <td style="font-size: 14px; text-align: right; padding: 10px 20px; color: #777; border-bottom: 1px solid #f2f2f2">
                    {{$invoice->payment_method}}<br>
                </td>
            </tr>
            @if($invoice->payment_method=='CARD')
            <tr>
                <td style="font-size: 14px; padding: 10px 20px; color: #777; border-bottom: 1px solid #f2f2f2">Card's Last Four Digits</td>
                <td style="font-size: 14px; text-align: right; padding: 10px 20px; color: #777; border-bottom: 1px solid #f2f2f2">
                    {{$invoice->last_four}}<br>
                </td>
            </tr>
            @endif
            <tr>
                <td colspan="2" style="padding: 8px; border-bottom: 1px solid #ccc; border-top: 1px solid #ccc;"></td>
            </tr>
            <tr>
                <td style="font-size: 14px; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">{{$invoice->source_address}}</td>
                <td style="font-size: 14px; text-align: right; padding: 10px 20px; color: #777; border-bottom: 1px solid #f2f2f2">
                     {{date('H:i',strtotime($invoice->started_on))}}
                 </td>
            </tr>
            <tr>
                <td style="font-size: 14px; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">{{$invoice->destination_address}}</td>
                <td style="font-size: 14px; text-align: right; padding: 10px 20px; color: #777; border-bottom: 1px solid #f2f2f2">
                    {{date('H:i',strtotime($invoice->reached_on))}}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img style="width: 100%; height: 240px;" src="{{$invoice->static_map}}" frameborder="0">
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>