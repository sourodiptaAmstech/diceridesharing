<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email template</title>
</head>
<body>
    <table style="width: 600px; box-sizing: border-box; border-spacing: 0px; border: 1px solid #ccc;">
        <thead>
            <tr>
                <td style="text-align: left; padding: 10px  20px; border-bottom: 2px solid #ff9800; ">
                    <img style="max-width: 50px;" src="{{env("baseURL")}}asset/img/dice_logo.jpeg" alt="">
                </td>
                <td style="text-align: right; padding: 10px 20px; border-bottom: 2px solid #ff9800;  color: #777;">
                    {{$invoice->date}}
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="font-size: 14px; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2"></td>
                <td style="font-size: 40px; text-align: right; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">
                    ₦{{$invoice->cost}}
                </td>
            </tr>
            <tr>
                <td style="font-size: 14px; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">Total ride amount received <br><br>
                    <span style="color: #777;">
                        Driver's referral balance
                    </span><br><br>
                    <span style="color: #777;">
                        Total cash in hand(driver)
                    </span><br><br>
                    <span style="color: #777;">
                         Total insurance amount
                    </span><br><br>
                    <span style="color: #777;">
                        Total tax amount
                    </span><br><br>
                    <span style="color: #777;">
                        Total commission
                    </span><br><br>
                    <span style="color: #777;">
                        Total passenger cancel charge
                    </span><br><br>
                    <span style="color: #777;">
                        Total driver cancel charge
                    </span>
                    
                </td>
                <td style="font-size: 14px; text-align: right; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">
                    ₦{{$invoice->total_rideamount}}<br><br>
                    <span style="color: #777;">
                       + ₦{{$invoice->referral_bal}}
                    </span><br><br>
                    <span style="color: #777;">
                       - ₦{{$invoice->cashin_hand}}
                    </span><br><br>
                    <span style="color: #777;">
                       - ₦{{$invoice->insorance_amount}}
                    </span><br><br>
                    <span style="color: #777;">
                       - ₦{{$invoice->tax_amount}}
                    </span><br><br>
                    <span style="color: #777;">
                       - ₦{{$invoice->commission_amount}}
                    </span><br><br>
                    <span style="color: #777;">
                       - ₦{{$invoice->passcharge_amount}}
                    </span><br><br>
                    <span style="color: #777;">
                       - ₦{{$invoice->drivercharge_amount}}
                    </span>
                </td>
            </tr>
            <tr>
                {{-- &#8358; --}}
                <td style="font-size: 14px; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">Total payable amount</td>
                <td style="font-size: 14px; text-align: right; padding: 10px 20px; color: #000; border-bottom: 1px solid #f2f2f2">
                     ₦{{$invoice->cost}}</td>
            </tr>
        </tbody>
    </table>
</body>
</html>