<?php

namespace App\Model\ReportIssue;

use Illuminate\Database\Eloquent\Model;

class ReportIssue extends Model
{
    protected $table = 'report_issues';
    protected $primaryKey = 'issues_id';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function reportSubject()
    {
        return $this->belongsTo('App\Model\ReportIssue\ReportSubject','report_subject_id', 'report_subject_id');
    }

    public function driver()
    {
        return $this->belongsTo('App\Model\Profiles\DriverProfiles','user_id', 'user_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Model\Profiles\PassengersProfile','user_id','user_id');
    }
    
}
