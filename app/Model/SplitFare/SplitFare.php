<?php

namespace App\Model\SplitFare;

use Illuminate\Database\Eloquent\Model;

class SplitFare extends Model
{
    protected $table = 'split_fare';
    protected $primaryKey = 'split_fare_id';
}
