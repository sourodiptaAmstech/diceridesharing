<?php

namespace App\Model\SplitFare;

use Illuminate\Database\Eloquent\Model;

class SplitTransactionLog extends Model
{
	protected $table = 'split_transaction_log';
    protected $primaryKey = 'split_transaction_log_id';
}