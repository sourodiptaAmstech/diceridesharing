<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;

class ServiceRequest extends Model
{
  
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_requests';
    protected $primaryKey = 'request_id';

    public function passenger()
    {
        return $this->belongsTo('App\Model\Profiles\PassengersProfile','passenger_id', 'user_id');
    }

    public function driver()
    {
        return $this->belongsTo('App\Model\Profiles\DriverProfiles','driver_id', 'user_id');
    }

    public function driver_subaccount()
    {
        return $this->belongsTo('App\Model\Profiles\DriverSubaccount', 'driver_id', 'user_id');
    }
    
    public function service_type()
    {
        return $this->belongsTo('App\Model\ServiceType\MstServiceType','service_type_id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Model\Transaction\TransactionLog','request_id','request_id');
    }
   
}
