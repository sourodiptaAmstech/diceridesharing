<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;
class ServiceRequestLog extends Model
{
    //
   // use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_request_logs';
    protected $primaryKey = 'service_request_log_id';

    public function passenger()
    {
        return $this->belongsTo('App\Model\Profiles\PassengersProfile','passenger_id', 'user_id');
    }

    public function driver()
    {
        return $this->belongsTo('App\Model\Profiles\DriverProfiles','driver_id', 'user_id');
    }

    public function service_type()
    {
        return $this->belongsTo('App\Model\ServiceType\MstServiceType','service_type_id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Model\Transaction\TransactionLog','request_id','request_id');
    }

    public function service_request()
    {
        return $this->belongsTo('App\Model\Request\ServiceRequest','request_id','request_id');
    }
}
