<?php

namespace App\Model\TaxiDispatch;

use Illuminate\Database\Eloquent\Model;

class TaxiDispatch extends Model
{
    protected $table = 'taxi_dispatch';
    protected $primaryKey = 'taxi_dispatch_id';
}
