<?php

namespace App\Model\Profiles;

use Illuminate\Database\Eloquent\Model;

class AdminsProfile extends Model
{
    protected $table = 'admins_profile';
    protected $primaryKey = 'admin_id';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
