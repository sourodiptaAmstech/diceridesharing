<?php

namespace App\Model\Profiles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class DriverProfiles extends Model
{
    //
    use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'drivers_profile';
    protected $primaryKey = 'driver_id';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function driverServiceType()
    {
        return $this->belongsTo('App\Model\ServiceType\DriverServiceType','user_id', 'user_id');
    }
}
