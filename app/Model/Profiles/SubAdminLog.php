<?php

namespace App\Model\Profiles;

use Illuminate\Database\Eloquent\Model;

class SubAdminLog extends Model
{
    protected $table = 'sub_admin_logs';
    protected $primaryKey = 'sub_admin_log_id';
}
