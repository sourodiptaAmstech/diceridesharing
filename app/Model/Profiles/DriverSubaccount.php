<?php

namespace App\Model\Profiles;

use Illuminate\Database\Eloquent\Model;

class DriverSubaccount extends Model
{
    protected $table = "driver_subaccounts";
    protected $primaryKey = "driver_subaccount_id";
}
