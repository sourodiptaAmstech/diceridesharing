<?php

namespace App\Model\FamilyAccount;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class FamilyAccount extends Model
{
  	//use SoftDeletes;

    protected $table = 'family_accounts';
    protected $primaryKey = 'family_accounts_id';

}
