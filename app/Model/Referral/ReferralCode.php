<?php

namespace App\Model\Referral;

use Illuminate\Database\Eloquent\Model;

class ReferralCode extends Model
{
    protected $table = 'referral_codes';
    protected $primaryKey = 'referral_code_id';
}