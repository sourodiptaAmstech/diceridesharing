<?php

namespace App\Model\Referral;

use Illuminate\Database\Eloquent\Model;

class PassengerReferralUse extends Model
{
    protected $table = 'passenger_referral_uses';
    protected $primaryKey = 'passenger_referral_use_id';
}
