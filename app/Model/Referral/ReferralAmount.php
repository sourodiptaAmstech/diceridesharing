<?php

namespace App\Model\Referral;

use Illuminate\Database\Eloquent\Model;

class ReferralAmount extends Model
{
    protected $table = 'referral_amounts';
    protected $primaryKey = 'referral_amount_id';
}