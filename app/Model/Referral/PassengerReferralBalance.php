<?php

namespace App\Model\Referral;

use Illuminate\Database\Eloquent\Model;

class PassengerReferralBalance extends Model
{
    protected $table = 'passenger_referral_balances';
    protected $primaryKey = 'passenger_referral_balance_id';
}
