<?php

namespace App\Model\Referral;

use Illuminate\Database\Eloquent\Model;

class DriverReferralUse extends Model
{
	protected $table = 'driver_referral_uses';
    protected $primaryKey = 'driver_referral_use_id';
}
