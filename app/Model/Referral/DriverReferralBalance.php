<?php

namespace App\Model\Referral;

use Illuminate\Database\Eloquent\Model;

class DriverReferralBalance extends Model
{
	protected $table = 'driver_referral_balances';
    protected $primaryKey = 'driver_referral_balance_id';
}
