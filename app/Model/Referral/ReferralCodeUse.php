<?php

namespace App\Model\Referral;

use Illuminate\Database\Eloquent\Model;

class ReferralCodeUse extends Model
{
    protected $table = 'referral_code_uses';
    protected $primaryKey = 'referral_code_use_id';
}