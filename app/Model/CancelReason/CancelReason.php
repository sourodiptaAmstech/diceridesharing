<?php

namespace App\Model\CancelReason;

use Illuminate\Database\Eloquent\Model;

class CancelReason extends Model
{
    protected $table = 'cancel_reason';
    protected $primaryKey = 'cancel_reason_id';
}