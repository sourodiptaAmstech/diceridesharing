<?php

namespace App\Model\Payment;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_cards';
    protected $primaryKey = 'user_cards_id';

    public function passenger_profile()
    {
        return $this->belongsTo('App\Model\Profiles\PassengersProfile', 'user_id','user_id');
    }
}
