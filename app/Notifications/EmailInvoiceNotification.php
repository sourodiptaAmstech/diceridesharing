<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmailInvoiceNotification extends Notification
{
    use Queueable;

    protected $subject;
    protected $invoice;
    protected $type;

    public function __construct($sub,$invoice,$type)
    {
        $this->subject = $sub;
        $this->invoice = $invoice;
        $this->type = $type;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        if ($this->type=='passenger'){
            return (new MailMessage)
                    ->subject($this->subject)
                    ->view('invoice-template', ['invoice'=>(object)$this->invoice]);
        }
        if ($this->type=='driver'){
            return (new MailMessage)
                    ->subject($this->subject)
                    ->view('driver-invoice-template', ['invoice'=>(object)$this->invoice]);
        }
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
