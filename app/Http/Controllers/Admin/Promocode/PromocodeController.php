<?php

namespace App\Http\Controllers\Admin\Promocode;

use App\Model\Promocode\Promocode;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Model\Promocode\PromocodeUsage;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;


class PromocodeController extends Controller
{
    
    //promocode listing api
    public function index()
    {
        abort_unless(\Gate::allows('promocode_access'), 403);
        abort_unless(\Gate::allows('promocode_list'), 403);
        $promocodes = Promocode::orderBy('created_at' , 'desc')->where('user_type', 'RIDER')->get();
        return view('admin.promocode.index', compact('promocodes'));
    }

    public function promocodeSearch(Request $request){
        $from_date = $request->from_date;
        $to_date = $request->to_date;
      
        $promocodes = Promocode::whereBetween('created_at', [$from_date, $to_date])->orderBy('created_at' , 'desc')->where('user_type', 'RIDER')->get();

        return response()->json([
            "success" => true,
            "message" => "Promocodes",
            "errors" => array("exception"=>["Everything is OK."]),
            'promocodes' => $promocodes
        ]);
    }

    
    public function create()
    {
        abort_unless(\Gate::allows('promocode_access'), 403);
        abort_unless(\Gate::allows('promocode_create'), 403);
        return view('admin.promocode.create');
    }

    //promocode store function
    public function store(Request $request)
    {
        abort_unless(\Gate::allows('promocode_access'), 403);
        abort_unless(\Gate::allows('promocode_create'), 403);
        $this->validate($request, [
            'promo_code' => 'required|max:100|unique:promocodes',
            'discount' => 'required|numeric',
            'activation' => 'required',
            'expiration' => 'required',
            'promocode_type' => 'required',
            'rideCount' => 'required'
        ]);

        try{
            $Promocode = new Promocode;
            $Promocode->promo_code = $request->promo_code;
            $Promocode->discount = $request->discount;
            $Promocode->activation = $request->activation;
            $Promocode->expiration = $request->expiration;
            $Promocode->promocode_type = $request->promocode_type;
            $Promocode->rideCount = $request->rideCount;
            $Promocode->user_type = 'RIDER';
            $Promocode->save();

            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "add_promocode";
                $request->action_id = $Promocode->promocodes_id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Promocode ".$Promocode->promo_code." is created by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }

            return redirect()->route('admin.promocode.index')->with('flash_success','Promocode Saved Successfully');

        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Promocode Not Found');
        }
    }

    //show promocode function
    public function show($id)
    {
        try {
            $promocode = Promocode::findOrFail($id);
            $PromocodeUsage = PromocodeUsage::where('promocode_id',$promocode->promocodes_id)->get();
            return view('admin.promocode.show',compact('PromocodeUsage'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    //fetch promocode to show in edit page 
    public function edit($id)
    {
        abort_unless(\Gate::allows('promocode_access'), 403);
        abort_unless(\Gate::allows('promocode_edit'), 403);
        try {
            $promocode = Promocode::findOrFail($id);
            return view('admin.promocode.edit',compact('promocode'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    //update function to update promocode
    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('promocode_access'), 403);
        abort_unless(\Gate::allows('promocode_edit'), 403);
        $this->validate($request, [
            'promo_code' => 'required|max:100|unique:promocodes,promo_code,'.$id.',promocodes_id',
            'discount' => 'required|numeric',
            'activation' => 'required',
            'expiration' => 'required',
            'promocode_type' => 'required',
            'rideCount' => 'required'
        ]);

        try {
            $promo = Promocode::findOrFail($id);
            $promo->promo_code = $request->promo_code;
            $promo->discount = $request->discount;
            $promo->activation = $request->activation;
            $promo->expiration = $request->expiration;
            $promo->promocode_type = $request->promocode_type;
            $promo->rideCount = $request->rideCount;
            $promo->save();
            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "edit_promocode";
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Promocode ".$promo->promo_code." is updated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }

            return redirect()->route('admin.promocode.index')->with('flash_success', 'Promocode Updated Successfully');    
        }

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Promocode Not Found');
        }
    }

    //promocode delete function
    public function destroy(Request $request,$id)
    {
        abort_unless(\Gate::allows('promocode_access'), 403);
        abort_unless(\Gate::allows('promocode_delete'), 403);
        try {
            Promocode::find($id)->delete();
            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "delete_promocode";
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Promocode is deleted by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }
            return back()->with('flash_success', 'Promocode deleted successfully');
        } 
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Promocode Not Found');
        }
    }

    public static function promo_used_count($promo_id)
    {
        return PromocodeUsage::where('status','USED')->where('promocode_id',$promo_id)->count();
    }
}
