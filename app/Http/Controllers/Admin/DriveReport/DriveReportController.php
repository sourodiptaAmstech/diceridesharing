<?php

namespace App\Http\Controllers\Admin\DriveReport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Request\ServiceRequest;
use App\Model\Request\ServiceRequestLog;
use App\Model\Request\ServiceRequestLocation;
use App\Model\ServiceType\DriverServiceType;
use App\Model\Transaction\TransactionLog;
use App\Services\EstimatedFareService;
use \Carbon\Carbon;


class DriveReportController extends Controller
{
    //PAYMENT REPORT SECTION
    public function allRequest($type){
        abort_unless(\Gate::allows('all_request_access'), 403);
    	return view('admin.driveReport.allrequest',compact('type'));
    }


	public function ajaxAllRequest(Request $request)
    {
        //['ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED','CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER']
        $columns = array(
            0 => 'dfirst_name',
            1 => 'first_name',
			2 => 'request_no',
            3 => 'created_at',
            4 => 'cost',
            5 => 'promo_code_value',
			6 => 'payment_method',
            7 => 'payment_status',
            8 => 'request_status'
        );

        $totalData = ServiceRequest::whereIn('request_status',['COMPLETED'])->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $type = $request->type;
        if($type == 'all'){

            if(empty($request->input('search.value'))){
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
                } else if($order=='dfirst_name'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
                } else if($order=='cost'||$order=='promo_code_value'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->offset($start)->limit($limit)->orderBy('tl.'.$order,$dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->count();

            }else{
                $search = $request->input('search.value');
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
                } else if($order=='dfirst_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
                } else if($order=='cost'||$order=='promo_code_value') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('tl.'.$order, $dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.payment_status','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%")
                    ->orWhere('tl.cost','like',"%{$search}%")
                    ->orWhere('tl.promo_code_value','like',"%{$search}%");
                })->count();
            }
            
        } elseif ($type == 'today') {

            if(empty($request->input('search.value'))){
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::today())->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
                } else if($order=='dfirst_name'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::today())->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
                } else if($order=='cost'||$order=='promo_code_value'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::today())->offset($start)->limit($limit)->orderBy('tl.'.$order,$dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::today())->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::today())->count();

            }else{
                $search = $request->input('search.value');
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::today())->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
                } else if($order=='dfirst_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::today())->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
                } else if($order=='cost'||$order=='promo_code_value') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::today())->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('tl.'.$order, $dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::today())->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::today())->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.payment_status','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%")
                    ->orWhere('tl.cost','like',"%{$search}%")
                    ->orWhere('tl.promo_code_value','like',"%{$search}%");
                })->count();
            }
            
        } elseif ($type == 'yesterday') {

            if(empty($request->input('search.value'))){
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::yesterday())->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
                } else if($order=='dfirst_name'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::yesterday())->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
                } else if($order=='cost'||$order=='promo_code_value'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::yesterday())->offset($start)->limit($limit)->orderBy('tl.'.$order,$dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::yesterday())->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::yesterday())->count();

            }else{
                $search = $request->input('search.value');
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::yesterday())->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
                } else if($order=='dfirst_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::yesterday())->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
                } else if($order=='cost'||$order=='promo_code_value') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::yesterday())->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('tl.'.$order, $dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::yesterday())->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereDate('service_requests.created_at', Carbon::yesterday())->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.payment_status','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%")
                    ->orWhere('tl.cost','like',"%{$search}%")
                    ->orWhere('tl.promo_code_value','like',"%{$search}%");
                })->count();
            }
            
        } elseif ($type == 'current-week') {

            if(empty($request->input('search.value'))){
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
                } else if($order=='dfirst_name'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
                } else if($order=='cost'||$order=='promo_code_value'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->offset($start)->limit($limit)->orderBy('tl.'.$order,$dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();

            }else{
                $search = $request->input('search.value');
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
                } else if($order=='dfirst_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
                } else if($order=='cost'||$order=='promo_code_value') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('tl.'.$order, $dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.payment_status','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%")
                    ->orWhere('tl.cost','like',"%{$search}%")
                    ->orWhere('tl.promo_code_value','like',"%{$search}%");
                })->count();
            }
            
        } elseif ($type == 'previous-week') {

            if(empty($request->input('search.value'))){
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [date("Y-m-d",strtotime("last monday",strtotime("-1 week +1 day"))), date("Y-m-d",strtotime("next monday",strtotime("last monday",strtotime("-1 week +1 day"))))])->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
                } else if($order=='dfirst_name'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [date("Y-m-d",strtotime("last monday",strtotime("-1 week +1 day"))), date("Y-m-d",strtotime("next monday",strtotime("last monday",strtotime("-1 week +1 day"))))])->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
                } else if($order=='cost'||$order=='promo_code_value'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [date("Y-m-d",strtotime("last monday",strtotime("-1 week +1 day"))), date("Y-m-d",strtotime("next monday",strtotime("last monday",strtotime("-1 week +1 day"))))])->offset($start)->limit($limit)->orderBy('tl.'.$order,$dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [date("Y-m-d",strtotime("last monday",strtotime("-1 week +1 day"))), date("Y-m-d",strtotime("next monday",strtotime("last monday",strtotime("-1 week +1 day"))))])->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [date("Y-m-d",strtotime("last monday",strtotime("-1 week +1 day"))), date("Y-m-d",strtotime("next monday",strtotime("last monday",strtotime("-1 week +1 day"))))])->count();

            }else{
                $search = $request->input('search.value');
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [date("Y-m-d",strtotime("last monday",strtotime("-1 week +1 day"))), date("Y-m-d",strtotime("next monday",strtotime("last monday",strtotime("-1 week +1 day"))))])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
                } else if($order=='dfirst_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [date("Y-m-d",strtotime("last monday",strtotime("-1 week +1 day"))), date("Y-m-d",strtotime("next monday",strtotime("last monday",strtotime("-1 week +1 day"))))])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
                } else if($order=='cost'||$order=='promo_code_value') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [date("Y-m-d",strtotime("last monday",strtotime("-1 week +1 day"))), date("Y-m-d",strtotime("next monday",strtotime("last monday",strtotime("-1 week +1 day"))))])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('tl.'.$order, $dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [date("Y-m-d",strtotime("last monday",strtotime("-1 week +1 day"))), date("Y-m-d",strtotime("next monday",strtotime("last monday",strtotime("-1 week +1 day"))))])->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereBetween('service_requests.created_at', [date("Y-m-d",strtotime("last monday",strtotime("-1 week +1 day"))), date("Y-m-d",strtotime("next monday",strtotime("last monday",strtotime("-1 week +1 day"))))])->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.payment_status','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%")
                    ->orWhere('tl.cost','like',"%{$search}%")
                    ->orWhere('tl.promo_code_value','like',"%{$search}%");
                })->count();
            }
            
        } elseif ($type == 'current-month') {

            if(empty($request->input('search.value'))){
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
                } else if($order=='dfirst_name'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
                } else if($order=='cost'||$order=='promo_code_value'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->offset($start)->limit($limit)->orderBy('tl.'.$order,$dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->count();

            }else{
                $search = $request->input('search.value');
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
                } else if($order=='dfirst_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
                } else if($order=='cost'||$order=='promo_code_value') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('tl.'.$order, $dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.payment_status','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%")
                    ->orWhere('tl.cost','like',"%{$search}%")
                    ->orWhere('tl.promo_code_value','like',"%{$search}%");
                })->count();
            }
            
        } elseif ($type == 'previous-month') {

            if(empty($request->input('search.value'))){
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->subMonth()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
                } else if($order=='dfirst_name'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->subMonth()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
                } else if($order=='cost'||$order=='promo_code_value'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->subMonth()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->offset($start)->limit($limit)->orderBy('tl.'.$order,$dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->subMonth()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->subMonth()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->count();

            }else{
                $search = $request->input('search.value');
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->subMonth()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
                } else if($order=='dfirst_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->subMonth()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
                } else if($order=='cost'||$order=='promo_code_value') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->subMonth()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('tl.'.$order, $dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->subMonth()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereMonth('service_requests.created_at', Carbon::now()->subMonth()->month)->whereYear('service_requests.created_at', Carbon::now()->year)->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.payment_status','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%")
                    ->orWhere('tl.cost','like',"%{$search}%")
                    ->orWhere('tl.promo_code_value','like',"%{$search}%");
                })->count();
            }
            
        } elseif ($type == 'current-year') {

            if(empty($request->input('search.value'))){
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y'))->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
                } else if($order=='dfirst_name'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y'))->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
                } else if($order=='cost'||$order=='promo_code_value'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y'))->offset($start)->limit($limit)->orderBy('tl.'.$order,$dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y'))->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y'))->count();

            }else{
                $search = $request->input('search.value');
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y'))->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
                } else if($order=='dfirst_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y'))->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
                } else if($order=='cost'||$order=='promo_code_value') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y'))->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('tl.'.$order, $dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y'))->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y'))->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.payment_status','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%")
                    ->orWhere('tl.cost','like',"%{$search}%")
                    ->orWhere('tl.promo_code_value','like',"%{$search}%");
                })->count();
            }
            
        } elseif ($type == 'previous-year') {

            if(empty($request->input('search.value'))){
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y', strtotime('-1 year')))->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
                } else if($order=='dfirst_name'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y', strtotime('-1 year')))->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
                } else if($order=='cost'||$order=='promo_code_value'){
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y', strtotime('-1 year')))->offset($start)->limit($limit)->orderBy('tl.'.$order,$dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y', strtotime('-1 year')))->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y', strtotime('-1 year')))->count();

            }else{
                $search = $request->input('search.value');
                if ($order=='first_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y', strtotime('-1 year')))->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
                } else if($order=='dfirst_name') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y', strtotime('-1 year')))->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
                } else if($order=='cost'||$order=='promo_code_value') {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y', strtotime('-1 year')))->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('tl.'.$order, $dir)->get();
                } else {
                    $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y', strtotime('-1 year')))->where(function($q) use ($search){
                        $q->where('ppr.first_name', 'like', "%{$search}%")
                        ->orWhere('service_requests.request_no','like',"%{$search}%")
                        ->orWhere('service_requests.payment_method','like',"%{$search}%")
                        ->orWhere('service_requests.payment_status','like',"%{$search}%")
                        ->orWhere('service_requests.request_status','like',"%{$search}%")
                        ->orWhere('service_requests.created_at','like',"%{$search}%")
                        ->orWhere('dpr.first_name','like',"%{$search}%")
                        ->orWhere('dpr.last_name','like',"%{$search}%")
                        ->orWhere('tl.cost','like',"%{$search}%")
                        ->orWhere('tl.promo_code_value','like',"%{$search}%");
                    })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
                }

                $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                    ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
                    ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
                    ->whereIn('service_requests.request_status',['COMPLETED'])->whereYear('service_requests.created_at', date('Y', strtotime('-1 year')))->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.payment_status','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%")
                    ->orWhere('tl.cost','like',"%{$search}%")
                    ->orWhere('tl.promo_code_value','like',"%{$search}%");
                })->count();
            }
            
        }

        $data = array();

        if($serviceRequests){
            foreach($serviceRequests as $d){
                $nestedData['driver_name'] = $d->dfirst_name." ".$d->dlast_name;
                $nestedData['customer_name'] = $d->first_name." ".$d->last_name;
                $nestedData['booking_number'] = $d->request_no;
                $nestedData['created_at'] = date('Y-m-d, h:i A',strtotime($d->created_at));
                $nestedData['cost'] = $d->cost;
                $nestedData['promo_code_value'] = $d->promo_code_value;
                $nestedData['payment_method'] = $d->payment_method;
                $nestedData['payment_status'] = $d->payment_status;
                $nestedData['booking_status'] = '<div style="background-color: #91fc6a;"><p style="color:black; font-weight: bold1;">'.$d->request_status.'</p></div>';
                // if ($d->request_status=='COMPLETED') {
                // 	$nestedData['booking_status'] = '<div style="background-color: #91fc6a;"><p style="color:black; font-weight: bold;">'.$d->request_status.'</p></div>';
                // } 
                // elseif ($d->request_status=='NOSERVICEFOUND') {
                // 	$nestedData['booking_status'] = '<div style="background-color: #ff8080;"><p style="color:black; font-weight: bold;">NO SERVICE FOUND</p></div>';
                // } 
                // elseif ($d->request_status=='CANCELBYSYSTEM'||$d->request_status=='CANCELBYPASSENGER'||$d->request_status=='CANCELBYDRIVER') {
                // 	$nestedData['booking_status'] = '<div style="background-color: #f7dae9;"><p style="color:black; font-weight: bold;">CANCEL</p></div>';
                // } else {
                // 	$nestedData['booking_status'] = '<div style="background-color: #ffbe57;"><p style="color:black; font-weight: bold;">ON RIDE</p></div>';
                // }
                $nestedData['action'] = '<a href="/debarati/diceridesharing/public/admin/request/details/'.$d->request_id.'/all" class="btn btn-info"> View</a>';
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function searchPaymentReport(Request $request){
        
        $from_date = $request->from_date;
        $to_date = $request->to_date;

        $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
            ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
            ->join('transaction_log as tl','tl.request_id','=','service_requests.request_id')
            ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','tl.cost','tl.promo_code_value')
            ->whereIn('service_requests.request_status',['COMPLETED'])
            ->whereBetween('service_requests.created_at',[$from_date,$to_date])
            ->orderBy('service_requests.created_at')
            ->get();

        return response()->json([
            "success" => true,
            "message" => "Payment report",
            "errors" => array("exception"=>["Everything is OK."]),
            'serviceRequests' => $serviceRequests,
        ],201);
    }
    //END PAYMENT REPORT SECTION


    public function noServiceFoundRequest(){
        abort_unless(\Gate::allows('no_service_found_access'), 403);
    	return view('admin.driveReport.noServiceFound');
    }


	public function ajaxNoServiceFoundRequest(Request $request)
    {
        $columns = array(
            0 => 'first_name',
			1 => 'request_no',
			2 => 'payment_method',
			3 => 'request_status',
        );

        $totalData = ServiceRequest::whereIn('request_status',['NOSERVICEFOUND'])->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            if ($order=='first_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
    				->select('service_requests.*','ppr.first_name','ppr.last_name')
					->whereIn('service_requests.request_status',['NOSERVICEFOUND'])
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('ppr.'.$order,$dir)
                    ->get();
            } else {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
    				->select('service_requests.*','ppr.first_name','ppr.last_name')
					->whereIn('service_requests.request_status',['NOSERVICEFOUND'])
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('service_requests.'.$order,$dir)
                    ->get();
            }

            $totalFiltered = ServiceRequest::whereIn('request_status',['NOSERVICEFOUND'])->count();
        }else{
            $search = $request->input('search.value');
            if ($order=='first_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
    			->select('service_requests.*','ppr.first_name','ppr.last_name')
				->whereIn('service_requests.request_status',['NOSERVICEFOUND'])
                ->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('ppr.'.$order, $dir)
                ->get();
            } else {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
    			->select('service_requests.*','ppr.first_name','ppr.last_name')
				->whereIn('service_requests.request_status',['NOSERVICEFOUND'])
                ->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no', 'like', "%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('service_requests.'.$order, $dir)
                ->get();
            }

            $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
    			->select('service_requests.*','ppr.first_name','ppr.last_name')
				->whereIn('service_requests.request_status',['NOSERVICEFOUND'])
                ->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no', 'like', "%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%");
                })
                ->count();
        }

        $data = array();

        if($serviceRequests){
            foreach($serviceRequests as $d){
                $nestedData['customer_name'] = $d->first_name." ".$d->last_name;
                $nestedData['booking_number'] = $d->request_no;
                $nestedData['payment_method'] = $d->payment_method;
                if ($d->request_status=='NOSERVICEFOUND') {
                	$nestedData['booking_status'] = '<div style="background-color: #ff8080;"><p style="color:black; font-weight: bold;">NO SERVICE FOUND</p></div>';
                }
                $nestedData['action'] = '<a href="details/'.$d->request_id.'/nofound" class="btn btn-info"> View</a>';
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function rejectedRequest(){
        abort_unless(\Gate::allows('rejected_request_access'), 403);
    	return view('admin.driveReport.rejectedRequest');
    }


	public function ajaxRejectedRequest(Request $request)
    {
        $columns = array(
            0 => 'first_name',
			1 => 'request_no',
			2 => 'payment_method',
			3 => 'request_status',
        );

        $totalData = ServiceRequestLog::join('passengers_profile as ppr','ppr.user_id','=','service_request_logs.passenger_id')
            ->join('drivers_profile as dpr','dpr.user_id','=','service_request_logs.driver_id')
            ->join('service_requests as sr','sr.request_id','=','service_request_logs.request_id')
            ->select('sr.request_no','sr.payment_method','sr.request_status','ppr.first_name as pfirst_name','ppr.last_name as plast_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','service_request_logs.request_id','service_request_logs.status')
            ->whereIn('service_request_logs.status', ['DECLINE'])
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            if ($order=='first_name') {
                $serviceRequests = ServiceRequestLog::join('passengers_profile as ppr','ppr.user_id','=','service_request_logs.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_request_logs.driver_id')
                    ->join('service_requests as sr','sr.request_id','=','service_request_logs.request_id')
                    ->select('sr.request_no','sr.payment_method','sr.request_status','ppr.first_name as pfirst_name','ppr.last_name as plast_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','service_request_logs.service_request_log_id','service_request_logs.request_id','service_request_logs.status')
                    ->whereIn('service_request_logs.status', ['DECLINE'])
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('ppr.'.$order,$dir)
                    ->get();
            } else {
                $serviceRequests = ServiceRequestLog::join('passengers_profile as ppr','ppr.user_id','=','service_request_logs.passenger_id')
                    ->join('drivers_profile as dpr','dpr.user_id','=','service_request_logs.driver_id')
                    ->join('service_requests as sr','sr.request_id','=','service_request_logs.request_id')
                    ->select('sr.request_no','sr.payment_method','sr.request_status','ppr.first_name as pfirst_name','ppr.last_name as plast_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','service_request_logs.service_request_log_id','service_request_logs.request_id','service_request_logs.status')
                    ->whereIn('service_request_logs.status', ['DECLINE'])
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('sr.'.$order,$dir)
                    ->get();
            }

            $totalFiltered = ServiceRequestLog::join('passengers_profile as ppr','ppr.user_id','=','service_request_logs.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_request_logs.driver_id')
                ->join('service_requests as sr','sr.request_id','=','service_request_logs.request_id')
                ->select('sr.request_no','sr.payment_method','sr.request_status','ppr.first_name as pfirst_name','ppr.last_name as plast_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','service_request_logs.request_id','service_request_logs.status')
                ->whereIn('service_request_logs.status', ['DECLINE'])
                ->count();
        }else{
            $search = $request->input('search.value');
            if ($order=='first_name') {
                $serviceRequests = ServiceRequestLog::join('passengers_profile as ppr','ppr.user_id','=','service_request_logs.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_request_logs.driver_id')
                ->join('service_requests as sr','sr.request_id','=','service_request_logs.request_id')
                ->select('sr.request_no','sr.payment_method','sr.request_status','ppr.first_name as pfirst_name','ppr.last_name as plast_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','service_request_logs.service_request_log_id','service_request_logs.request_id','service_request_logs.status')
                ->whereIn('service_request_logs.status', ['DECLINE'])
                ->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('sr.request_no','like',"%{$search}%")
                    ->orWhere('sr.payment_method','like',"%{$search}%")
                    ->orWhere('service_request_logs.status','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('ppr.'.$order, $dir)
                ->get();
            } else {
                $serviceRequests = ServiceRequestLog::join('passengers_profile as ppr','ppr.user_id','=','service_request_logs.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_request_logs.driver_id')
                ->join('service_requests as sr','sr.request_id','=','service_request_logs.request_id')
                ->select('sr.request_no','sr.payment_method','sr.request_status','ppr.first_name as pfirst_name','ppr.last_name as plast_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','service_request_logs.service_request_log_id','service_request_logs.request_id','service_request_logs.status')
                ->whereIn('service_request_logs.status', ['DECLINE'])
                ->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('sr.request_no', 'like', "%{$search}%")
                    ->orWhere('sr.payment_method','like',"%{$search}%")
                    ->orWhere('service_request_logs.status','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('sr.'.$order, $dir)
                ->get();
            }

            $totalFiltered = ServiceRequestLog::join('passengers_profile as ppr','ppr.user_id','=','service_request_logs.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_request_logs.driver_id')
                ->join('service_requests as sr','sr.request_id','=','service_request_logs.request_id')
                ->select('sr.request_no','sr.payment_method','sr.request_status','ppr.first_name as pfirst_name','ppr.last_name as plast_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name','service_request_logs.request_id','service_request_logs.status')
                ->whereIn('service_request_logs.status', ['DECLINE'])
                ->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('sr.request_no', 'like', "%{$search}%")
                    ->orWhere('sr.payment_method','like',"%{$search}%")
                    ->orWhere('service_request_logs.status','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%");
                })
                ->count();
        }

        $data = array();

        if($serviceRequests){
            foreach($serviceRequests as $d){
                $nestedData['customer_name'] = $d->pfirst_name." ".$d->plast_name;
                $nestedData['booking_number'] = $d->request_no;
                $nestedData['payment_method'] = $d->payment_method;
                if ($d->status=='DECLINE') {
                	$nestedData['booking_status'] = '<div style="background-color: #ff8080;"><p style="color:black; font-weight: bold;">DECLINE</p></div>';
                }
                $nestedData['rejected_by'] = $d->dfirst_name." ".$d->dlast_name;
                $nestedData['action'] = '<a href="details/'.$d->service_request_log_id.'/rejected" class="btn btn-info"> View</a>';
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }


    public function scheduleRequest(){
        abort_unless(\Gate::allows('ride_later_bookings_access'), 403);
    	return view('admin.driveReport.schedule');
    }

    public function ajaxScheduleRequest(Request $request)
    {
        $columns = array(
            0 => 'first_name',
			1 => 'request_no',
			2 => 'payment_method',
			3 => 'request_status',
        );

        $totalData = ServiceRequest::whereIn('request_status', ['NOSERVICEFOUND','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED','CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER'])->where('request_type','SCHEDULE')->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            if ($order=='first_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
    				->select('service_requests.*','ppr.first_name','ppr.last_name')
                    ->whereIn('service_requests.request_status', ['NOSERVICEFOUND','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED','CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER'])
                    ->where('request_type','SCHEDULE')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('ppr.'.$order,$dir)
                    ->get();
            } else {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
    				->select('service_requests.*','ppr.first_name','ppr.last_name')
                    ->whereIn('service_requests.request_status', ['NOSERVICEFOUND','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED','CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER'])
                    ->where('request_type','SCHEDULE')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('service_requests.'.$order,$dir)
                    ->get();
            }

            $totalFiltered = ServiceRequest::whereIn('request_status', ['NOSERVICEFOUND','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED','CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER'])->where('request_type','SCHEDULE')->count();
        }else{
            $search = $request->input('search.value');
            if ($order=='first_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
    			->select('service_requests.*','ppr.first_name','ppr.last_name')
                ->whereIn('service_requests.request_status', ['NOSERVICEFOUND','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED','CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER'])
                ->where('request_type','SCHEDULE')
                ->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('ppr.'.$order, $dir)
                ->get();
            } else {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
    			->select('service_requests.*','ppr.first_name','ppr.last_name')
                ->whereIn('service_requests.request_status', ['NOSERVICEFOUND','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED','CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER'])
                ->where('request_type','SCHEDULE')
                ->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no', 'like', "%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('service_requests.'.$order, $dir)
                ->get();
            }

            $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
    			->select('service_requests.*','ppr.first_name','ppr.last_name')
                ->whereIn('service_requests.request_status', ['NOSERVICEFOUND','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED','CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER'])
                ->where('request_type','SCHEDULE')
                ->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no', 'like', "%{$search}%")
                    ->orWhere('service_requests.payment_method','like',"%{$search}%")
                    ->orWhere('service_requests.request_status','like',"%{$search}%");
                })
                ->count();
        }

        $data = array();

        if($serviceRequests){
            foreach($serviceRequests as $d){
                $nestedData['customer_name'] = $d->first_name." ".$d->last_name;
                $nestedData['booking_number'] = $d->request_no;
                $nestedData['payment_method'] = $d->payment_method;
                if ($d->request_status=='COMPLETED') {
                	$nestedData['booking_status'] = '<div style="background-color: #91fc6a;"><p style="color:black; font-weight: bold;">'.$d->request_status.'</p></div>';
                } elseif ($d->request_status=='NOSERVICEFOUND') {
                	$nestedData['booking_status'] = '<div style="background-color: #ff8080;"><p style="color:black; font-weight: bold;">NO SERVICE FOUND</p></div>';
                } elseif ($d->request_status=='CANCELBYSYSTEM'||$d->request_status=='CANCELBYPASSENGER'||$d->request_status=='CANCELBYDRIVER') {
                	$nestedData['booking_status'] = '<div style="background-color: #f7dae9;"><p style="color:black; font-weight: bold;">CANCEL</p></div>';
                } else {
                	$nestedData['booking_status'] = '<div style="background-color: #ffbe57;"><p style="color:black; font-weight: bold;">ON RIDE</p></div>';
                }
                $nestedData['action'] = '<a href="details/'.$d->request_id.'/schedule" class="btn btn-info"> View</a>';
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }


    public function requestDetails($request_id,$param){
        if($param=='rejected'){
            $serviceRequest = ServiceRequestLog::where('service_request_log_id',$request_id)->where('status','DECLINE')->first();

        } else {
            $serviceRequest = ServiceRequest::find($request_id);
        }

        $request_logs = '';
        if($serviceRequest->request_status=='NOSERVICEFOUND'){
            $request_logs = ServiceRequestLog::join('drivers_profile as dpr','dpr.user_id','=','service_request_logs.driver_id')
                ->join('service_types as st','st.id','=','service_request_logs.service_type_id')
                ->join('driver_service_type as dst','dst.user_id','=','service_request_logs.driver_id')
                ->select('dpr.first_name','dpr.last_name','service_request_logs.*','st.name','dst.model','dst.registration_no')
                ->where('request_id',$serviceRequest->request_id)->get();
        }

        $requests = ServiceRequestLocation::where('request_id',$serviceRequest->request_id)->get();

        $DriverServiceType = DriverServiceType::where('user_id',$serviceRequest->driver_id)->where('service_type_id',(int)$serviceRequest->service_type_id)->first();

        $serviceRequest['staredFromSource_on']=$serviceRequest['started_from_source'];

        $Invoice = '';
        if ($param=='all'||$param=='schedule') {
            if($serviceRequest->request_status == 'COMPLETED'){
                $EstimatedFareService=new EstimatedFareService();
                $Invoice=$EstimatedFareService->accessCalculateFinalPayable((object)$serviceRequest);
            }
        }
        foreach ($requests as $request) {
            if ($request->types=='source') {
                $serviceRequest->s_latitude = $request->latitude;
                $serviceRequest->s_longitude = $request->longitude;
                $serviceRequest->s_address = $request->address;
            }
            if ($request->types=='destination') {
                $serviceRequest->d_latitude = $request->latitude;
                $serviceRequest->d_longitude = $request->longitude;
                $serviceRequest->d_address = $request->address;
            }
        }
        //dd($serviceRequest);
        return view('admin.driveReport.view',compact('serviceRequest','param','DriverServiceType','Invoice','request_logs'));
    }
}
