<?php

namespace App\Http\Controllers\Admin\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Profiles\PassengersProfile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\User;
// use Storage;
use App\Services\UsersDevices;
use App\Model\Request\ServiceRequest;
use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailAccountNotification;
use App\Model\Referral\ReferralAmount;
use App\Services\ReferralCodeService;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;
use Illuminate\Support\Facades\DB;


class PassengerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('passenger_access'), 403);
        abort_unless(\Gate::allows('passenger_list'), 403);
        return view('admin.passenger.index');
    }

    public function ajaxPassenger(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'first_name',
            2 => 'email_id',
            3 => 'isd_code',
            4 => 'created_at'
        );

        $totalData =  User::join('passengers_profile as pr','users.id','=','pr.user_id')->where('users.user_scope', 'passenger-service')->where('pr.is_remove', 'NO')->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            if ($order=='id') {
                $passengers = User::join('passengers_profile as pr','users.id','=','pr.user_id')
                    ->where('users.user_scope', 'passenger-service')
                    ->where('pr.is_remove', 'NO')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('users.'.$order,$dir)
                    ->get();
                
                foreach ($passengers as $key => $passenger) {
                    $completeRequest = ServiceRequest::where('passenger_id',$passenger->id)->where('request_status','COMPLETED')->count();
                    $passengers[$key]['completeRequest']=$completeRequest;
                }
                
            } else {
                $passengers = User::join('passengers_profile as pr','users.id','=','pr.user_id')
                    ->where('users.user_scope', 'passenger-service')
                    ->where('pr.is_remove', 'NO')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('pr.'.$order,$dir)
                    ->get();
                
                foreach ($passengers as $key => $passenger) {
                    $completeRequest = ServiceRequest::where('passenger_id',$passenger->id)->where('request_status','COMPLETED')->count();
                    $passengers[$key]['completeRequest']=$completeRequest;
                }
               
            }

            $totalFiltered =  User::join('passengers_profile as pr','users.id','=','pr.user_id')->where('users.user_scope', 'passenger-service')->where('pr.is_remove', 'NO')->count();
        }else{
            $search = $request->input('search.value');
            if ($order=='id') {
                $passengers = User::join('passengers_profile as pr','users.id','=','pr.user_id')
                ->where('users.user_scope', 'passenger-service')
                ->where('pr.is_remove', 'NO')
                ->where(function($q) use ($search){
                    $q->where('users.id', 'like', "%{$search}%")
                    ->orWhere('pr.first_name', 'like', "%{$search}%")
                    ->orWhere('pr.last_name','like',"%{$search}%")
                    ->orWhere('pr.email_id','like',"%{$search}%")
                    ->orWhere('users.username','like',"%{$search}%")
                    ->orWhere('pr.isd_code','like',"%{$search}%")
                    ->orWhere('pr.mobile_no','like',"%{$search}%")
                    ->orWhere('pr.status','like',"%{$search}%")
                    ->orWhere('pr.created_at','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('users.'.$order, $dir)
                ->get();
                foreach ($passengers as $key => $passenger) {
                    $completeRequest = ServiceRequest::where('passenger_id',$passenger->id)->where('request_status','COMPLETED')->count();
                    $passengers[$key]['completeRequest']=$completeRequest;
                }
            } else {
                $passengers = User::join('passengers_profile as pr','users.id','=','pr.user_id')
                ->where('users.user_scope', 'passenger-service')
                ->where('pr.is_remove', 'NO')
                ->where(function($q) use ($search){
                    $q->where('users.id', 'like', "%{$search}%")
                    ->orWhere('pr.first_name', 'like', "%{$search}%")
                    ->orWhere('pr.last_name','like',"%{$search}%")
                    ->orWhere('pr.email_id','like',"%{$search}%")
                    ->orWhere('users.username','like',"%{$search}%")
                    ->orWhere('pr.isd_code','like',"%{$search}%")
                    ->orWhere('pr.mobile_no','like',"%{$search}%")
                    ->orWhere('pr.status','like',"%{$search}%")
                    ->orWhere('pr.created_at','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('pr.'.$order, $dir)
                ->get();
                foreach ($passengers as $key => $passenger) {
                    $completeRequest = ServiceRequest::where('passenger_id',$passenger->id)->where('request_status','COMPLETED')->count();
                    $passengers[$key]['completeRequest']=$completeRequest;
                }
            }

            $totalFiltered = User::join('passengers_profile as pr','users.id','=','pr.user_id')
                ->where('users.user_scope', 'passenger-service')
                ->where('pr.is_remove', 'NO')
                ->where(function($q) use ($search){
                    $q->where('users.id', 'like', "%{$search}%")
                    ->orWhere('pr.first_name', 'like', "%{$search}%")
                    ->orWhere('pr.last_name','like',"%{$search}%")
                    ->orWhere('pr.email_id','like',"%{$search}%")
                    ->orWhere('users.username','like',"%{$search}%")
                    ->orWhere('pr.isd_code','like',"%{$search}%")
                    ->orWhere('pr.mobile_no','like',"%{$search}%")
                    ->orWhere('pr.status','like',"%{$search}%")
                    ->orWhere('pr.created_at','like',"%{$search}%");
                })
                ->count();
        }

        $data = array();

        if($passengers){
            foreach($passengers as $p){
                if(\Gate::allows('passenger_edit')){
                    $editButton = '<a href="passenger/'.$p->id.'/edit" class="btn btn-info">Edit</a>';
                }else{
                    $editButton = null;
                }
                if(\Gate::allows('passenger_trip_history')){
                    $tripHistoryButton = '<a href="passenger/trip/history/'.$p->id.'" class="btn btn-info">Trip History</a>';
                }else{
                    $tripHistoryButton = null;
                }
                if(\Gate::allows('passenger_review_rating')){
                    $ratingButton = '<a href="passenger/review-rating/'.$p->id.'" class="btn btn-info"> Review/Rating</a>';
                }else{
                    $ratingButton = null;
                }
                if(\Gate::allows('passenger_transaction')){
                    $transactionButton = '<a href="passenger/transaction/'.$p->id.'" class="btn btn-info">Transaction</a>';
                }else{
                    $transactionButton = null;
                }

                if ($p->passenger_profile->status == 'BLOCK') {
                    $activationButton = '<a href="passenger/activation/'.$p->id.'" class="btn btn-danger driver-status" data-toggle="tooltip" title="Click to Active">Block</a>';
                }
                if ($p->passenger_profile->status == 'ACTIVE') {
                    $activationButton = '<a href="passenger/activation/'.$p->id.'" class="btn btn-success driver-status" data-toggle="tooltip" title="Click to Block">Active</a>';
                }


                $nestedData['id']     = $p->id;
                $nestedData['passenger_name']   = $p->passenger_profile->first_name." ".$p->passenger_profile->last_name;
                $nestedData['email']   = $p->passenger_profile->email_id;
                $nestedData['mobile']  = $p->passenger_profile->isd_code.'-'.$p->passenger_profile->mobile_no;
                $nestedData['created_at']  = date('Y-m-d',strtotime($p->passenger_profile->created_at));
                if ($p->completeRequest<=0) {
                    $nestedData['referral']  = "Eligible";
                } else {
                    $nestedData['referral']  = "Not Eligible";
                }

                $nestedData['action'] = '<span style="line-height:33px;">'.$editButton.' '.$transactionButton.' '.$tripHistoryButton.' '.$ratingButton.' <a href="passenger/split/transaction/'.$p->id.'" class="btn btn-info">Split Transaction</a> '.$activationButton.' <a href="javascript:void(0)" class="btn btn-danger remove" data-id="'.$p->id.'" data-toggle="tooltip" title="Click to Remove">Remove</a></span>';
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function passengerSearch(Request $request){
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $passProfile = User::join('passengers_profile as pr','users.id','=','pr.user_id')
            ->where('users.user_scope', 'passenger-service')
            ->whereBetween('pr.created_at', [$from_date, $to_date])
            ->orderBy('pr.user_id','DESC')
            ->get();
        foreach ($passProfile as $key => $passenger) {
            $completeRequest = ServiceRequest::where('passenger_id',$passenger->id)->where('request_status','COMPLETED')->count();
            $passProfile[$key]['completeRequest']=$completeRequest;
        }
        return response()->json([
            "success" => true,
            "message" => "Rider profile",
            "errors" => array("exception"=>["Everything is OK."]),
            'profiles' => $passProfile
        ]);
    }

    public function passengerRemoveProcess(Request $request){

        $serviceRequests = ServiceRequest::where('passenger_id',$request->user_id)->whereIn('request_status',['RIDESEARCH','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING'])->get();
        if(count($serviceRequests)>0){
            return response()->json([
                "success" => false,
                "message" => "Rider is onride condition. Not possible to remove this rider.",
            ],401);
        }

        DB::table('oauth_access_tokens')->where("user_id",$request->user_id)->delete();
        $passenger = PassengersProfile::where('user_id',$request->user_id)->first();
        $passenger->is_remove = 'YES';
        $passenger->save();

        return response()->json([
            "success" => true,
            "message" => "Rider removed.",
            "errors" => array("exception"=>["Everything is OK."]),
            'is_remove' => $passenger->is_remove
        ]);
    }

    public function passengerActivationProcess(Request $request,$id){

        $passenger = PassengersProfile::where('user_id',$id)->first();

        if ($passenger->status=='ACTIVE') {
            $serviceRequests = ServiceRequest::where('passenger_id',$id)->whereIn('request_status',['RIDESEARCH','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING'])->get();
            if(count($serviceRequests)>0){
                return redirect()->back()->with('flash_error','Rider is onride condition. Not possible to block this rider.');
            }
            DB::table('oauth_access_tokens')->where("user_id",$id)->delete();
            $passenger->status = 'BLOCK';
        }
        else {
            $passenger->status = 'ACTIVE';
        }
        $passenger->save();

        if ($passenger->status=='ACTIVE') {
            return redirect()->back()->with('flash_success', 'Rider is activated successfully.');
        }
        if ($passenger->status=='BLOCK') {
            return redirect()->back()->with('flash_success','Rider is blocked successfully.');
        }

    }


    public function passengerRatingReview($id){
        abort_unless(\Gate::allows('passenger_access'), 403);
        abort_unless(\Gate::allows('passenger_review_rating'), 403);
        $passenger = PassengersProfile::where('user_id',$id)->first();
        $serviceRequests = ServiceRequest::where('passenger_id',$id)->where('request_status','COMPLETED')->get();
        return view('admin.passenger.review-rating',compact('passenger','serviceRequests'));
    }


    public function create()
    {
        abort_unless(\Gate::allows('passenger_access'), 403);
        abort_unless(\Gate::allows('passenger_create'), 403);
        return view('admin.passenger.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(\Gate::allows('passenger_access'), 403);
        abort_unless(\Gate::allows('passenger_create'), 403);
        $this->validate($request, [
            'first_name'        => 'required|max:255',
            'last_name'         => 'required|max:255',
            'email'             => 'nullable|email|max:255|unique:passengers_profile,email_id',
            'picture'           => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'password'          => 'required|confirmed|min:6',
            'mobile_no'         => 'required|digits_between:5,10|unique:passengers_profile',
        ]);
        try{

            if (empty($request->mobile_no)) {
                $request->code = null;
            } else {
                $request->code = '+'.$request->code;
            }
            // creating new users
            $User = new User();
            $User->password = bcrypt(trim($request->password));
            $User->user_scope = "passenger-service";
            $User->username = $request->mobile_no;
            $User->save();

            // create the user's profile
            $PassengersProfile = new PassengersProfile();
            $PassengersProfile->user_id = $User->id;
            $PassengersProfile->email_id = $request->email;
            $PassengersProfile->first_name = $request->first_name;
            $PassengersProfile->last_name = $request->last_name;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/passenger/profile');
                $picture = str_replace("public", "storage", $picture);
                $PassengersProfile->picture=env("baseURL").$picture;
            }
            $PassengersProfile->mobile_no = $request->mobile_no;
            $PassengersProfile->isd_code = $request->code;
            $PassengersProfile->dob=$request->dob;
            $PassengersProfile->gender=$request->gender;
            $PassengersProfile->isMobileverified=1;
            $PassengersProfile->save();

            //confirm it
            $request->user_id = $User->id;
            $request->device_id = "device_id";
            $request->device_token = "device_token";
            $UserDevice=new UsersDevices();
            $UserDevice->accessCreateDevices($request);

            //referral code
            $ReferralAmount = ReferralAmount::where('user_type','passenger')->first();
            $request->referral_amount_id=$ReferralAmount->referral_amount_id;
            $ReferralCodeService = new ReferralCodeService();
            $ReferralCodeService->accessInsertReferralCode($request);

            $User->email = $request->email;
            $sub = "Login Credentials to Dice App";
            $message = "Please login to the Dice app with below login credentials.";
            $email = "Username: ".$request->code."-".$request->mobile_no;
            $password = "Password: ".$request->password;
            Notification::send($User, new EmailAccountNotification($sub,$message,$email,$password));

            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = 'add_passenger';
                $request->action_id = $User->id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = $PassengersProfile->first_name." ".$PassengersProfile->last_name."'s profile is created by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }

            if(!empty($User)){
                if($User->id>0){
                    return redirect()->route('admin.passenger.index')->with('flash_success', 'Rider account created successfully.');
                }
            }
            return redirect()->back()->with('flash_error', 'Registration not possible!');
        }

        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_unless(\Gate::allows('passenger_access'), 403);
        abort_unless(\Gate::allows('passenger_edit'), 403);
        try {
            $passenger = User::find($id);
            return view('admin.passenger.edit',compact('passenger'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('passenger_access'), 403);
        abort_unless(\Gate::allows('passenger_edit'), 403);
        $this->validate($request, [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'email'         => 'nullable|email|unique:passengers_profile,email_id,'.$id.',user_id',
            'picture'       => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'mobile'        => 'required|digits_between:5,10|unique:passengers_profile,mobile_no,'.$id.',user_id',
        ]);
        if (empty($request->mobile)) {
            $request->code = null;
        } else {
            $request->code = '+'.(integer)$request->code;
        }
        try {
            $passenger = User::find($id);
            $passenger->username = $request->mobile;
            $passenger->save();
            $passenger_profile = PassengersProfile::where('user_id', $id)->first();
            if (isset($passenger_profile)) {
                $passenger_profile->first_name    = $request->first_name;
                $passenger_profile->last_name     = $request->last_name;
                $passenger_profile->email_id      = $request->email;
                if(isset($request->picture) && !empty($request->picture)){
                    //$Storage=Storage::delete($passenger->picture);
                    $picture = $request->picture->store('public/passenger/'.$id.'/profile');
                    $picture = str_replace("public", "storage", $picture);
                    $passenger_profile->picture = $picture;
                    $passenger_profile->picture=env("baseURL").$picture;
                }
                $passenger_profile->mobile_no = $request->mobile;
                $passenger_profile->isd_code  = $request->code;
                $passenger_profile->dob       = $request->dob;
                $passenger_profile->gender    = $request->gender;
                $passenger_profile->save();

                //add sub admin log
                if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                    $request->flag = 'edit_passenger';
                    $request->action_id = $id;
                    $request->sub_admin_id = Auth::user()->id;
                    $request->message = $passenger_profile->first_name." ".$passenger_profile->last_name."'s profile is updated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                    $UserService = new UserService();
                    $UserService->insertSubadminLog($request);
                }
            }

            return redirect()->route('admin.passenger.index')->with('flash_success', 'Rider Updated Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Rider Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
