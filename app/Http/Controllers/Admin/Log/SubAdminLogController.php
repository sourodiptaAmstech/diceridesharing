<?php

namespace App\Http\Controllers\Admin\Log;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Profiles\SubAdminLog;

class SubAdminLogController extends Controller
{
    public function getLogReport(Request $request){
        abort_unless(\Gate::allows('subadmin_log'), 403);
        return view('admin.log.index');
    }

    public function getSubadminLog(Request $request)
    {
		$columns = array(
			0 => 'flag',
			1 => 'message',
			2 => 'created_at'
		);
		
        $totalData = SubAdminLog::count();
		$limit = $request->input('length');
		$start = $request->input('start');
		$order = $columns[$request->input('order.0.column')];
		$dir = $request->input('order.0.dir');
		
		if(empty($request->input('search.value'))){
			$posts = SubAdminLog::offset($start)
					->limit($limit)
					->orderBy($order,$dir)
					->get();
			$totalFiltered = SubAdminLog::count();
		}else{
			$search = $request->input('search.value');
            $posts = SubAdminLog::where('flag', 'like', "%{$search}%")
							->orWhere('message','like',"%{$search}%")
							->orWhere('created_at','like',"%{$search}%")
							->offset($start)
							->limit($limit)
							->orderBy($order, $dir)
							->get();
            $totalFiltered = SubAdminLog::where('flag', 'like', "%{$search}%")
                            ->orWhere('message','like',"%{$search}%")
                            ->orWhere('created_at','like',"%{$search}%")
							->count();
		}		

		$data = array();
		
		if($posts){
			foreach($posts as $r){
				$nestedData['flag']   = str_replace("_"," ",$r->flag);
				$nestedData['message']   = $r->message;
				$nestedData['created_at']  = date('Y-m-d h:i A',strtotime($r->created_at));
				$data[] = $nestedData;
			}
		}
		
		$json_data = array(
			"draw"			  => intval($request->input('draw')),
			"recordsTotal"	  => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"			  => $data
		);
		
		echo json_encode($json_data);
    }
}
