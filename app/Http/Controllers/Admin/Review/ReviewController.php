<?php

namespace App\Http\Controllers\Admin\Review;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Request\ServiceRequest;

class ReviewController extends Controller
{
	public function index()
	{
		return view('admin.review.index');
	}

    public function ajaxReview(Request $request)
    {
        $columns = array(
        	2 => 'request_no',
            0 => 'dfirst_name',
            1 => 'first_name',
			4 => 'rating_by_passenger',
            3 => 'created_at',
            5 => 'comment_by_passenger'
        );

        $totalData = ServiceRequest::where('payment_status','COMPLETED')->where('request_status','COMPLETED')->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            if ($order=='first_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
            } else if($order=='dfirst_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
            } else {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
            }

            $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->count();

        } else {
            $search = $request->input('search.value');
            if ($order=='first_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('ppr.last_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.rating_by_passenger','like',"%{$search}%")
                    ->orWhere('service_requests.comment_by_passenger','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%");
                })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
            } else if($order=='dfirst_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('ppr.last_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.rating_by_passenger','like',"%{$search}%")
                    ->orWhere('service_requests.comment_by_passenger','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%");
                })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
            } else {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('ppr.last_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.rating_by_passenger','like',"%{$search}%")
                    ->orWhere('service_requests.comment_by_passenger','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%");
                })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
            }

            $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->where(function($q) use ($search){
                $q->where('ppr.first_name', 'like', "%{$search}%")
                ->orWhere('ppr.last_name', 'like', "%{$search}%")
                ->orWhere('service_requests.request_no','like',"%{$search}%")
                ->orWhere('service_requests.rating_by_passenger','like',"%{$search}%")
                ->orWhere('service_requests.comment_by_passenger','like',"%{$search}%")
                ->orWhere('service_requests.created_at','like',"%{$search}%")
                ->orWhere('dpr.first_name','like',"%{$search}%")
                ->orWhere('dpr.last_name','like',"%{$search}%");
            })->count();
        }
            
        $data = array();

        if($serviceRequests){
            foreach($serviceRequests as $d){
                $nestedData['driver_name'] = $d->dfirst_name." ".$d->dlast_name;
                $nestedData['customer_name'] = $d->first_name." ".$d->last_name;
                $nestedData['booking_number'] = $d->request_no;
                $nestedData['created_at'] = date('Y-m-d, h:i A',strtotime($d->created_at));
                $nestedData['rating_by_passenger'] = $d->rating_by_passenger;
                $nestedData['comment_by_passenger'] = $d->comment_by_passenger;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }


    public function indexPassenger()
	{
		return view('admin.review.passenger-index');
	}

    public function ajaxReviewPassenger(Request $request)
    {
        $columns = array(
        	2 => 'request_no',
            0 => 'dfirst_name',
            1 => 'first_name',
			4 => 'rating_by_driver',
            3 => 'created_at',
            5 => 'comment_by_driver'
        );

        $totalData = ServiceRequest::where('payment_status','COMPLETED')->where('request_status','COMPLETED')->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value'))){
            if ($order=='first_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->offset($start)->limit($limit)->orderBy('ppr.'.$order,$dir)->get();
            } else if($order=='dfirst_name'){
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->offset($start)->limit($limit)->orderBy('dpr.first_name',$dir)->get();
            } else {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->offset($start)->limit($limit)->orderBy('service_requests.'.$order,$dir)->get();
            }

            $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->count();

        } else {
            $search = $request->input('search.value');
            if ($order=='first_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('ppr.last_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.rating_by_driver','like',"%{$search}%")
                    ->orWhere('service_requests.comment_by_driver','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%");
                })->offset($start)->limit($limit)->orderBy('ppr.'.$order, $dir)->get();
            } else if($order=='dfirst_name') {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('ppr.last_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.rating_by_driver','like',"%{$search}%")
                    ->orWhere('service_requests.comment_by_driver','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%");
                })->offset($start)->limit($limit)->orderBy('dpr.first_name', $dir)->get();
            } else {
                $serviceRequests = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->where(function($q) use ($search){
                    $q->where('ppr.first_name', 'like', "%{$search}%")
                    ->orWhere('ppr.last_name', 'like', "%{$search}%")
                    ->orWhere('service_requests.request_no','like',"%{$search}%")
                    ->orWhere('service_requests.rating_by_driver','like',"%{$search}%")
                    ->orWhere('service_requests.comment_by_driver','like',"%{$search}%")
                    ->orWhere('service_requests.created_at','like',"%{$search}%")
                    ->orWhere('dpr.first_name','like',"%{$search}%")
                    ->orWhere('dpr.last_name','like',"%{$search}%");
                })->offset($start)->limit($limit)->orderBy('service_requests.'.$order, $dir)->get();
            }

            $totalFiltered = ServiceRequest::join('passengers_profile as ppr','ppr.user_id','=','service_requests.passenger_id')
                ->join('drivers_profile as dpr','dpr.user_id','=','service_requests.driver_id')
                ->select('service_requests.*','ppr.first_name','ppr.last_name','dpr.first_name as dfirst_name','dpr.last_name as dlast_name')
                ->where('payment_status','COMPLETED')->where('request_status','COMPLETED')->where(function($q) use ($search){
                $q->where('ppr.first_name', 'like', "%{$search}%")
                ->orWhere('ppr.last_name', 'like', "%{$search}%")
                ->orWhere('service_requests.request_no','like',"%{$search}%")
                ->orWhere('service_requests.rating_by_driver','like',"%{$search}%")
                ->orWhere('service_requests.comment_by_driver','like',"%{$search}%")
                ->orWhere('service_requests.created_at','like',"%{$search}%")
                ->orWhere('dpr.first_name','like',"%{$search}%")
                ->orWhere('dpr.last_name','like',"%{$search}%");
            })->count();
        }

        $data = array();

        if($serviceRequests){
            foreach($serviceRequests as $d){
                $nestedData['driver_name'] = $d->dfirst_name." ".$d->dlast_name;
                $nestedData['customer_name'] = $d->first_name." ".$d->last_name;
                $nestedData['booking_number'] = $d->request_no;
                $nestedData['created_at'] = date('Y-m-d, h:i A',strtotime($d->created_at));
                $nestedData['rating_by_driver'] = $d->rating_by_driver;
                $nestedData['comment_by_driver'] = $d->comment_by_driver;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

}
