<?php

namespace App\Http\Controllers\Admin\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\Model\ServiceType\MstServiceType;
use Storage;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;


class ServiceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('service_type_access'), 403);
        abort_unless(\Gate::allows('service_type_list'), 403);
        $services = MstServiceType::orderBy('created_at' , 'desc')->get();
        return view('admin.service.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('service_type_access'), 403);
        abort_unless(\Gate::allows('service_type_create'), 403);
        return view('admin.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(\Gate::allows('service_type_access'), 403);
        abort_unless(\Gate::allows('service_type_create'), 403);
        $this->validate($request, [
            'name'                  => 'required|max:255',
            'provider_name'         => 'required|max:255',
            'image'                 => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'capacity'              => 'required|numeric',
            'description'           => 'required',
            'fixed'                 => 'required|numeric',
            'insure_price'          => 'required|numeric',
            //'min_price'             => 'required|numeric',
            'price'                 => 'required|numeric',
            'minute'                => 'required|numeric',
            //'distance'              => 'required|numeric',
            //'calculator'            => 'required|in:MIN,HOUR,DISTANCE,DISTANCEMIN,DISTANCEHOUR',
            'min_waiting_time'      => 'required|numeric',
            'min_waiting_charge'    => 'required|numeric',

        ]);

        try {
            $service = new MstServiceType;

            if(isset($request->image) && !empty($request->image)){
                $image = $request->image->store('public/service');
                $image=str_replace("public", "storage", $image);
                $service->image=env("baseURL").$image;
            }


            $service->name                  = $request->name;
            $service->provider_name         = $request->provider_name;
            $service->fixed                 = $request->fixed;
            $service->distance              = 0;
            $service->minute                = $request->minute;
            $service->price                 = $request->price;
            $service->capacity              = $request->capacity;
            $service->calculator            = 'MIN';
            $service->description           = $request->description;
            $service->insure_price          = $request->insure_price;
            $service->min_price             = 0;
            $service->min_waiting_time      = $request->min_waiting_time;
            $service->min_waiting_charge    = $request->min_waiting_charge;
            $service->save();

            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "add_service_type";
                $request->action_id = $service->id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Service '".$service->name."' is created by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }

            return back()->with('flash_success','Service Type Saved Successfully');
        } catch (Exception $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_unless(\Gate::allows('service_type_access'), 403);
        abort_unless(\Gate::allows('service_type_edit'), 403);
        try {
            $service = MstServiceType::findOrFail($id);
            return view('admin.service.edit',compact('service'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }


    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('service_type_access'), 403);
        abort_unless(\Gate::allows('service_type_edit'), 403);
        $this->validate($request, [
            'name'                  => 'required|max:255',
            'provider_name'         => 'required|max:255',
            'image'                 => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'capacity'              => 'required|numeric',
            'description'           => 'required',
            'fixed'                 => 'required|numeric',
            'insure_price'          => 'required|numeric',
            //'min_price'             => 'required|numeric',
            'price'                 => 'required|numeric',
            'minute'                => 'required|numeric',
            // 'distance'              => 'required|numeric',
            // 'calculator'            => 'required|in:MIN,HOUR,DISTANCE,DISTANCEMIN,DISTANCEHOUR',
            'min_waiting_time'      => 'required|numeric',
            'min_waiting_charge'    => 'required|numeric',
        ]);

        try {
            $service = MstServiceType::findOrFail($id);
            if(isset($request->image) && !empty($request->image)){
                $Storage=Storage::delete($service->image);
                $image = $request->image->store('public/service/'.$id);
                $image=str_replace("public", "storage", $image);
                $service->image=env("baseURL").$image;
            }

            $service->name = $request->name;
            $service->provider_name = $request->provider_name;
            $service->fixed = $request->fixed;
            $service->insure_price = $request->insure_price;
            $service->min_price = 0;
            $service->price = $request->price;
            $service->minute = $request->minute;
            $service->distance = 0;
            $service->calculator = 'MIN';
            $service->capacity = $request->capacity;
            $service->description = $request->description;
            $service->min_waiting_time = $request->min_waiting_time;
            $service->min_waiting_charge = $request->min_waiting_charge;
            $service->status = $request->status;
            $service->save();

            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "edit_service_type";
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Service '".$service->name."' is updated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }

            return redirect()->route('admin.service.index')->with('flash_success', 'Service Type Updated Successfully');
        }

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }


    public function destroy(Request $request,$id)
    {
        abort_unless(\Gate::allows('service_type_access'), 403);
        abort_unless(\Gate::allows('service_type_delete'), 403);
        try {
            MstServiceType::find($id)->delete();
            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "delete_service_type";
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Service is deleted by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }
            return back()->with('flash_success', 'Service Type deleted successfully');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        } catch (Exception $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }
}
