<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setting\Setting;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;


class SettingController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('site_setting_access'), 403);
        $site_settings = Setting::where('key','site_title')->orWhere('key','site_logo')->orWhere('key','site_email_logo')->orWhere('key','site_icon')->orWhere('key','site_copyright')->orWhere('key','contact_number')->orWhere('key','contact_email')->orWhere('key','sos_number')->orWhere('key','dsos_number')->orWhere('key','commission')->orWhere('key','passenger_cancellation_charge')->orWhere('key','driver_cancellation_charge')->orWhere('key','surge_price')->get();

        return view('admin.setting.index', compact('site_settings'));
    }

    public function surgePriceUpdate(Request $request)
    {
        if ($request->status=='YES'){
            $setting = Setting::where('key','surge_price')->first();
            $setting->status = 'Y';
            $setting->save();
        }
        if ($request->status=='NO'){
            $setting = Setting::where('key','surge_price')->first();
            $setting->status = 'N';
            $setting->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Updated Successfully.',
            'data' => $setting,
        ], 200);
    }

    public function getSurgePrice(Request $request)
    {
        $setting = Setting::where('key','surge_price')->first();
        return response()->json([
            'success' => true,
            'message' => 'Get surge price.',
            'data' => $setting,
        ], 200);
    }
    
    public function privacyPolicy()
    {
        abort_unless(\Gate::allows('cms_management_access'), 403);
        $policies = Setting::where('key','condition_privacy')->orWhere('key','page_privacy')->orWhere('key','condition_privacy_driver')->orWhere('key','page_privacy_driver')->get();
        return view('admin.setting.policy', compact('policies'));
    }

    public function privacyPolicyUpdate(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);

            if ($site_setting->key == 'condition_privacy'||$site_setting->key == 'page_privacy'||$site_setting->key == 'condition_privacy_driver'||$site_setting->key == 'page_privacy_driver') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            }
            $site_setting->save();

            //add sub admin log
            if($site_setting->key == 'condition_privacy'){
                $flag = "passenger_term_condition";
                $submsg = "Passenger's terms&conditions";
            }
            if($site_setting->key == 'page_privacy'){
                $flag = "passenger_privacy_policy";
                $submsg = "Passenger's privacy policy";
            }
            if($site_setting->key == 'condition_privacy_driver'){
                $flag = "driver_term_condition";
                $submsg = "Driver's terms&conditions";
            }
            if($site_setting->key == 'page_privacy_driver'){
                $flag = "driver_privacy_policy";
                $submsg = "Driver's privacy policy";
            }
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = $flag;
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = $submsg." is updated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }

            return redirect()->route('admin.privacy.policy')->with('flash_success', 'Privacy Policy Updated Successfully');    
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Privacy Policy Not Found');
        }
    }

   
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('site_setting_access'), 403);
        abort_unless(\Gate::allows('site_setting_edit'), 403);
        try {
            $site_setting = Setting::findOrFail($id);
            return view('admin.setting.edit',compact('site_setting'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Site Setting Not Found');
        }
    }

    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('site_setting_access'), 403);
        abort_unless(\Gate::allows('site_setting_edit'), 403);
        try {
            $site_setting = Setting::findOrFail($id);
            if ($request->key == 'site_title'||$request->key == 'site_copyright'||$site_setting->key == 'web_site'||$site_setting->key == 'contact_number'||$site_setting->key == 'contact_email'||$site_setting->key == 'sos_number'||$site_setting->key == 'dsos_number'||$site_setting->key == 'commission'||$site_setting->key == 'passenger_cancellation_charge'||$site_setting->key == 'driver_cancellation_charge'||$site_setting->key == 'surge_price') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            } else {
                $this->validate($request, [
                    'value' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                ]);
                if(isset($request->value) && !empty($request->value)){
                    //$Storage=Storage::delete($site_setting->value);
                    $value = $request->value->store('public/setting/'.$id);
                    $value=str_replace("public", "storage", $value);
                    $site_setting->value=$value;
                }
            }
            $site_setting->save();

            if($request->key == 'site_title'){
                $flag = "edit_title";
                $submsg = "Site title";
            } else if($request->key == 'surge_price'){
                $flag = "surge_price";
                $submsg = "Surge price";
            } else if($request->key == 'passenger_cancellation_charge'){
                $flag = "passenger_cancellation_charge";
                $submsg = "Passenger cancellation charge";
            } else if($request->key == 'driver_cancellation_charge'){
                $flag = "driver_cancellation_charge";
                $submsg = "Driver cancellation charge";
            } else if($request->key == 'commission'){
                $flag = "commission";
                $submsg = "Commission";
            } else if($request->key == 'sos_number'){
                $flag = "sos_number";
                $submsg = "Passenger SOS number";
            }  else if($request->key == 'dsos_number'){
                $flag = "dsos_number";
                $submsg = "Driver SOS number";
            } else if($request->key == 'site_copyright'){
                $flag = "edit_copyright";
                $submsg = "Site copyright";
            } else if($site_setting->key == 'contact_number'){
                $flag = "edit_contact_number";
                $submsg = "Site contact number";
            } else if($site_setting->key == 'contact_email'){
                $flag = "edit_contact_email";
                $submsg = "Site contact email";
            } else {
                $flag = "edit_logo_icon";
                $submsg = "Site logo or icon";
            }

            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = $flag;
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = $submsg." is updated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }
            return redirect()->route('admin.setting.index')->with('flash_success', 'Site Setting Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Site Setting Not Found');
        }
    }

    public function destroy($id)
    {
        //
    }
}
