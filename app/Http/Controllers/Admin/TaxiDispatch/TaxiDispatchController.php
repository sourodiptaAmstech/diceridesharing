<?php

namespace App\Http\Controllers\Admin\TaxiDispatch;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Model\TaxiDispatch\TaxiDispatch;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;

class TaxiDispatchController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('taxi_dispatch_access'), 403);
        abort_unless(\Gate::allows('taxi_dispatch_list'), 403);
        $taxiDispatchs = TaxiDispatch::all();
        return view('admin.taxiDispatch.index', compact('taxiDispatchs'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('taxi_dispatch_access'), 403);
        abort_unless(\Gate::allows('taxi_dispatch_create'), 403);
        $driver_services = DB::select('SELECT dp.`user_id`,dp.`first_name`,dp.`last_name`, dst.make, dst.model, dst.model_year FROM `drivers_profile` AS dp INNER JOIN driver_service_type AS dst ON dp.`user_id`=dst.user_id WHERE dp.`service_status`="ACTIVE"');

        return view('admin.taxiDispatch.create',compact('driver_services'));
    }

    public function store(Request $request)
    {
        abort_unless(\Gate::allows('taxi_dispatch_access'), 403);
        abort_unless(\Gate::allows('taxi_dispatch_create'), 403);
        $request->validate([
            'user_name' => 'required',
            'contact_number' => 'required',
            'email_address' => 'required|email',
            'pickup_location' => 'required',
            'drop_location' => 'required',
            'amount' => 'required',
            'driver_service' => 'required',
        ]);

        try{
            $taxiDispatch = new TaxiDispatch;
            $taxiDispatch->user_name = $request->user_name;
            $taxiDispatch->contact_number = $request->contact_number;
            $taxiDispatch->email_address = $request->email_address;
            $taxiDispatch->pickup_location = $request->pickup_location;
            $taxiDispatch->drop_location = $request->drop_location;
            $taxiDispatch->amount = $request->amount;
            $taxiDispatch->driver_id = $request->driver_service;
            $taxiDispatch->save();

            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = 'add_taxi_dispatch';
                $request->action_id = $taxiDispatch->taxi_dispatch_id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Taxi Dispatch is created by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }

            return redirect()->route('admin.taxi-dispatch.index')->with('flash_success','Taxi Dispatch Saved Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'City Not Found');
        }
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('taxi_dispatch_access'), 403);
        abort_unless(\Gate::allows('taxi_dispatch_edit'), 403);
        try {
            $taxiDispatch = TaxiDispatch::findOrFail($id);
            $driver_services = DB::select('SELECT dp.`user_id`,dp.`first_name`,dp.`last_name`, dst.make, dst.model, dst.model_year FROM `drivers_profile` AS dp INNER JOIN driver_service_type AS dst ON dp.`user_id`=dst.user_id WHERE dp.`service_status`="ACTIVE"');
            return view('admin.taxiDispatch.edit',compact('taxiDispatch','driver_services'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('taxi_dispatch_access'), 403);
        abort_unless(\Gate::allows('taxi_dispatch_edit'), 403);
        $request->validate([
            'user_name' => 'required',
            'contact_number' => 'required',
            'email_address' => 'required|email',
            'pickup_location' => 'required',
            'drop_location' => 'required',
            'amount' => 'required',
            'driver_service' => 'required',
        ]);

        try {
            TaxiDispatch::where('taxi_dispatch_id',$id)->update([
                    'user_name' => $request->user_name,
                    'contact_number' => $request->contact_number,
                    'email_address' => $request->email_address,
                    'pickup_location' => $request->pickup_location,
                    'drop_location' => $request->drop_location,
                    'amount' => $request->amount,
                    'driver_id' => $request->driver_service
                ]);
            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = 'edit_taxi_dispatch';
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Taxi Dispatch is updated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }
            return redirect()->route('admin.taxi-dispatch.index')->with('flash_success', 'Taxi Dispatch Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Taxi Dispatch Not Found');
        }
    }

}
