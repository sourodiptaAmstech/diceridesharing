<?php

namespace App\Http\Controllers\Admin\RBAC;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RBAC\Role;
use App\Model\RBAC\Permission;

class RoleController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $roles = Role::all();
        return view('admin.role.index',compact('roles'));
    }

  
    public function create()
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $permissions = Permission::all()->pluck('title', 'id');
        return view('admin.role.create',compact('permissions'));
    }

    public function store(Request $request)
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $this->validate($request, [
            'role'        => 'required|max:255',
        ]);
        try{
            $role = new Role();
            $role->title = $request->role;
            $role->save();

            $role->permissions()->sync($request->input('permissions', []));

            if(!empty($role)){
                if($role->id>0){
                    return redirect()->route('admin.role.index')->with('flash_success', 'Role created successfully');
                }
            }
            return redirect()->back()->with('flash_error', 'Role not possible!');
        }

        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $role = Role::where('id',$id)->first();
        $permissions = Permission::all()->pluck('title', 'id');
        $role->load('permissions');
        return view('admin.role.edit', compact('permissions', 'role'));
    }

    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $this->validate($request, [
            'role'    => 'required|max:255',
        ]);
        try {
            $role = Role::find($id);
            $role->title = $request->role;
            $role->save();
            $role->permissions()->sync($request->input('permissions', []));
            return redirect()->route('admin.role.index')->with('flash_success', 'Role Updated Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Role Not Found');
        }
    }

    
    public function destroy($id)
    {
        //
    }

}
