<?php

namespace App\Http\Controllers\Admin\RBAC;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RBAC\Permission;

class PermissionController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $permissions = Permission::all();
        return view('admin.permission.index',compact('permissions'));
    }

  
    public function create()
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        return view('admin.permission.create');
    }

    public function store(Request $request)
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $this->validate($request, [
            'permission'        => 'required|max:255',
        ]);
        try{
            $Permission = new Permission();
            $Permission->title = $request->permission;
            $Permission->save();

            if(!empty($Permission)){

                if($Permission->id>0){
                    return redirect()->route('admin.permission.index')->with('flash_success', 'Permission created successfully');
                }
            }
            return redirect()->back()->with('flash_error', 'Permission not possible!');
        }

        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $permission = Permission::where('id',$id)->first();
        return view('admin.permission.edit', compact('permission'));
    }

    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $this->validate($request, [
            'permission'    => 'required|max:255',
        ]);
        try {
            $permission = Permission::find($id);
            $permission->title = $request->permission;
            $permission->save();

            return redirect()->route('admin.permission.index')->with('flash_success', 'Permission Updated Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Permission Not Found');
        }
    }

    
    public function destroy($id)
    {
        //
    }
}
