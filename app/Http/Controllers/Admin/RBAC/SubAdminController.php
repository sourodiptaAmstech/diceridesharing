<?php

namespace App\Http\Controllers\Admin\RBAC;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RBAC\Role;
use App\User;
use App\Model\Profiles\AdminsProfile;
use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailNotificationToSubadmin;
use App\Notifications\EmailNotification;

class SubAdminController extends Controller
{

    public function index()
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $sub_admins = AdminsProfile::where('admin_type','sub_admin')->get();
        return view('admin.subadmin.index',compact('sub_admins'));
    }

  
    public function create()
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $roles = Role::all()->pluck('title', 'id');
        return view('admin.subadmin.create',compact('roles'));
    }

    public function store(Request $request)
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $this->validate($request, [
            'first_name'        => 'required|max:255',
            'last_name'         => 'required|max:255',
            'email'             => 'nullable|email|max:255|unique:admins_profile,email_id',
            'picture'           => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'password'          => 'required|confirmed|min:6',
            'mobile_no'         => 'required|digits_between:5,10|unique:admins_profile',
        ]);
        try{

            if (empty($request->mobile_no)) {
                $request->code = null;
            } else {
                $request->code = '+'.$request->code;
            }
            // creating new users
            $User = new User();
            $User->password = bcrypt(trim($request->password));
            $User->user_scope = "admin-service";
            $User->username = $request->email;
            $User->admin_user_status = "ACTIVE";
            $User->save();

            // create the user's profile
            $PassengersProfile = new AdminsProfile();
            $PassengersProfile->user_id = $User->id;
            $PassengersProfile->email_id = $request->email;
            $PassengersProfile->first_name = $request->first_name;
            $PassengersProfile->last_name = $request->last_name;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/admin/profile');
                $picture = str_replace("public", "storage", $picture);
                $PassengersProfile->picture=env("baseURL").$picture;
            }
            $PassengersProfile->mobile_no = $request->mobile_no;
            $PassengersProfile->isd_code = $request->code;
            $PassengersProfile->admin_type = "sub_admin";
            // $PassengersProfile->gender=$request->gender;
            // $PassengersProfile->isMobileverified=1;
            $PassengersProfile->save();

            $User->roles()->sync($request->input('roles', []));

            $User->email = $request->email;
            $sub = "Login Credentials";
            $message = "Please login to the admin panel with below login credentials.";
            $url = env("baseURL").'login';
            $email = "Username: ".$request->email;
            $password = "Password: ".$request->password;
            Notification::send($User, new EmailNotificationToSubadmin($sub,$message,$url,$email,$password));

            if(!empty($User)){

                if($User->id>0){
                    return redirect()->route('admin.sub-admin.index')->with('flash_success', 'Sub-admin profile created successfully');
                }
            }
            return redirect()->back()->with('flash_error', 'Registration not possible!');
        }

        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
    }

    
    public function show($id)
    {
        //
    }
    
    public function edit($id)
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $sub_admin = User::where('id',$id)->first();
        $roles = Role::all()->pluck('title', 'id');
        $sub_admin->load('roles');
        return view('admin.subadmin.edit', compact('roles', 'sub_admin'));
    }

    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $this->validate($request, [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'email'         => 'nullable|email|unique:admins_profile,email_id,'.$id.',user_id',
            'picture'       => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'mobile'        => 'required|digits_between:5,10|unique:admins_profile,mobile_no,'.$id.',user_id',
        ]);
        if (empty($request->mobile)) {
            $request->code = null;
        } else {
            $request->code = '+'.(integer)$request->code;
        }
        try {
            $passenger = User::find($id);
            $passenger->username = $request->email;
            $passenger->save();

            $passenger_profile = AdminsProfile::where('user_id', $id)->first();
            if (isset($passenger_profile)) {
                $passenger_profile->first_name    = $request->first_name;
                $passenger_profile->last_name     = $request->last_name;
                $passenger_profile->email_id      = $request->email;
                if(isset($request->picture) && !empty($request->picture)){
                    //$Storage=Storage::delete($passenger->picture);
                    $picture = $request->picture->store('public/admin/'.$id.'/profile');
                    $picture = str_replace("public", "storage", $picture);
                    $passenger_profile->picture=env("baseURL").$picture;
                }
                $passenger_profile->mobile_no = $request->mobile;
                $passenger_profile->isd_code  = $request->code;
                // $passenger_profile->dob       = $request->dob;
                // $passenger_profile->gender    = $request->gender;
                $passenger_profile->save();

                $passenger->roles()->sync($request->input('roles', []));
            }

            return redirect()->route('admin.sub-admin.index')->with('flash_success', 'Sub-admin Profile Updated Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Sub-admin Profile Not Found');
        }
    }

    
    public function destroy($id)
    {
        //
    }

    //sub-admin active/block
    public function changeAdminStatus(Request $request, $id)
    {
        abort_unless(\Gate::allows('rbac_management'), 403);
        $subadmin = User::find($id);
        if ($request->value == 'activate') {
            $subadmin->admin_user_status = "ACTIVE";
            $subadmin->save();
            $msg="Your account has been activated by admin.";
            $sub = 'Profile Activated';
            $subadmin->email = $subadmin->admin_profile->email_id;
            Notification::send($subadmin, new EmailNotification($msg,$sub));
        }  
        if ($request->value == 'block') {
            $subadmin->admin_user_status = "BLOCK";
            $subadmin->save();
            $msg="Your account has been deactivated by admin.";
            $sub = 'Profile Deactivated';
            $subadmin->email = $subadmin->admin_profile->email_id;
            Notification::send($subadmin, new EmailNotification($msg,$sub));
        }

        return response()->json([
            'success' => true,
            'message' => 'Sub-admin status updated successfully',
            'subadmin' => $subadmin
        ], 200);
    }

}
