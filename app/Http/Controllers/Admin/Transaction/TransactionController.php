<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Request\ServiceRequest;
use App\Model\Transaction\TransactionLog;
use App\Services\TransactionLogService;
use Illuminate\Support\Facades\DB;
use App\Services\PaymentService;
use App\Services\ServiceRequestService;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;
use App\Services\PayStackServices;
use App\Model\Profiles\DriverSubaccount;
use App\Model\Referral\DriverReferralBalance;

use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailInvoiceNotification;
use App\User;
use App\Model\SplitFare\SplitTransactionLog;


class TransactionController extends Controller
{
	public function index(Request $request,$id)
	{
		abort_unless(\Gate::allows('driver_access'), 403);
        abort_unless(\Gate::allows('driver_transaction'), 403);

		$serviceRequest = DB::select('SELECT * from service_requests as sr
		left join transaction_log as tl on tl.request_id=sr.request_id
		where  sr.driver_id=? and tl.request_id<>""  and (sr.payment_status="COMPLETED" or (sr.payment_status="PENDING" and sr.request_status="CANCELBYDRIVER")) order by sr.request_id DESC',[$request->id]);

		if(count($serviceRequest)>0){
			foreach($serviceRequest as $key=>$value)
			{
				$serviceRequest[$key]->driverCancelAmount = 0;
				$serviceRequest[$key]->passengerCancelAmount = 0;
				if($value->request_status!='CANCELBYDRIVER'){
					$TransactionLogService = new TransactionLogService;
					$LogService= $TransactionLogService->getPaidCancelAmount((object)['request_id'=>$value->request_id]);
					$serviceRequest[$key]->passengerCancelAmount = $LogService['data'];
				}
				if($value->request_status=='CANCELBYDRIVER'){
					$serviceRequest[$key]->driverCancelAmount = $value->cost;
				}
			}
			$statistic=$this->getDriverPaymentStatactics($id);
            $driver_id = $id;
            $DriverReferralBalance = DriverReferralBalance::where("user_id",$driver_id)->first();
            if(isset($DriverReferralBalance)){
                $referral_balance = $DriverReferralBalance->referral_balance;
            } else {
                $referral_balance = 0.00;
            }

			return view('admin.transaction.driver-index',compact('serviceRequest','id','statistic','referral_balance'));

		} else {
			return redirect()->back()->with('flash_error', 'Transaction not found!');
		}
		return view('admin.transaction.driver-index');
	}

	//***********GET DRIVER'S PAYMENT GATEWAY BENEFICIARY******************//
    public function getDriverBeneficiary(Request $request)
    {
        try{
            $user_id = $request->driver_id;
            $driverSubaccount = DriverSubaccount::where('user_id',$user_id)->first();
            if(isset($driverSubaccount)){
                $request->id = $driverSubaccount->id;
                $PayStackServices = new PayStackServices();
                $result = $PayStackServices->accessGetBeneficiary($request);
                return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
            } else {
                return response(['status'=>false,'message'=>"Beneficiary not found.","data"=>(object)[],"errors"=>""],200);
            }
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

     //***********GET ADMIN FLUTTERWAVE BALANCE******************//
    public function getAdminFlutterWaveBalance(Request $request)
    {
        try{
            $PayStackServices = new PayStackServices();
            $result = $PayStackServices->accessGetBalance();
            return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    //***********************Transfer Payout*********************************
    public function transferPayoutToBebeficiary(Request $request)
    {
        try {
            $rule = [
                "account_number" => "required",
                "account_bank" => "required",
                "amount" => "required"
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ 
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); 
            };
            
            $request->account_number = $request->account_number;
            $request->account_bank = $request->account_bank;
            $request->amount = $request->amount;
            $request->currency = "NGN";
            $request->callback_url = "https://webhook.site/b3e505b0-fe02-430e-a538-22bbbce8ce0d";
            $request->debit_currency = "NGN";
            
            $PayStackServices = new PayStackServices();
            $result = $PayStackServices->transferBeneficiary($request);
            
            return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }


	public function transactionPayout(Request $request){

		$this->validate($request, [
			'comments'    => 'required',
			'transaction_id'    => 'required|unique:transaction_log,payment_gateway_transaction_id',
        ]);

		$TranLog=new TransactionLog();
		$TranLog->payment_gateway_transaction_id=$request->transaction_id;
		$TranLog->comments=$request->comments;
		$TranLog->is_paid = 'Y';
		$TranLog->status = 'COMPLETED';
		$TranLog->types = 'DEBIT';
		$TranLog->cost = $request->cost;
		$TranLog->transaction_type = 'PAYOUTFORRIDE';
		$TranLog->payout_user_id = $request->driver_id;
		$TranLog->save();
		$request_arr = explode(",",$request->request_id);
		foreach($request_arr as $value){
			$transaction = TransactionLog::where('request_id', $value)->first();
			$transaction->is_paid = 'Y';
			$transaction->payout_reference_id = $TranLog->transaction_log_id;
			$transaction->save();

            $request->request_id = $value;
		
    		//ADD SUB-ADMIN LOG---------
    		$query = DB::select('SELECT dr.first_name, dr.last_name, sr.`request_no` FROM `transaction_log` AS tl INNER JOIN service_requests AS sr ON sr.request_id=tl.`request_id` INNER JOIN drivers_profile AS dr ON dr.user_id = sr.`driver_id`  WHERE tl.`request_id` = ? LIMIT 1',[$request->request_id]);

    		if(Auth::user()->admin_profile->admin_type=='sub_admin'){
    			$request->flag = 'driver_transaction_payout';
    			$request->action_id = $TranLog->transaction_log_id;
    			$request->sub_admin_id = Auth::user()->id;
    			$request->message = "Ride request of request No. ".$query[0]->request_no." of driver ".$query[0]->first_name." ".$query[0]->last_name." is payout by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
    			$UserService = new UserService();
    			$UserService->insertSubadminLog($request);
    		}
            //---------------------------
        }

        //UPDATE REFERRAL BALANCE
        $referral_bal = $request->referral_bal;
        $driver_id = $request->driver_id;
        $DriverReferralBalance = DriverReferralBalance::where("user_id",$driver_id)->first();
        if(isset($DriverReferralBalance)){
            $DriverReferralBalance->referral_balance = $DriverReferralBalance->referral_balance-$referral_bal;
            $DriverReferralBalance->save();
        }
        //----------------------------

        //=============END TRIP INVOICE MAIL TO DRIVER=================//
        $user = User::find($request->driver_id);
        $user->email = $user->driver_profile->email_id;

        $sub = "Trip Invoice";
        $type = "driver";
        $invoice = [
            'cost'=>$request->cost,
            'total_rideamount'=>$request->total_rideamount,
            'referral_bal'=>$request->referral_bal,
            'cashin_hand'=>$request->cashin_hand,
            'insorance_amount'=>$request->insorance_amount,
            'tax_amount'=>$request->tax_amount,
            'commission_amount'=>$request->commission_amount,
            'passcharge_amount'=>$request->passcharge_amount,
            'drivercharge_amount'=>$request->drivercharge_amount,
            'date'=>date("d.m.Y")
        ];

        Notification::send($user, new EmailInvoiceNotification($sub,$invoice,$type));
        //===========END TRIP INVOICE MAIL TO DRIVER==================//

		return response()->json([
            "success" => true,
            "message" => "Payout inserted successfully"
        ],201);
	}

    private function getSumSplitfare($data){
        $SplitTransactionLog = SplitTransactionLog::where("request_id",$data->request_id)->sum("cost");
        return (float)$SplitTransactionLog;
    }

	public function passengerTransaction(Request $request,$id){
		abort_unless(\Gate::allows('passenger_access'), 403);
        abort_unless(\Gate::allows('passenger_transaction'), 403);
        
		$serviceRequest = DB::select('SELECT * from transaction_log as tl
		left join service_requests as sr on tl.request_id=sr.request_id
		where sr.passenger_id=? and tl.request_id<>"" and (tl.transaction_type IN ("FORRIDE","CANCELBYPASSENGERCHARGE")) order by sr.request_id DESC',[$request->id]);

		if(count($serviceRequest)>0){
			foreach($serviceRequest as $key=>$value)
			{
				$serviceRequest[$key]->driverCancelAmount = 0;
				$serviceRequest[$key]->passengerCancelAmount = 0;
				if($value->request_status=='CANCELBYPASSENGER'){
					$serviceRequest[$key]->passengerCancelAmount  = $value->cost;
				}
				if($value->request_status=='CANCELBYDRIVER'){
					$serviceRequest[$key]->driverCancelAmount = $value->cost;
				}
                $serviceRequest[$key]->splitedAmount = $this->getSumSplitfare((object)["request_id"=>$value->request_id]);
			}
			return view('admin.transaction.passenger-index',compact('serviceRequest'));
		} else {
			return redirect()->back()->with('flash_error', 'Transaction not found!');
		}
		return view('admin.transaction.passenger-index',compact('id'));
	}

    public function passengerSplitTransaction($user_id){
        $SplitTransactionLog = SplitTransactionLog::join('service_requests as sr', 'sr.request_id','=','split_transaction_log.request_id')
            ->select("sr.request_status","sr.request_id","sr.request_no","sr.payment_method","sr.created_at","split_transaction_log.cost","split_transaction_log.currency")
            ->where('split_transaction_log.status','COMPLETED')
            ->where("split_transaction_log.split_user_id",$user_id)
            ->orderBy('split_transaction_log.request_id', 'desc')
            ->get();

        if(count($SplitTransactionLog)>0){
            return view('admin.transaction.passenger-split-index',compact('SplitTransactionLog'));
        } else {
            return redirect()->back()->with('flash_error', 'Split transaction not found!');
        }
    }

	public function passengerTransactionList(Request $request)
	{
		try{
            $request->user_id=$request->passenger_id;
            $ServiceRequestService=new ServiceRequestService();
            $TransactionLogService=new TransactionLogService();
            $PaymentService= new PaymentService();
            $returnRequest=$ServiceRequestService->accessGetallRequestByCustomerPagewise($request);
            foreach($returnRequest["data"] as $key=>$val){
                $returnRequest["data"][$key]['created_at']=$this->checkNull($returnRequest["data"][$key]['created_at']);
                if($returnRequest["data"][$key]['created_at']!=""){
                    $returnRequest["data"][$key]['created_at']=$this->convertFromUTC($returnRequest["data"][$key]['created_at'],$request->timeZone);
                }
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);
                // getPaidCancelAmount
                //accessGetCardDetailsByCardId

                $cancelAmount=$TransactionLogService->getPaidCancelAmount((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);
               //print_r($cancelAmount); exit;

                $returnRequest["data"][$key]['transaction']=$tracationsReturn['data']['TransactionLog'];
                $returnRequest["data"][$key]['transaction']->cancelFee=(float)$cancelAmount['data'];
                $returnRequest["data"][$key]['transaction']->cost=(float)$returnRequest["data"][$key]['transaction']->cost-(float)$returnRequest["data"][$key]['transaction']->promo_code_value-(float)$returnRequest["data"][$key]['referral_value'];//-(float)$cancelAmount;
                if($returnRequest["data"][$key]['transaction']->cost<=0){
                    $returnRequest["data"][$key]['transaction']->cost=0.00;
                }
                $returnRequest["data"][$key]['transaction']->cost= number_format((float)($returnRequest["data"][$key]['transaction']->cost), 2, '.', '');
                $returnRequest["data"][$key]['cardDetais']=(object)[];
                if($returnRequest["data"][$key]['payment_method']=="CARD" && $returnRequest["data"][$key]['card_id']!="" && $returnRequest["data"][$key]['card_id']!=null  ){

                    $cardDetails=$PaymentService->accessGetCardDetailsByCardId((object)["user_cards_id"=>$returnRequest["data"][$key]['card_id']]);
                    $returnRequest["data"][$key]['cardDetais']=$cardDetails['data'];
                }
            }
            return response(['message'=>trans("api.SYSTEM_MESSAGE.Default_Card_Updated"),"data"=>$returnRequest,"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
	}

	public function adminEarning(Request $request)
	{
		$serviceRequest = DB::select('SELECT * from transaction_log as tl
		left join service_requests as sr on tl.request_id=sr.request_id
		where  sr.request_id<>"" and (tl.transaction_type IN ("FORRIDE","CANCELBYPASSENGERCHARGE","CANCELBYDRIVERCHARGE")) order by sr.request_id DESC');
		if(count($serviceRequest)>0){
			foreach($serviceRequest as $key=>$value)
			{
				$serviceRequest[$key]->driverCancelAmount = 0;
				$serviceRequest[$key]->passengerCancelAmount = 0;
				if($value->request_status=='CANCELBYPASSENGER'){
					$serviceRequest[$key]->passengerCancelAmount  = $value->cost;
				}
				if($value->request_status=='CANCELBYDRIVER'){
					$serviceRequest[$key]->driverCancelAmount = $value->cost;
				}
			}
			return view('admin.transaction.admin-earning',compact('serviceRequest'));

		} else {
			return redirect()->back()->with('flash_error', 'Transaction not found!');
		}
		return view('admin.transaction.admin-earning');
	}

	public function getDriverPaymentStatactics($id)
	{
        try{
            $user_id= $id;
            $completedRide=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and sr.driver_id=?',[ $user_id]);
            $totolCancelAmmoutPassenger=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total  from transaction_log tl
            left join service_requests sr on sr.request_id=tl.payment_gateway_transaction_id and sr.request_status="COMPLETED" and sr.payment_status="COMPLETED"
            where sr.driver_id=? and tl.transaction_type="CANCELBYPASSENGERCHARGE" and tl.status="COMPLETED"',[ $user_id]);
            $totolCancelAmmoutDriver=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total
            FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.transaction_type="CANCELBYDRIVERCHARGE" and tl.status="PENDING"
            where sr.payment_status="PENDING" and sr.request_status="CANCELBYDRIVER" and sr.driver_id=?',[ $user_id]);
            $completedRideByCash=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.payment_method="CASH" and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and sr.driver_id=?',[ $user_id]);
            $totalCashCardCollection=(float)$completedRide[0]->total;
            $totalTax =(float)$completedRide[0]->tax;
            $totalCommission=(float)$completedRide[0]->commission;
            $totalTripecount =(float)$completedRide[0]->tripecount;
            $totalInsurance=(float)$completedRide[0]->insurance;
            $totalPassengerCancelTrip=(float)$totolCancelAmmoutPassenger[0]->tripecount;
            $totalPassengerCancelAmount=(float)$totolCancelAmmoutPassenger[0]->total;
            $totalDriverCancelTrip=(float)$totolCancelAmmoutDriver[0]->tripecount;
            $totalDriverCancelAmount=(float)$totolCancelAmmoutDriver[0]->total;
            $totalCashCollection=(float)$completedRideByCash[0]->total;
            $totalAdminTransfer=0;
            $totalPayOut=(float)$totalCashCollection+(float)$totalAdminTransfer;
            $totalBalance=(float)$totalCashCardCollection-(float)$totalTax - (float)$totalCommission-(float)$totalInsurance-(float)$totalPayOut-(float)$totalDriverCancelAmount;
            $Background=[
                "totalCashCardCollection"=>number_format((float)$totalCashCardCollection, 2, '.', ''),
                "totalTax"=>number_format((float)$totalTax, 2, '.', ''),
                "totalCommission"=>number_format((float)$totalCommission, 2, '.', ''),
                "totalTripecount"=>(int)number_format((float)$totalTripecount, 2, '.', ''),
                "totalInsurance"=>number_format((float)$totalInsurance, 2, '.', ''),
                "totalPassengerCancelTripCount"=>(int)number_format((float)$totalPassengerCancelTrip, 2, '.', ''),
                "totalPassengerCancelAmount"=>number_format((float)$totalPassengerCancelAmount, 2, '.', ''),
                "totalDriverCancelTripCount"=>(int)number_format((float)$totalDriverCancelTrip, 2, '.', ''),
                "totalDriverCancelAmount"=>number_format((float)$totalDriverCancelAmount, 2, '.', ''),
                "totalCashCollection"=>number_format((float)$totalCashCollection, 2, '.', ''),
                "totalPayOut"=>number_format((float)$totalPayOut, 2, '.', ''),
                "totalBalance"=>number_format((float)$totalBalance, 2, '.', ''),
                "totalAdminTransfer"=>number_format((float)$totalAdminTransfer, 2, '.', ''),
                "currency"=>"₦"
            ];
            return ['message'=>"Driver Background","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")]),"statusCode"=>400];
        }
    }

}
