<?php

namespace App\Http\Controllers\Admin\PushNotification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Profiles\DriverProfiles;
use App\Model\Profiles\PassengersProfile;
use App\Services\NotificationServices;

class PushNotificationController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('push_notification'), 403);
        return view('admin.pushNotification.push-notification');
    }

    public function getUser(Request $request)
    {
        if($request->user_type == 'driver'){
            $users = DriverProfiles::select('user_id','first_name','last_name')->get();
        } else if($request->user_type == 'rider') {
            $users = PassengersProfile::select('user_id','first_name','last_name')->get();
        } else {
            $users = [];
        }
        return response()->json([
            'success' => true,
            'message' => 'User list.',
            'data' => $users
        ],200);
    }

    public function adminSendPushNotification(Request $request)
    {
        abort_unless(\Gate::allows('push_notification'), 403);
        $this->validate($request, [
            'user_type'     => 'required',
            'user'          => 'required',
            'title'         => 'required',
            'message'       => 'required'
        ]);
        //admin send push
        $pushDate=[
            "title"=>$request->title,
            "text"=>$request->title,
            "body"=>$request->message,
            "type"=>"ADMINSENDPUSH",
            "user_id"=>$request->user,
            "setTo"=>'SINGLE',
            //"request_id"=>$data["request_id"]
        ];
        if($request->user_type=='rider'){
            $noteDate=[
                "message_type"=>"ADMINSENDPUSH",
                "message"=>$request->message,
                "recepient_user_id"=>$request->user,
                "user_type"=>'passenger'
            ];
        }
        if($request->user_type=='driver'){
            $noteDate=[
                "message_type"=>"ADMINSENDPUSH",
                "message"=>$request->message,
                "recepient_user_id"=>$request->user,
                "user_type"=>'driver'
            ];
        }

        $NotificationServices =new NotificationServices();
        $NotificationServices->insertPushNotification((object)$noteDate);
        $result = $NotificationServices->sendPushNotification((object)$pushDate);
        if($result==true){
            return redirect()->route('admin.send.push-notification')->with('flash_success','Push notification sent successfully.');
        } else {
            return redirect()->route('admin.send.push-notification')->with('flash_error','Something wrong! Push notification not sent successfully.');
        }
    }

}
