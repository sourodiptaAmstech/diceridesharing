<?php

namespace App\Http\Controllers\Admin\Document;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Document\Document;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\Model\Document\DriverDocuments;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;

class DocumentController extends Controller
{

    public function index()
    {
        abort_unless(\Gate::allows('document_access'), 403);
        abort_unless(\Gate::allows('document_list'), 403);
        $documents = Document::orderBy('created_at' , 'desc')->get();
        return view('admin.document.index', compact('documents'));
    }


    public function create()
    {
        abort_unless(\Gate::allows('document_access'), 403);
        abort_unless(\Gate::allows('document_create'), 403);
        return view('admin.document.create');
    }


    public function store(Request $request)
    {
        abort_unless(\Gate::allows('document_access'), 403);
        abort_unless(\Gate::allows('document_create'), 403);
        $this->validate($request, [
            'name' => 'required|max:255',
            'type' => 'required|in:VEHICLE,DRIVER,VEHICLE IMAGE',
        ]);

        try{

            // Document::create($request->all());
            $Document = new Document;
            $Document->name = $request->name;
            $Document->type = $request->type;
            $Document->save();

            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "add_document";
                $request->action_id = $Document->id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Document '".$Document->name."' is created by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }
            return redirect()->route('admin.document.index')->with('flash_success','Document Saved Successfully');

        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Document Not Found');
        }

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        abort_unless(\Gate::allows('document_access'), 403);
        abort_unless(\Gate::allows('document_edit'), 403);
        try {
            $document = Document::findOrFail($id);
            return view('admin.document.edit',compact('document'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    
    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('document_access'), 403);
        abort_unless(\Gate::allows('document_edit'), 403);
        $this->validate($request, [
            'name' => 'required|max:255',
            'type' => 'required|in:VEHICLE,DRIVER,VEHICLE IMAGE',
        ]);

        try {
            $Document = Document::findOrFail($id);
            $Document->name = $request->name;
            $Document->type = $request->type;
            $Document->save();
            
            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "edit_document";
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Document '".$Document->name."' is updated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }
            return redirect()->route('admin.document.index')->with('flash_success', 'Document Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Document Not Found');
        }
    }


    public function destroy(Request $request,$id)
    {
        abort_unless(\Gate::allows('document_access'), 403);
        abort_unless(\Gate::allows('document_delete'), 403);
        try {
            $doc_count = Document::count();
            if ($doc_count>1) {
                $driver_document = DriverDocuments::where('document_id', $id)->get();

                if ($driver_document->count() > 0) {
                    return back()->with('flash_error', 'Document already used by driver. Does not allow to delete it');
                } else {
                    $document = Document::find($id);
                    $document->status = 0;
                    $document->save();
                    $document->delete();
                    //add sub admin log
                    if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                        $request->flag = "delete_document";
                        $request->action_id = $id;
                        $request->sub_admin_id = Auth::user()->id;
                        $request->message = "Document is deleted by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                        $UserService = new UserService();
                        $UserService->insertSubadminLog($request);
                    }
                    return back()->with('flash_success', 'Document deleted successfully');
                }
            } else {
                return back()->with('flash_error', 'Does not allowe to delete all document');
            }        

        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'Document Not Found');
        }
    }

    public function changeStatus(Request $request, $id)
    {
        abort_unless(\Gate::allows('document_access'), 403);
        abort_unless(\Gate::allows('document_active'), 403);
        $document = Document::find($id);

        if ($document->status == 1) {
            $doc_count = Document::where('status', 1)->count();
            if ($doc_count>1) {
                $driver_document = DriverDocuments::where('document_id', $id)->get();
                if ($driver_document->count() > 0) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Document already used by driver. Does not allow to inactivate it'
                    ], 400);
                } else {
                    $document->status = 0;
                    //add sub admin log
                    if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                        $request->flag = "inactive_document";
                        $request->action_id = $id;
                        $request->sub_admin_id = Auth::user()->id;
                        $request->message = "Document '".$document->name."' is inactivated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                        $UserService = new UserService();
                        $UserService->insertSubadminLog($request);
                    }
                }
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Not allow to inactivate all document.'
                ], 400);
            }

        } else {
            $document->status = 1;
            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "active_document";
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "Document '".$document->name."' is activated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }
        }
        $document->save();
        
        return response()->json([
            'success' => true,
            'message' => 'Document status updated successfully'
        ], 200);
    }

}
