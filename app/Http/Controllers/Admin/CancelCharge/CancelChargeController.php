<?php

namespace App\Http\Controllers\Admin\CancelCharge;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Request\ServiceRequest;
use App\Model\Transaction\TransactionLog;
use App\Model\Request\ServiceRequestLog;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CancelChargeController extends Controller
{
    public function passengerCancelReport(){
        abort_unless(\Gate::allows('cancel_by_passenger_access'), 403);
        $passengerReports = TransactionLog::join('service_requests as sr','sr.request_id','=','transaction_log.request_id')
                    ->join('passengers_profile as pp','pp.user_id','=','sr.passenger_id')
                    ->select('transaction_log.is_paid','transaction_log.status','transaction_log.cost','transaction_log.currency','transaction_log.transaction_type','sr.*','pp.first_name','pp.last_name')
                    ->where('transaction_log.transaction_type', 'CANCELBYPASSENGERCHARGE')
                    ->get();
        return view('admin.cancelReport.passenger-cancel',compact('passengerReports'));
    }

    public function driverCancelReport(){
        abort_unless(\Gate::allows('cancel_by_driver_access'), 403);
        $driverReports = TransactionLog::join('service_requests as sr','sr.request_id','=','transaction_log.request_id')
                    ->join('drivers_profile as dp','sr.driver_id','=','dp.user_id')
                    ->select('transaction_log.is_paid','transaction_log.status','transaction_log.cost','transaction_log.currency','transaction_log.transaction_type','sr.*','dp.first_name','dp.last_name','dp.email_id','dp.isd_code','dp.mobile_no')
                    ->where('transaction_log.transaction_type', 'CANCELBYDRIVERCHARGE')
                    ->get();
        $totalCancel24 = TransactionLog::where('transaction_type', 'CANCELBYDRIVERCHARGE')->where('updated_at', '>=', Carbon::now()->subDay()->toDateTimeString())->count();
        //$totalDecline24 = DB::select("SELECT COUNT(*) FROM `service_request_logs` WHERE `status`='DECLINE' AND `updated_at`> DATE_SUB(NOW(), INTERVAL 24 HOUR)");
        $totalDecline24 = ServiceRequestLog::where('status', 'DECLINE')->where('updated_at', '>=', Carbon::now()->subDay()->toDateTimeString())->count();
        return view('admin.cancelReport.driver-cancel',compact('driverReports','totalCancel24','totalDecline24'));
    }
}
