<?php

namespace App\Http\Controllers\Admin\Referral;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Model\Referral\ReferralAmount;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;

class ReferralController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('referral_amount_access'), 403);
        abort_unless(\Gate::allows('referral_amount_list'), 403);
        $referralAmounts = ReferralAmount::all();
        return view('admin.referralAmount.index', compact('referralAmounts'));
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('referral_amount_access'), 403);
        abort_unless(\Gate::allows('referral_amount_edit'), 403);
        try {
            $referralAmount = ReferralAmount::findOrFail($id);
            return view('admin.referralAmount.edit',compact('referralAmount'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('referral_amount_access'), 403);
        abort_unless(\Gate::allows('referral_amount_edit'), 403);
        $request->validate([
            'referral_amount_sent_by' => 'required',
            'referral_amount_used_by' => 'required',
        ]);

        try {
            ReferralAmount::where('referral_amount_id',$id)->update([
                'referral_amount_sent_by' => $request->referral_amount_sent_by,
                'referral_amount_used_by' => $request->referral_amount_used_by
            ]);
            
            //add sub admin log
            if($request->user_type=="driver"){
                $flag = "edit_driver_referral_amount";
                $submsg = "Driver's referral amount";
            }
            if($request->user_type=="passenger"){
                $flag = "edit_passenger_referral_amount";
                $submsg = "Passenger's referral amount";
            }
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = $flag;
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = $submsg." is updated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }

            return redirect()->route('admin.referral-amount.index')->with('flash_success', 'Referral Amount Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Referral Amount Not Found');
        }
    }

}
