<?php

namespace App\Http\Controllers\Admin\CancelReason;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\CancelReason\CancelReason;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;


class CancelReasonController extends Controller
{ 
    public function index()
    {
        $documents = CancelReason::orderBy('created_at' , 'desc')->get();
        return view('admin.cancelReason.index', compact('documents'));
    }


    public function create()
    {
        return view('admin.cancelReason.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'reason' => 'required|max:255',
            'reason_given_by' => 'required|in:driver,passenger,driver_for_reject',
        ]);

        try{
            $CancelReason = new CancelReason;
            $CancelReason->reason = $request->reason;
            $CancelReason->reason_given_by = $request->reason_given_by;
            $CancelReason->save();
            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "add_cancel_reason";
                $request->action_id = $CancelReason->cancel_reason_id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "'".$request->reason."' is added by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }
            return redirect()->route('admin.cancel-reason.index')->with('flash_success','Cancel Reason Saved Successfully');

        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Cancel Reason Not Found');
        }

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        try {
            $document = CancelReason::findOrFail($id);
            return view('admin.cancelReason.edit',compact('document'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'reason' => 'required|max:255',
        ]);

        try {
            $CancelReason = CancelReason::where('cancel_reason_id',$id)->first();
            $CancelReason->reason = $request->reason;
            $CancelReason->save();

            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){

                $request->flag = "update_cancel_reason";
                $request->action_id = $CancelReason->cancel_reason_id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "'".$request->reason."' is updated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);

            }
            return redirect()->route('admin.cancel-reason.index')->with('flash_success', 'Cancel Reason Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Cancel Reason Not Found');
        }
    }

}
