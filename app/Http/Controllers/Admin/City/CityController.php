<?php

namespace App\Http\Controllers\Admin\City;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\City\City;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;

class CityController extends Controller
{
   
    public function index()
    {
        abort_unless(\Gate::allows('city_access'), 403);
        abort_unless(\Gate::allows('city_list'), 403);
        $cities = City::all();
        return view('admin.city.index', compact('cities'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('city_access'), 403);
        abort_unless(\Gate::allows('city_create'), 403);
        return view('admin.city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(\Gate::allows('city_access'), 403);
        abort_unless(\Gate::allows('city_create'), 403);
        $request->validate([
            'city' => 'required',
        ]);

        try{
            $city = new City;
            $city->name = $request->city;
            $city->save();

            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "add_city";
                $request->action_id = $city->city_id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "City '".$city->name."' is created by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }

            return redirect()->route('admin.city.index')->with('flash_success','City Saved Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'City Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_unless(\Gate::allows('city_access'), 403);
        abort_unless(\Gate::allows('city_edit'), 403);
        try {
            $city = City::findOrFail($id);
            return view('admin.city.edit',compact('city'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('city_access'), 403);
        abort_unless(\Gate::allows('city_edit'), 403);
        $request->validate([
            'city' => 'required',
        ]);

        try {
            City::where('city_id',$id)->update([
                    'name' => $request->city,
                ]);
            //add sub admin log
            if(Auth::user()->admin_profile->admin_type=='sub_admin'){
                $request->flag = "edit_city";
                $request->action_id = $id;
                $request->sub_admin_id = Auth::user()->id;
                $request->message = "City '".$request->city."' is updated by ".Auth::user()->admin_profile->first_name." ".Auth::user()->admin_profile->last_name;
                $UserService = new UserService();
                $UserService->insertSubadminLog($request);
            }
            return redirect()->route('admin.city.index')->with('flash_success', 'City Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'City Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            City::find($id)->delete();
            return response(['message'=>"City deleted successfully","data"=>[],"errors"=>array("exception"=>["City deleted"])],201);
        } 
        catch (Exception $e) {
            return response(['message'=>"City Not Found","data"=>[],"errors"=>array("exception"=>["City Not Found"])],201);
        }
    }

}
