<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\AuthService;
use Illuminate\Support\Facades\Auth;
use App\Services\PassengersProfileService;
use App\Services\EmergencyContact;
use App\Services\UsersDevices;

use Validator;
class PayStackController extends Controller
{
    public function verifyTransaction(Request $request){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.flutterwave.com/v3/transactions/".$data->reference."/verify",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer FLWSECK_TEST-f70ca951a3f429b6199800d2c65dae38-X"
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return ['message'=>"PayStack transaction verify","data"=>$response,"errors"=>array("exception"=>["Not Found"],"e"=>[])];//,404);
    }
}
