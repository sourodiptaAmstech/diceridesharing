<?php
namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\PassengersProfileService;
use App\Services\UsersDevices;
use App\Services\PaymentService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\ServiceRequestService;
use App\Services\EstimatedFareService;
use App\Services\TransactionLogService;
use App\Services\UserTrasactionEmailServices;
use App\Services\PayStackServices;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\SplitFare\SplitTransactionLog;


class PaymentController extends Controller
{
    public function verifyEmail(Request $request,$param)
    {
        $ute_id = base64_decode($param);
        $request->ute_id=$ute_id;
        $UserTrasactionEmailServices=new UserTrasactionEmailServices();
        $result=$UserTrasactionEmailServices->accessVerifyTrasactionEmail($request);
        return view('verify-email');
    }
    
    public function verifyEmailID(Request $request){
        try {
            $request->ute_id = $request->ute_id;
            $UserTrasactionEmailServices=new UserTrasactionEmailServices();
            $result=$UserTrasactionEmailServices->accessVerifyTrasactionEmail($request);

            return response(['message'=>trans("api.SYSTEM_MESSAGE.EMAIL_VERIFIED"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }

    }
   private function getWeekDt($TodaysDate) {
        $todateDateInTime = strtotime($TodaysDate);
        $weekStartDate = (date('w', $todateDateInTime) == 0) ? $todateDateInTime : strtotime('last sunday', $todateDateInTime);
        $weekLastDate= date('Y-m-d', strtotime('next saturday', $weekStartDate));
        return ["firstDateOfWeek"=>date('Y-m-d', $weekStartDate),"lastDayOfWeek"=>$weekLastDate];
    }
    private function getFirstAndLastDateOfMonth($month){
        $currentMonth    = strtotime($month);
        $firstDate = date('Y-m-01', $currentMonth);
        $lastDate  = date('Y-m-t', $currentMonth);
        return ["firstDate"=>date('Y-m-d', strtotime($firstDate)),"lastDate"=>date('Y-m-d', strtotime($lastDate))];
    }

    public function addCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'reference'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $request->normalize = 1;
            $request->SECKEY = env("SECKEY");
            // verify the payament by payment refernce_id

            $PayStackServices=new PayStackServices();
            $verifyArray=$PayStackServices->verifyTranscationByRefrence($request);
            
            if($verifyArray['statusCode']==200){
                $cardDetails=[
                    "email"=>$verifyArray['data']['data']['customer']["email"],
                    //"first_six"=>$verifyArray['data']['data']['card']["first_6digits"],
                    "last_four"=>$verifyArray['data']['data']['card']["last4digits"],
                    "tx_ref"=>$verifyArray['data']['data']["tx_ref"],
                    "user_id"=>$request->user_id,
                    "device_fingerprint"=>$verifyArray['data']['data']["device_fingerprint"],
                    "brand"=>trim($verifyArray['data']['data']['card']["brand"]),
                    "card_token"=>trim($verifyArray['data']['data']['card']["life_time_token"]),
                    "card_expiry"=>trim($verifyArray['data']['data']['card']["expirymonth"]).'/'.trim($verifyArray['data']['data']['card']["expiryyear"]),

                    "channel"=>$verifyArray['data']['data']["payment_entity"],
                    "is_default"=>0,
                    // "country"=>$verifyArray['data']['data']['card']["country"],
                    "country"=>"NIGERIA NG",
                    "status"=>$verifyArray['data']['data']['status']
                ];
                
                // Add card
                $PaymentService=new PaymentService();
                $addCards=$PaymentService->accessAddCards((object)$cardDetails);
               // dd( $addCards); exit;
                // Add transcation data
            }
            $card=$PaymentService->accessListCard($request);
            return response(['message'=>trans("api.SYSTEM_MESSAGE.Card_Added"),"data"=>["card"=>$card['data']],"errors"=>$card['errors']],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function ListCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // check for the verified payment email
            $UserTrasactionEmailServices=new UserTrasactionEmailServices();
            $stauts=$UserTrasactionEmailServices->checkVerifyEmail($request);
            $emailStatus="EMAILNOTADDED";
            $message="";$email="";
            if($stauts['statusCode']==403){
                $emailStatus="EMAILNOTVERIFIED";
                $message="Please verify your email id.";
                $email=$stauts['data'];


            }
            elseif($stauts['statusCode']==404){
                $emailStatus="EMAILNOTADDED";
                $message="Please add your email id.";
            }
            elseif($stauts['statusCode']==200){
                $emailStatus="EMAILVERIFIED";
                $email=$stauts['data'];

            }
            $PaymentService=new PaymentService();

            $card=$PaymentService->accessListCard($request);
            if(  $emailStatus=="EMAILVERIFIED")
                $message=$card['message'];

            return response(['message'=>$message,"data"=>["card"=>$card['data'],"emailStatus"=>$emailStatus,"email"=>$email],"errors"=>$card['errors']],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function DeleteCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'user_cards_id' => 'required|exists:user_cards,user_cards_id,user_id,'.Auth::user()->id
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,
                "field"=>$validator->field,"data"=>(object)[],
                "errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],
                "e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $PaymentService=new PaymentService();
            $card=$PaymentService->accessDeleteCard($request);

            return response(['message'=>"Deleted Card updated","data"=>[],"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }


    }

    public function setDefaultCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[ 'timeZone'=>'required','user_cards_id' => 'required'];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $request->user_id=Auth::user()->id;
            $PaymentService=new PaymentService();
            $card=$PaymentService->accessSetDefaultCard($request);

            return response(['message'=>trans("api.SYSTEM_MESSAGE.Default_Card_Updated"),"data"=>[],"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }

    private function getSumSplitFare($data){
        $SplitTransactionLog = SplitTransactionLog::where("request_id",$data->request_id)->sum("cost");
        return (float)$SplitTransactionLog;
    }

    public function getCustomerTransaction(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[ 'timeZone'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],
                "errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $ServiceRequestService=new ServiceRequestService();
            $TransactionLogService=new TransactionLogService();
            $PaymentService= new PaymentService();
            $returnRequest=$ServiceRequestService->accessGetallRequestByCustomerPagewise($request);
            foreach($returnRequest["data"] as $key=>$val){
                $returnRequest["data"][$key]['created_at']=$this->checkNull($returnRequest["data"][$key]['created_at']);
                if($returnRequest["data"][$key]['created_at']!=""){
                    $returnRequest["data"][$key]['created_at']=$this->convertFromUTC($returnRequest["data"][$key]['created_at'],$request->timeZone);
                }
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);
                // getPaidCancelAmount
                //accessGetCardDetailsByCardId

                $cancelAmount=$TransactionLogService->getPaidCancelAmount((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);
               //print_r($cancelAmount); exit;
                //get total split amount of other users
                $splitedAmount = $this->getSumSplitFare((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);

                $returnRequest["data"][$key]['transaction']=$tracationsReturn['data']['TransactionLog'];
                $returnRequest["data"][$key]['transaction']->cancelFee=(float)$cancelAmount['data'];
                $returnRequest["data"][$key]['transaction']->cost=(float)$returnRequest["data"][$key]['transaction']->cost-(float)$returnRequest["data"][$key]['transaction']->promo_code_value-(float)$returnRequest["data"][$key]['referral_value']-$splitedAmount;//-(float)$cancelAmount;
                
                if($returnRequest["data"][$key]['transaction']->cost<=0){
                    $returnRequest["data"][$key]['transaction']->cost=0.00;
                }
                $returnRequest["data"][$key]['transaction']->cost= number_format((float)($returnRequest["data"][$key]['transaction']->cost), 2, '.', '');
                $returnRequest["data"][$key]['cardDetais']=(object)[];
                if($returnRequest["data"][$key]['payment_method']=="CARD" && $returnRequest["data"][$key]['card_id']!="" && $returnRequest["data"][$key]['card_id']!=null  ){

                    $cardDetails=$PaymentService->accessGetCardDetailsByCardId((object)["user_cards_id"=>$returnRequest["data"][$key]['card_id']]);
                    $returnRequest["data"][$key]['cardDetais']=$cardDetails['data'];
                }
            }
            return response(['message'=>trans("api.SYSTEM_MESSAGE.Default_Card_Updated"),"data"=>$returnRequest,"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }


    public function currentWeekCalculation($request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            /**  get
             *   total completed trip
             *   total ammount paid to driver for all completed trip
             *   total tax collected for the completed trip
             *   total insurances fee collected for the trip
             *   total commission fee collected for the completed trip
             *
             *
             */
            $completedRide=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED"  and  WEEKOFYEAR(sr.updated_at)=WEEKOFYEAR(NOW()) and sr.request_status="COMPLETED" and sr.driver_id=?',[ $request->user_id]);

            /**
             *  total cancel ammout collected by the driver
             *
             *
             */
            $totolCancelAmmoutPassenger=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total  from transaction_log tl
            left join service_requests sr on sr.request_id=tl.payment_gateway_transaction_id and sr.request_status="COMPLETED" and sr.payment_status="COMPLETED"
            where sr.driver_id=? and  WEEKOFYEAR(sr.updated_at)=WEEKOFYEAR(NOW()) and tl.transaction_type="CANCELBYPASSENGERCHARGE" and tl.status="COMPLETED"',[ $request->user_id]);








              /**
             *  total amount for cancel fee due to driver cancel
             *
             *
             */

            $totolCancelAmmoutDriver=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total
            FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.transaction_type="CANCELBYDRIVERCHARGE" and tl.status="PENDING"
            where sr.payment_status="PENDING" and  WEEKOFYEAR(sr.updated_at)=WEEKOFYEAR(NOW()) and sr.request_status="CANCELBYDRIVER" and sr.driver_id=?',[ $request->user_id]);

            /***
             *
             *  total cash collected by the driver
             *
             */
            $completedRideByCash=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.payment_method="CASH" and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and  WEEKOFYEAR(sr.updated_at)=WEEKOFYEAR(NOW()) and sr.request_status="COMPLETED" and sr.driver_id=?',[ $request->user_id]);

            $totalCashCardCollection=(float)$completedRide[0]->total;
            $totalTax =(float)$completedRide[0]->tax;
            $totalCommission=(float)$completedRide[0]->commission;
           // $totalTripecount =(float)$completedRide[0]->tripecount;
            $totalInsurance=(float)$completedRide[0]->insurance;
           // $totalPassengerCancelTrip=(float)$totolCancelAmmoutPassenger[0]->tripecount;
            $totalPassengerCancelAmount=(float)$totolCancelAmmoutPassenger[0]->total;
           // $totalDriverCancelTrip=(float)$totolCancelAmmoutDriver[0]->tripecount;
            $totalDriverCancelAmount=(float)$totolCancelAmmoutDriver[0]->total;
            $totalCashCollection=(float)$completedRideByCash[0]->total;
            $totalAdminTransfer=0;
            $totalPayOut=(float)$totalCashCollection+(float)$totalAdminTransfer;
           // $totalBalance=(float)$totalCashCardCollection-(float)$totalTax - (float)$totalCommission-(float)$totalInsurance-(float)$totalPayOut-(float)$totalDriverCancelAmount;
            $totalEarning= $totalCashCardCollection-$totalTax-$totalCommission-$totalInsurance-$totalPassengerCancelAmount-$totalDriverCancelAmount;



          /*  $Background=[
                "totalCashCardCollection"=>number_format((float)$totalCashCardCollection, 2, '.', ''),
                "totalTax"=>number_format((float)$totalTax, 2, '.', ''),
                "totalCommission"=>number_format((float)$totalCommission, 2, '.', ''),
                "totalTripecount"=>(int)number_format((float)$totalTripecount, 2, '.', ''),
                "totalInsurance"=>number_format((float)$totalInsurance, 2, '.', ''),
                "totalPassengerCancelTripCount"=>(int)number_format((float)$totalPassengerCancelTrip, 2, '.', ''),
                "totalPassengerCancelAmount"=>number_format((float)$totalPassengerCancelAmount, 2, '.', ''),
                "totalDriverCancelTripCount"=>(int)number_format((float)$totalDriverCancelTrip, 2, '.', ''),
                "totalDriverCancelAmount"=>number_format((float)$totalDriverCancelAmount, 2, '.', ''),
                "totalCashCollection"=>number_format((float)$totalCashCollection, 2, '.', ''),
                "totalPayOut"=>number_format((float)$totalPayOut, 2, '.', ''),
                "totalBalance"=>number_format((float)$totalBalance, 2, '.', ''),
                "totalAdminTransfer"=>number_format((float)$totalAdminTransfer, 2, '.', ''),
                "currency"=>"₦"
            ]; */
            return number_format((float)$totalEarning, 2, '.', '') ;
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
    public function getDriverPaymentStatactics(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            /**  get
             *   total completed trip
             *   total ammount paid to driver for all completed trip
             *   total tax collected for the completed trip
             *   total insurances fee collected for the trip
             *   total commission fee collected for the completed trip
             *
             *
             */
            $completedRide=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and sr.driver_id=?',[ $request->user_id]);

            /**
             *  total cancel ammout collected by the driver
             *
             *
             */
            $totolCancelAmmoutPassenger=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total  from transaction_log tl
            left join service_requests sr on sr.request_id=tl.payment_gateway_transaction_id and sr.request_status="COMPLETED" and sr.payment_status="COMPLETED"
            where sr.driver_id=? and tl.transaction_type="CANCELBYPASSENGERCHARGE" and tl.status="COMPLETED"',[ $request->user_id]);








              /**
             *  total amount for cancel fee due to driver cancel
             *
             *
             */

            $totolCancelAmmoutDriver=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total
            FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.transaction_type="CANCELBYDRIVERCHARGE" and tl.status="PENDING"
            where sr.payment_status="PENDING" and sr.request_status="CANCELBYDRIVER" and sr.driver_id=?',[ $request->user_id]);

            /***
             *
             *  total cash collected by the driver
             *
             */
            $completedRideByCash=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.payment_method="CASH" and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and sr.driver_id=?',[ $request->user_id]);

            $totalCashCardCollection=(float)$completedRide[0]->total;
            $totalTax =(float)$completedRide[0]->tax;
            $totalCommission=(float)$completedRide[0]->commission;
            $totalTripecount =(float)$completedRide[0]->tripecount;
            $totalInsurance=(float)$completedRide[0]->insurance;

            $totalPassengerCancelTrip=(float)$totolCancelAmmoutPassenger[0]->tripecount;
            $totalPassengerCancelAmount=(float)$totolCancelAmmoutPassenger[0]->total;

            $totalDriverCancelTrip=(float)$totolCancelAmmoutDriver[0]->tripecount;
            $totalDriverCancelAmount=(float)$totolCancelAmmoutDriver[0]->total;

            $totalCashCollection=(float)$completedRideByCash[0]->total;

            $totalAdminTransfer=0;
            $totalPayOut=(float)$totalCashCollection+(float)$totalAdminTransfer;
            
            $totalBalance=(float)$totalCashCardCollection-(float)$totalTax - (float)$totalCommission-(float)$totalInsurance-(float)$totalPayOut-(float)$totalDriverCancelAmount;



            $Background=[
                "totalCashCardCollection"=>number_format((float)$totalCashCardCollection, 2, '.', ''),
                "totalTax"=>number_format((float)$totalTax, 2, '.', ''),
                "totalCommission"=>number_format((float)$totalCommission, 2, '.', ''),
                "totalTripecount"=>(int)number_format((float)$totalTripecount, 2, '.', ''),
                "totalInsurance"=>number_format((float)$totalInsurance, 2, '.', ''),
                "totalPassengerCancelTripCount"=>(int)number_format((float)$totalPassengerCancelTrip, 2, '.', ''),
                "totalPassengerCancelAmount"=>number_format((float)$totalPassengerCancelAmount, 2, '.', ''),
                "totalDriverCancelTripCount"=>(int)number_format((float)$totalDriverCancelTrip, 2, '.', ''),
                "totalDriverCancelAmount"=>number_format((float)$totalDriverCancelAmount, 2, '.', ''),
                "totalCashCollection"=>number_format((float)$totalCashCollection, 2, '.', ''),
                "totalPayOut"=>number_format((float)$totalPayOut, 2, '.', ''),
                "totalBalance"=>number_format((float)$totalBalance, 2, '.', ''),
                "totalAdminTransfer"=>number_format((float)$totalAdminTransfer, 2, '.', ''),
                "currentWeek"=>$this->currentWeekCalculation($request),
                "currency"=>"₦"
            ];
            return response(['message'=>"Driver Background","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function getDriverTransaction(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[ 'timeZone'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],
                "errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $ServiceRequestService=new ServiceRequestService();
            $TransactionLogService=new TransactionLogService();
            $PaymentService= new PaymentService();
            $returnRequest=$ServiceRequestService->accessGetallRequestByDriverPagewise($request);
            $driverTransactionDetails=[];
            //print_r($returnRequest); exit;
            foreach($returnRequest["data"] as $key=>$val){
                $driverTransactionDetailsTemp=[];
                $driverTransactionDetailsTemp['transactionCreated']="";
                if($this->checkNull($returnRequest["data"][$key]['transactionCreatedTime'])!=""){
                    $driverTransactionDetailsTemp['transactionCreated']=$this->convertFromUTC($returnRequest["data"][$key]['transactionCreatedTime'],$request->timeZone);
                }
                $driverTransactionDetailsTemp['request_id']=$returnRequest["data"][$key]['request_id'];
                $driverTransactionDetailsTemp['request_no']=$returnRequest["data"][$key]['request_no'];
                $driverTransactionDetailsTemp['passenger_id']=$returnRequest["data"][$key]['passenger_id'];
                $driverTransactionDetailsTemp['driver_id']=$returnRequest["data"][$key]['driver_id'];
                $driverTransactionDetailsTemp['request_type']=$returnRequest["data"][$key]['request_type'];
                $driverTransactionDetailsTemp['request_status']=$returnRequest["data"][$key]['request_status'];
                $driverTransactionDetailsTemp['isFemaleFriendly']=$returnRequest["data"][$key]['isFemaleFriendly'];
                $driverTransactionDetailsTemp['transaction_log_id']=$returnRequest["data"][$key]['transaction_log_id'];
                $driverTransactionDetailsTemp['ride_insurance']=$returnRequest["data"][$key]['ride_insurance'];
                $driverTransactionDetailsTemp['tax']=$returnRequest["data"][$key]['tax'];
                $driverTransactionDetailsTemp['cost']=$returnRequest["data"][$key]['cost'];
                $driverTransactionDetailsTemp['payment_method']=$returnRequest["data"][$key]['payment_method'];
                $driverTransactionDetailsTemp['types']=$returnRequest["data"][$key]['types'];
                $driverTransactionDetailsTemp['commission']=$returnRequest["data"][$key]['commission'];
                $driverTransactionDetailsTemp['transaction_type']=$returnRequest["data"][$key]['transaction_type'];
                $driverTransactionDetailsTemp['is_paid']=$returnRequest["data"][$key]['is_paid'];
                $driverTransactionDetailsTemp['status']=$returnRequest["data"][$key]['status'];
                $driverTransactionDetailsTemp['cancelAmount']=0;
                if( $driverTransactionDetailsTemp['request_status']!="CANCELBYDRIVER"){
                    $cancelAmount=$TransactionLogService->getPaidCancelAmount((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);
                    $driverTransactionDetailsTemp['cancelAmount']=$cancelAmount['data'];
                    $driverTransactionDetailsTemp['cost']=(float)$driverTransactionDetailsTemp['cost']-(float)$driverTransactionDetailsTemp['cancelAmount']-(float)$driverTransactionDetailsTemp['commission']-(float)$driverTransactionDetailsTemp['tax']-(float)$driverTransactionDetailsTemp['ride_insurance'];
                }
                if( $driverTransactionDetailsTemp['cost']<=0){
                    $ $driverTransactionDetailsTemp['cost']=0.00;
                }
                $driverTransactionDetailsTemp['cost']= number_format((float)( $driverTransactionDetailsTemp['cost']), 2, '.', '');
               // $driverTransactionDetailsTemp['cardDetais']=(object)[];
                if($returnRequest["data"][$key]['payment_method']=="CARD" && $returnRequest["data"][$key]['card_id']!="" && $returnRequest["data"][$key]['card_id']!=null  ){

                   // $cardDetails=$PaymentService->accessGetCardDetailsByCardId((object)["user_cards_id"=>$returnRequest["data"][$key]['card_id']]);
                    //$driverTransactionDetailsTemp['cardDetais']=$cardDetails['data'];
                }
                $driverTransactionDetails[]=$driverTransactionDetailsTemp;
            }
            $returnRequest['data']=$driverTransactionDetails;
            return response(['message'=>"List","data"=>$returnRequest,"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }


    public function getDriverSummaryStatactics(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            /**  get
             *   total completed trip for current week; current month , current yrear
             *   total ammount paid to driver for all completed trip
             *   total tax collected for the completed trip
             *   total insurances fee collected for the trip
             *   total commission fee collected for the completed trip
             *
             *
             */
            $currentDate=date('Y-m-d h:i:s A');
            $weekDates=$this->getWeekDt($currentDate);
            $currentMonthDates=$this->getFirstAndLastDateOfMonth(date('M Y',strtotime($currentDate)));
            $currentYear=date('Y',strtotime($currentDate));



            $completedRideWeekly=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and DATE_FORMAT(sr.updated_at, "%Y-%m-%d") BETWEEN "'.$weekDates["firstDateOfWeek"].'" and "'.$weekDates["lastDayOfWeek"].'" and sr.driver_id=?',[ $request->user_id]);

            $completedRideMonthly=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and DATE_FORMAT(sr.updated_at, "%Y-%m-%d") BETWEEN "'.$currentMonthDates["firstDate"].'" and "'.$currentMonthDates["lastDate"].'" and sr.driver_id=?',[ $request->user_id]);


            $completedRideYearly=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and DATE_FORMAT(sr.updated_at, "%Y")= "'.$currentYear.'" and sr.driver_id=?',[ $request->user_id]);

            $completedRideMonthWise=DB::select('SELECT count(sr.request_id) as tripecount,
            (sum(tl.cost) - sum(tl.ride_insurance) - sum(tl.tax) - sum(tl.commission)) as amount, DATE_FORMAT(sr.updated_at, "%M") as Month
            FROM service_requests sr left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and DATE_FORMAT(sr.updated_at, "%Y")= "'.$currentYear.'"
             and sr.driver_id=? GROUP BY DATE_FORMAT(sr.updated_at, "%M")',[ $request->user_id]);
             $monthArry=[
                 'January'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'February'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'March'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'April'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'May'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'June'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'July'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'August'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'September'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'October'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'November'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'December'=>['tripecount'=>0,'ammount'=>"0.00"]
             ];
             foreach($completedRideMonthWise as $key=>$val)
             {
                $monthArry[$val->Month]=['tripecount'=>(int)$val->tripecount,'ammount'=>number_format((float)$val->amount, 2, '.', '')];
             }

            $currentMonthEarning=(float)$completedRideMonthly[0]->total - (float)$completedRideMonthly[0]->tax-(float)$completedRideMonthly[0]->commission;
            $currentMonthEarning=number_format((float)$currentMonthEarning, 2, '.', '');
            $currentMonthTripCount=(float)$completedRideMonthly[0]->tripecount;

            $currentWeeklyEarning=(float)$completedRideWeekly[0]->total - (float)$completedRideWeekly[0]->tax-(float)$completedRideWeekly[0]->commission;
            $currentWeeklyEarning=number_format((float)$currentWeeklyEarning, 2, '.', '');
            $currentWeeklyTripCount=(float)$completedRideWeekly[0]->tripecount;

            $currentYearEarning=(float)$completedRideYearly[0]->total - (float)$completedRideYearly[0]->tax-(float)$completedRideYearly[0]->commission;
            $currentYearEarning=number_format((float)$currentYearEarning, 2, '.', '');
            $currentYearTripCount=(float)$completedRideYearly[0]->tripecount;

            $Background=[
                "currentMonthEarning"=>$currentMonthEarning,
                "currentMonthTripCount"=>$currentMonthTripCount,
                "currentWeeklyEarning"=>$currentWeeklyEarning,
                "currentWeeklyTripCount"=>$currentWeeklyTripCount,
                "currentYearEarning"=>$currentYearEarning,
                "currentYearTripCount"=>$currentYearTripCount,
                "currency"=>"₦",
                "graphData"=>$monthArry
            ];
            return response(['message'=>"","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
    public function getDriverYearlyStatactics(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'year'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            $currentDate=date('Y-m-d h:i:s A');
            $completedRideMonthWise=DB::select('SELECT count(sr.request_id) as tripecount,
            (sum(tl.cost) - sum(tl.ride_insurance) - sum(tl.tax) - sum(tl.commission)) as amount, DATE_FORMAT(sr.updated_at, "%M") as Month
            FROM service_requests sr left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and DATE_FORMAT(sr.updated_at, "%Y")= ?
             and sr.driver_id=? GROUP BY DATE_FORMAT(sr.updated_at, "%M")',[$request->year, $request->user_id]);
             $monthArry=[
                 'January'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'February'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'March'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'April'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'May'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'June'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'July'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'August'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'September'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'October'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'November'=>['tripecount'=>0,'ammount'=>"0.00"],
                 'December'=>['tripecount'=>0,'ammount'=>"0.00"]
             ];
             foreach($completedRideMonthWise as $key=>$val)
             {
                $monthArry[$val->Month]=['tripecount'=>(int)$val->tripecount,'ammount'=>number_format((float)$val->amount, 2, '.', '')];
             }


             $Background=[
                 "currency"=>"₦",
                "graphData"=>$monthArry
            ];
            return response(['message'=>"","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function getDriverdateStatactics(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'toDate'=>'required',
                'fromDate'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            $currentDate=date('Y-m-d h:i:s A');
            $completedRideMonthly=DB::select('SELECT sr.request_no, sr.request_id,(tl.cost-tl.ride_insurance-tl.tax-tl.commission) as total,tl.transaction_log_id, DATE_FORMAT(sr.updated_at, "%d-%m-%Y") as date  FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and DATE_FORMAT(sr.updated_at, "%Y-%m-%d") BETWEEN "'.$request->fromDate.'" and "'.$request->toDate.'" and sr.driver_id=?',[ $request->user_id]);




             $Background=[
                 "currency"=>"₦",
                "transaction"=>$completedRideMonthly
            ];
            return response(['message'=>"","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

}
