<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Services\EstimatedFareService;
use App\Services\DriversProfileService;
use App\Services\EmergencyContact;
use App\Services\UserService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use App\Services\DriversServiceType;
use App\Services\DriverPreferencesService;
use App\Services\ServiceRequestService;
use App\Services\UnitConvertionService;
use App\Services\RequestServices;
use App\Services\LocationService;
use App\Services\TransactionLogService;
use App\Http\Controllers\Api\Background\DriversBackgroundController;
use App\Services\PaymentService;
use Validator;
use Exception;
use PHPUnit\Util\Printer;
use App\Services\PassengersProfileService;
use App\Services\NotificationServices;
use App\Services\SettingServices;
use App\Model\Request\ServiceRequest;
use App\Model\Transaction\TransactionLog;
use App\Services\ReferralCodeService;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailInvoiceNotification;
use App\Model\Request\ServiceRequestLocation;
use App\Model\Payment\UserCard;


class TripController extends Controller
{
    public function reject(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'timeZone'=>'required',
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();

            $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"ACTIVE","driver_id"=>$request->user_id],"REQUESTED");
            $RequestServices=new RequestServices();
            $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"DECLINE","driver_id"=>$request->user_id],"REQUESTED");
            return response(['message'=>trans("api.SYSTEM_MESSAGE.REQUEST_DECLINE"),"data"=>(object)[],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    private function calculateFinalPayable($data){
        $EstimatedFareService=new EstimatedFareService();
        return $EstimatedFareService->accessCalculateFinalPayable((object)$data);
    }
    public function tripControl(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'timeZone'=>'required',
                'tripStatus'=>'required|in:ACCEPTED,REACHED,STARTED,DROP,PAYMENT,RATING,COMPLETED',
                 'payment_method'=>'required_if:tripStatus,PAYMENT|in:CARD,CASH',
                 'rating'=>'required_if:tripStatus,RATING',
                 'comment'=>'required_if:tripStatus,RATING'

            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();
            $RequestServices=new RequestServices();
            $ServiceRequestService=new ServiceRequestService();
            $LocationService = new LocationService();
            $NotificationServices =new NotificationServices();

            $SettingServices=new SettingServices();
            $notification=$SettingServices->getStatusByKey("notification");

            $message="";
            $status="";
            $nextStep="";
            switch($request->tripStatus){
                case "ACCEPTED":
                    //updating the driver profile
                    $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"ONRIDE","driver_id"=>$request->user_id],"requested");
                    // updating the service request log
                    $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"ONRIDE","driver_id"=>$request->user_id],"REQUESTED");
                    // updating the service request
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"ACCEPTED","driver_id"=>$request->user_id]);
                    $message=trans("api.SYSTEM_MESSAGE.REQUEST_ACCEPTED");
                    $status="ACCEPTED";
                    $nextStep="REACHED";
                      // push
                      $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.RIDE_ACCEPTED"),
                        "text"=>trans("api.NOTIFICATION.MESSAGE.RIDE_ACCEPTED"),
                        "body"=>trans("api.NOTIFICATION.MESSAGE.RIDE_ACCEPTED"),
                        "type"=>"RIDEACCEPTED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                        ];
                        $noteDate=[
                            "message_type"=>"RIDEACCEPTED",
                            "message"=>trans("api.NOTIFICATION.MESSAGE.RIDE_ACCEPTED"),
                            "recepient_user_id"=>$ServiceRequestServiceReturn->passenger_id,
                            "user_type"=>'passenger'
                        ];
                        if($notification == "ON"){
                            $NotificationServices->insertPushNotification((object)$noteDate);
                            $NotificationServices->sendPushNotification((object)$pushDate);
                        }
                        // send sms
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email

                break;
                case "REACHED":
                    // updating the service request
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"REACHED","driver_id"=>$request->user_id]);
                    $LocationServiceReturn= $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"types"=>"source"]);
                    $message=trans("api.SYSTEM_MESSAGE.WAIT_FOR_CUSTOMER");
                    $status="REACHED";
                    $nextStep="STARTED";
                     // push
                     $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.RIDE_REACHED"),
                        "text"=>trans("api.NOTIFICATION.MESSAGE.RIDE_REACHED"),
                        "body"=>trans("api.NOTIFICATION.MESSAGE.RIDE_REACHED"),
                        "type"=>"RIDEREACHED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                        ];
                        $noteDate=[
                            "message_type"=>"RIDEREACHED",
                            "message"=>trans("api.NOTIFICATION.MESSAGE.RIDE_REACHED"),
                            "recepient_user_id"=>$ServiceRequestServiceReturn->passenger_id,
                            "user_type"=>'passenger'
                        ];
                        if($notification == "ON"){
                            $NotificationServices->insertPushNotification((object)$noteDate);
                            $NotificationServices->sendPushNotification((object)$pushDate);
                        }
                        // send sms
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email
                break;
                case "STARTED":
                      //caculate waiting time;
                      $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"STARTED","driver_id"=>$request->user_id]);
                      $LocationServiceReturn= $LocationService->accessUpdateLocationStartedTime((object)["request_id"=>$request->request_id,"types"=>"source"]);
                      $message=trans("api.SYSTEM_MESSAGE.PROCEED_TO_DESTINATION");
                      $status="STARTED";
                      $nextStep="DROP";
                break;
                case "DROP":
                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"DROP","driver_id"=>$request->user_id]);
                    $LocationServiceReturn = $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"types"=>"destination"]);
                    $ServiceRequestServiceReturn = $ServiceRequestService->accessGetRequestByID((object)['request_id'=>$request->request_id]);
                    //   print_r( $ServiceRequestServiceReturn ); exit;
                    if($ServiceRequestServiceReturn->payment_method=="CARD"){
                        $requestedRide=[
                            "request_no"=>$ServiceRequestServiceReturn->request_no,
                            "request_id"=>$ServiceRequestServiceReturn->request_id,
                            "request_type"=>$ServiceRequestServiceReturn->request_type,
                            "request_status"=>$ServiceRequestServiceReturn->request_status,
                            "payment_method"=>$ServiceRequestServiceReturn->payment_method,
                            "staredFromSource_on"=>$ServiceRequestServiceReturn->started_from_source,
                            "dropped_on_destination"=>$ServiceRequestServiceReturn->dropped_on_destination,
                            "driver_rating_status"=>$ServiceRequestServiceReturn->driver_rating_status,
                            "user_cards_id"=>$ServiceRequestServiceReturn->card_id,
                            "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                            "passenger_id"=>$ServiceRequestServiceReturn->passenger_id
                        ];
                        $reqRq = $this->calculateFinalPayable($requestedRide);
                        if((float)($reqRq['cost'])-(float)($reqRq['promo_code_value'])>0){

                            $PaymentService = new PaymentService();
                            $retunPayment=$PaymentService->accesssMakeCardPayment((object)$reqRq,(object)$requestedRide);
                            
                            if($retunPayment['statusCode']==200){
                                // if payment is success update the transcation table
                                $TransactionLogService = new TransactionLogService();
                                $reqRq['payment_gateway_charge']=$retunPayment['data']['fees'];
                                $reqRq['payment_gateway_transaction_id']=$retunPayment['data']['reference'];
                                $TransactionReturn= $TransactionLogService->updateTransactionDetails((object)$reqRq);
                                if($TransactionReturn['statusCode']===200){
                                    $TransactionLogService->makeCancelPayment((object)$requestedRide);
                                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                                    "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                                    $message=trans("api.SYSTEM_MESSAGE.PAYMENT_COLLECTED");
                                    $status="PAYMENT";
                                    $nextStep="RATING";
                                }
                            }
                            else{
                                // handle paymanet failed
                            }
                        }
                        else{
                             // if payment is success update the transcation table
                             $TransactionLogService = new TransactionLogService();
                             $reqRq['payment_gateway_charge']=0;
                             $reqRq['payment_gateway_transaction_id']=$reqRq['promo_code'];
                             $TransactionReturn=  $TransactionLogService->updateTransactionDetails((object)$reqRq);
                             if($TransactionReturn['statusCode']===200){
                                $TransactionLogService->makeCancelPayment((object)$requestedRide);
                                 $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                                 "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                                 $message=trans("api.SYSTEM_MESSAGE.PAYMENT_COLLECTED");//"Thank you for collecting the payment";
                                 $status="PAYMENT";
                                 $nextStep="RATING";
                             }
                        }
                    }
                    else{
                        $message=trans("api.SYSTEM_MESSAGE.CALCULATED_PAYABLE");
                        $status="DROP";
                        $nextStep="PAYMENT";
                    }
                break;
                case "PAYMENT":
                    if($request->payment_method=="CASH"){
                        // update the trascation log table as payament completed,
                        // update the service request table to PAYMENT and mark payment completed,
                        $TransactionLogService = new TransactionLogService();
                        $TransactionLogService->updateCreditNoteStatusForRide((object)["request_id"=>$request->request_id,"status"=>"COMPLETED"]);
                        $TransactionLogService->makeCancelPayment((object)["request_id"=>$request->request_id,"status"=>"COMPLETED"]);
                        $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);

                        $message=trans("api.SYSTEM_MESSAGE.PAYMENT_COLLECTED");
                        $status="PAYMENT";
                        $nextStep="RATING";
                    }

                break;
                case "RATING":
                    // update the service request with rating comment

                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                    "request_status"=>"RATING","rating_by_driver"=>$request->rating,"comment_by_driver"=>$request->comment,
                    "driver_rating_status"=>"GIVEN","driver_id"=>$request->user_id]);
                    $message=trans("api.SYSTEM_MESSAGE.RATING_CUSTOMER");
                    $status="RATING";
                    $nextStep="COMPLETED";


                    //=============END TRIP INVOICE MAIL TO RIDER=================//
                    $ServiceRequestService=new ServiceRequestService();
                    $serviceRequest=$ServiceRequestService->getRiderActiveRequestsForInvoice($request);
                    
                    $serviceRequest['staredFromSource_on']=$serviceRequest['started_from_source'];

                    $EstimatedFareService=new EstimatedFareService();
                    $Invoice=$EstimatedFareService->accessCalculateFinalPayable((object)$serviceRequest);

                    $user = User::find($ServiceRequestServiceReturn->passenger_id);
                    $user->email = $user->passenger_profile->email_id;

                    $RequestLocations = ServiceRequestLocation::where('request_id',$request->request_id)->get();
                    foreach ($RequestLocations as $RequestLocation) {
                        if ($RequestLocation->types=='source') {
                            $Invoice['started_on'] = $RequestLocation->started_on;
                            $Invoice['source_address'] = $RequestLocation->address;
                        }
                        if ($RequestLocation->types=='destination') {
                            $Invoice['reached_on'] = $RequestLocation->reached_on;
                            $Invoice['destination_address'] = $RequestLocation->address;
                        }
                    }
                    $UserCardLastFour = '';
                    if($serviceRequest['payment_method']=='CARD'){
                        $UserCard = UserCard::find($serviceRequest['card_id']);
                        $UserCardLastFour = $UserCard->last_four;
                    }

                    $sub = "Trip Invoice";
                    $type = "passenger";
                    $invoice = [
                        'payment_method'=>$serviceRequest['payment_method'],
                        'request_no'=>$serviceRequest['request_no'],
                        'currency'=>$Invoice['currency'],
                        'total'=>$Invoice['break_up']['total'],
                        'promo_discount'=>$Invoice['break_up']['promo_code_value'],
                        'basefare'=>$Invoice['break_up']['basefare'],
                        'distance_fare'=>$Invoice['break_up']['distanceFare'],
                        'waiting_time_fee'=>$Invoice['break_up']['waitTimeCost'],
                        'tax'=>$Invoice['break_up']['tax'],
                        'distance'=>$Invoice['distance_km'],
                        'duration_hr'=>$Invoice['duration_hr'],
                        'duration_min'=>$Invoice['duration_min'],
                        'duration_sec'=>$Invoice['duration_sec'],
                        'source_address'=>$Invoice['source_address'],
                        'started_on'=>$Invoice['started_on'],
                        'destination_address'=>$Invoice['destination_address'],
                        'reached_on'=>$Invoice['reached_on'],
                        'static_map'=>$serviceRequest['static_map'],
                        'last_four'=>$UserCardLastFour,
                        'date'=>date("d.m.Y")
                    ];

                    Notification::send($user, new EmailInvoiceNotification($sub,$invoice,$type));
                    //===========END TRIP INVOICE MAIL TO RIDER==================//
                    //===========SEND EMAIL TO DRIVER============================//
                    $user = User::find($ServiceRequestServiceReturn->driver_id);
                    $user->email = $user->driver_profile->email_id;
                    Notification::send($user, new EmailInvoiceNotification($sub,$invoice,$type));
                    //===========END SEND EMAIL TO DRIVER========================//

                    //===========UPDATE PASSENGER REFERRAL BALANCE===============// 
                    $ServiceRequest = ServiceRequest::where('request_id',$request->request_id)->first();
                    $ReferralCodeService = new ReferralCodeService();
                    $PassengerReferralBalance = $ReferralCodeService->getPassengerReferralBalance($ServiceRequest->passenger_id);

                    if (isset($PassengerReferralBalance)) {
                        $PassengerReferralBalance->referral_balance = (float)$PassengerReferralBalance->referral_balance - (float)$ServiceRequest->referral_value;
                        $PassengerReferralBalance->save();
                    }

                    //add referral balance to sender account after first ride and update passenger referral use
                    $request->user_id = $ServiceRequest->passenger_id;
                    $ReferralCodeService = new ReferralCodeService();
                    $getPassengerReferralUse = $ReferralCodeService->getPassengerReferralUse($ServiceRequest->passenger_id);
                    if (!isset($getPassengerReferralUse)) {
                        $ReferralCodeUse = $ReferralCodeService->ReferralCodeUse($request);
                        if(count($ReferralCodeUse)>0){
                            $request->referral_code_id = $ReferralCodeUse[0]->referral_code_id;

                            $getReferralCodeToSendBY = $ReferralCodeService->getReferralCodeToSendBY($request);

                            $request->user_id=$getReferralCodeToSendBY->user_id;
                            $request->referral_balance=(float)$getReferralCodeToSendBY->referral_amount_sent_by;

                            $ReferralCodeService->updatePassengerReferralBalance($request);
                            $ReferralCodeService->updatePassengerReferralUse($request->user_id,$ServiceRequest->passenger_id);
                        }
                    }
                    //===========END UPDATE PASSENGER REFERRAL BALANCE===============//
                    
                    //===========UPDATE DRIVER REFERRAL BALANCE======================// 
                    //add referral balance to sender account after first ride and update passenger referral use
                    $request->user_id = $ServiceRequest->driver_id;
                    $ReferralCodeService = new ReferralCodeService();
                    $getDriverReferralUse = $ReferralCodeService->getDriverReferralUse($ServiceRequest->driver_id);
                    if (!isset($getDriverReferralUse)) {
                        $ReferralCodeUse = $ReferralCodeService->ReferralCodeUse($request);
                        if(count($ReferralCodeUse)>0){
                            $request->referral_code_id = $ReferralCodeUse[0]->referral_code_id;

                            $getReferralCodeToSendBY = $ReferralCodeService->getReferralCodeToSendBY($request);

                            $request->user_id=$getReferralCodeToSendBY->user_id;
                            $request->referral_balance=(float)$getReferralCodeToSendBY->referral_amount_sent_by;

                            $ReferralCodeService->updateDriverReferralBalance($request);
                            $ReferralCodeService->updateDriverReferralUse($request->user_id,$ServiceRequest->driver_id);
                        }
                    }
                    //===========END UPDATE DRIVER REFERRAL BALANCE==================//

                    //SEND PUSH
                    $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.RIDE_COMPLETED"),
                        "text"=>trans("api.NOTIFICATION.MESSAGE.RIDE_COMPLETED"),
                        "body"=>trans("api.NOTIFICATION.MESSAGE.RIDE_COMPLETED"),
                        "type"=>"RIDECOMPLETED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                    ];
                    $noteDate=[
                        "message_type"=>"RIDECOMPLETED",
                        "message"=>trans("api.NOTIFICATION.MESSAGE.RIDE_COMPLETED"),
                        "recepient_user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "user_type"=>'passenger'
                    ];

                    if($notification == "ON"){
                        $NotificationServices->insertPushNotification((object)$noteDate);
                        $NotificationServices->sendPushNotification((object)$pushDate);
                    }
                    // send sms
                    $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                    // email
                break;
                case "COMPLETED":
                    $message=trans("api.NOTIFICATION.MESSAGE.RIDE_COMPLETED_DRIVER");
                    $status="COMPLETED";
                    $nextStep="NORMAL";
                break;
            }
            return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function tripHistory(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $requestBooking=$RequestServices->accessGetAllCompleteRequestByDriverId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['schedule_datetime']=$this->checkNull($requestBooking["data"][$key]['schedule_datetime']);
                if($requestBooking["data"][$key]['schedule_datetime']!=""){
                    $requestBooking["data"][$key]['schedule_datetime']=$this->convertFromUTC($requestBooking["data"][$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['started_from_source']=$this->checkNull($requestBooking["data"][$key]['started_from_source']);
                if($requestBooking["data"][$key]['started_from_source']!=""){
                    $requestBooking["data"][$key]['started_from_source']=$this->convertFromUTC($requestBooking["data"][$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking["data"][$key]['dropped_on_destination']=$this->checkNull($requestBooking["data"][$key]['dropped_on_destination']);
                if($requestBooking["data"][$key]['dropped_on_destination']!=""){
                    $requestBooking["data"][$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking["data"][$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking["data"][$key]['accepted_on']=$this->checkNull($requestBooking["data"][$key]['accepted_on']);
                if($requestBooking["data"][$key]['accepted_on']!=""){
                    $requestBooking["data"][$key]['accepted_on']=$this->convertFromUTC($requestBooking["data"][$key]['accepted_on'],$request->timeZone);
                }

                $requestBooking["data"][$key]['created_at']=$this->checkNull($requestBooking["data"][$key]['created_at']);
                if($requestBooking["data"][$key]['created_at']!=""){
                    $requestBooking["data"][$key]['created_at']=$this->convertFromUTC($requestBooking["data"][$key]['created_at'],$request->timeZone);
                }
                if($requestBooking["data"][$key]['request_status']=="CANCELBYDRIVER"){
                    $requestBooking["data"][$key]['started_from_source']=$requestBooking["data"][$key]['created_at'];
                }

                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking["data"][$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$requestBooking["data"][$key]['request_id']]);
                $myBooking[$key]['transaction']=$tracationsReturn['data']['TransactionLog'];

                $myBooking[$key]['transaction']->cost=(float)$myBooking[$key]['transaction']->cost-(float)$myBooking[$key]['transaction']->promo_code_value;
                if($myBooking[$key]['transaction']->cost<=0){
                    $myBooking[$key]['transaction']->cost=0.00;
                }

                $myBooking[$key]['transaction']->cost= number_format((float)($myBooking[$key]['transaction']->cost), 2, '.', '');

                $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$requestBooking["data"][$key]['driver_id']]);
                $myBooking[$key]['driverSer']=$driverSer['data'];
            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


          //  print_r($requestHistory); exit;





           // return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function tripDetails(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $PassengersProfileService=new PassengersProfileService();
            $requestBooking=$RequestServices->accessGetAllCompleteRequestByDriverId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            foreach($requestBooking as $key=>$val){
                $requestBooking[$key]['schedule_datetime']=$this->checkNull($requestBooking[$key]['schedule_datetime']);
                if($requestBooking[$key]['schedule_datetime']!=""){
                    $requestBooking[$key]['schedule_datetime']=$this->convertFromUTC($requestBooking[$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking[$key]['started_from_source']=$this->checkNull($requestBooking[$key]['started_from_source']);
                if($requestBooking[$key]['started_from_source']!=""){
                    $requestBooking[$key]['started_from_source']=$this->convertFromUTC($requestBooking[$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking[$key]['dropped_on_destination']=$this->checkNull($requestBooking[$key]['dropped_on_destination']);
                if($requestBooking[$key]['dropped_on_destination']!=""){
                    $requestBooking[$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking[$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking[$key]['accepted_on']=$this->checkNull($requestBooking[$key]['accepted_on']);
                if($requestBooking[$key]['accepted_on']!=""){
                    $requestBooking[$key]['accepted_on']=$this->convertFromUTC($requestBooking[$key]['accepted_on'],$request->timeZone);
                }
                $requestBooking[$key]['created_at']=$this->checkNull($requestBooking[$key]['created_at']);
                if($requestBooking[$key]['created_at']!=""){
                    $requestBooking[$key]['created_at']=$this->convertFromUTC($requestBooking[$key]['created_at'],$request->timeZone);
                }
                if($requestBooking[$key]['request_status']=="CANCELBYDRIVER"){
                    $requestBooking[$key]['started_from_source']=$requestBooking[$key]['created_at'];
                }
                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking[$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking[$key];
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$requestBooking[$key]['request_id']]);
                $myBooking[$key]['transaction']=$tracationsReturn['data']['TransactionLog'];



                $myBooking[$key]['transaction']->cost=(float)$myBooking[$key]['transaction']->cost-(float)$myBooking[$key]['transaction']->promo_code_value;
                if($myBooking[$key]['transaction']->cost<=0){
                    $myBooking[$key]['transaction']->cost=0.00;
                }

                $myBooking[$key]['transaction']->cost= number_format((float)($myBooking[$key]['transaction']->cost), 2, '.', '');

                $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$requestBooking[$key]['driver_id']]);
                $myBooking[$key]['driverSer']=$driverSer['data'];

                $ProfileData=$PassengersProfileService->accessGetProfile((object)['user_id'=>$requestBooking[$key]['passenger_id']]);

                $myBooking[$key]['ProfileData']=$ProfileData['data'];
            }
            $requestBooking= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking[0],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


          //  print_r($requestHistory); exit;





           // return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function cancelRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id'=>'required'
            ];


            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            // get request details,
            $ServiceRequestService=new ServiceRequestService();
            $RequestServices=new RequestServices();
            $DriverProfile=new DriversProfileService();
            $SettingServices=new SettingServices();
            $TransactionLogService=new TransactionLogService();
            $NotificationServices =new NotificationServices();

            $SettingServices=new SettingServices();
            $notification=$SettingServices->getStatusByKey("notification");

            $getRequestByID=$ServiceRequestService->accessGetRequestByID($request);
            //print_r($getSerReqLog); exit;
            if($getRequestByID->request_status=="ACCEPTED" || $getRequestByID->request_status=="REACHED")
            {
                $getRequestByID->request_status="CANCELBYDRIVER";
                $getRequestByID->save();
                // get service log object
                $getSerReqLog=$RequestServices->accessGetSerReqLog($getRequestByID);
                foreach($getSerReqLog as $key=>$val){
                    if($val['status']=="ONRIDE"){
                        // update the driver profile status and service log status
                        $updateServiceLogStatus=$RequestServices->accessUpdateStatusWithReason((object)["request_id"=>$val['request_id'],"driver_id"=>$val['driver_id'],"status"=>"CANCELBYDRIVER","cancel_reason_id"=>$request->cancel_reason_id,"reason_optional"=>$request->reason_optional],$status="ONRIDE");
                        $updateProfile=$DriverProfile->accessSetRequestToDriver((object)['driver_service_status'=>"ACTIVE","driver_id"=>$val['driver_id']],$service_status="ONRIDE");
                    }
                }
                $cancelAmount=$SettingServices->getValueByKey("driver_cancellation_charge");
                $cancelationCharger=[
                "request_id"=>$request->request_id,
                'ride_insurance'=>0,
                'per_minute'=>0,
                'per_distance_km'=>0,
                'minimum_waiting_time_in_minutes'=>0,
                'waiting_charge_per_min'=>0,
                'waitTime'=>0,
                'duration_hr'=>0,
                'duration_min'=>0,
                'duration_sec'=>0,
                'duration'=>0,
                'distance_km'=>0,
                'distance_miles'=>0,
                'distance_meters'=>0,
                "cost"=>$cancelAmount,
                "payment_method"=>"NONE",
                "payment_gateway_charge"=>0,
                'types'=>'CREDIT',
                "transaction_type"=>"CANCELBYDRIVERCHARGE",
                "is_paid"=>"N",
                "status"=>"PENDING"
                ];
                $TransactionLogService->createCreditNoteForRide($cancelationCharger);
                $message=trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_BY_DRIVER", [ 'cancelAmount' =>$cancelAmount, 'currency' => '₦' ]);//"Ride request cancelled by you. The cancellation charge of $cancelAmount ₦ will be deducted from your wallet.";
                $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                        "text"=>$message,
                        "body"=>$message,
                        "type"=>"CANCELBYDRIVER",
                        "user_id"=>$getRequestByID->driver_id,
                        "setTo"=>'SINGLE'
                        ];
                        $noteDateD=[
                            "message_type"=>"CANCELBYDRIVER",
                            "message"=>$message,
                            "recepient_user_id"=>$getRequestByID->driver_id,
                            "user_type"=>'driver'
                        ];
                           
                        if($notification=="ON"){
                            $NotificationServices->insertPushNotification((object)$noteDateD);
                            $NotificationServices->sendPushNotification((object)$pushDate);
                        }
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        $pushDate=[
                            "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                            "text"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_DRIVER_TO_PASSENGER"),
                            "body"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_DRIVER_TO_PASSENGER"),
                            "type"=>"CANCELBYDRIVER",
                            "user_id"=>$getRequestByID->passenger_id,
                            "setTo"=>'SINGLE'
                            ];
                            $noteDateP=[
                                "message_type"=>"CANCELBYDRIVER",
                                "message"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_DRIVER_TO_PASSENGER"),
                                "recepient_user_id"=>$getRequestByID->passenger_id,
                                "user_type"=>'passenger'
                            ];
                            if($notification=="ON"){
                                $NotificationServices->insertPushNotification((object)$noteDateP);
                                $NotificationServices->sendPushNotification((object)$pushDate);
                            }
                            $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
            }
            else{

                $message=trans("api.SYSTEM_MESSAGE.ON_RIDE");
            }
            return response(['message'=>$message,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }

}
