<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\DriversProfileService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use App\Services\EmergencyContact;
use App\Services\DriversServiceType;
use App\Model\Referral\ReferralAmount;
use App\Services\ReferralCodeService;
use Validator;

class RegistrationController extends Controller
{
    //=====Driver Registration========//
    public function register(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");

            $rule=[
                'social_unique_id' => ['required_if:login_by,facebook,google,apple','unique:users'],
                'device_type' => 'required|in:web,android,ios',
                'device_token' => 'required',
                'device_id' => 'required',
                'login_by' => 'required|in:manual,facebook,google,apple',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'mobile_no' => 'required|max:10|unique:drivers_profile',
                'isdCode' => 'required',
                'password' => 'required_if:login_by,manual|between:6,255|confirmed',
                'timeZone'=>'required',
                'service_type_id'=>'required',
                'model'=>'required',
                'make'=>'required',
                'registration_no'=>'required',
                'model_year'=>'required',
                'isActive'=>'required',
                'city'=>'required',
                'vehicle_color'=>'required',
                //'gender'=>'required|in:Male,Female,Transgender,Others',
                //'address' => 'required',
                'registration_expire'=>'required',
                //'vechile_identification_no'=>'required'
                'email_id' => 'required|email|max:255|unique:drivers_profile',
                //'picture' => ['required_if:login_by,manual','mimes:jpeg,jpg,bmp,png']
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            //======= verify OTP ==================
            if((int)$request->isActive!==1){
               return response(['message'=>trans("api.SYSTEM_MESSAGE.PHONE_NUMBER_NOT_VERIFED"),"data"=>(object)[],"errors"=>array("exception"=>["SMS Gateway error"],"e"=>[])],403);
            }
            //======================

            if($request->login_by=="manual" || isset($_FILES['picture'])){
                $rule=[
                    'picture' => 'required|mimes:jpeg,jpg,bmp,png'
                ];
                $validator=$this->requestValidation($request->all(),$rule);
                if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
             }

            $request->user_scope="driver-service";
            $request->username=$request->mobile_no;
            $User=new UserService();
            $Profile=new DriversProfileService();
            $UserDevice=new UsersDevices();
            $DriversServiceType=new DriversServiceType();

            $ProfileData=(object)[];
            $DevicesData=(object)[];
            $EmergencyContactData=[];
            $UserData=$User->accessCreateUser($request);
            if((int)$UserData->id>0){
                $request->user_id=(int)$UserData->id;
                $ProfileData=$Profile->accessCreateProfile($request);
                if($request->login_by=="manual"){
                    $Profile->accessProfileImageUpdate($request);
                    $ProfileData= $Profile->accessGetProfile($request);
                    $ProfileData=$ProfileData['data'];
                }
                $DevicesData=$UserDevice->accessCreateDevices($request);
                $DriversServiceTypeData=$DriversServiceType->accessCreate($request);
            }
            if(!empty($UserData)){
                if($UserData->id>0){
                    //autologin to the system.
                    $UserAuth=new AuthService();
                    $UserToken=$UserAuth->accessAutoLogin((object)['user_id'=>$UserData->id,"user_scope"=>"driver-service"]);
                    if($UserToken['statusCode']==200){
                        //referral code
                        $ReferralAmount = ReferralAmount::where('user_type','driver')->first();
                        $request->referral_amount_id=$ReferralAmount->referral_amount_id;
                        $ReferralCodeService = new ReferralCodeService();
                        $ReferralCodeService->accessInsertReferralCode($request);

                        $UserAccessToken=$UserToken['data']->access_token;
                        $token_type=$UserToken['data']->token_type;
                        return response(['message'=>trans("api.SYSTEM_MESSAGE.LOGGED_IN"),"data"=>(object)["access_token"=>$UserAccessToken,"token_type"=>$token_type,"user_profile"=>$Profile->setProfileData($ProfileData,$request->login_by),"driver_services"=>$DriversServiceTypeData['data']],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],201);
                    }
                    else{
                        return response(['message'=>$UserToken['message'],"data"=>$UserToken['data'],"errors"=>$UserToken['errors']],$UserToken['statusCode']);
                    }
                }
            }
            return response(['message'=>trans("api.SYSTEM_MESSAGE.REGISTRATION_FAILED"),"data"=>(object)[],"errors"=>array("exception"=>["Not Found"],"e"=>[])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }

    //=====Send OTP for mobile verification========
    public function onlyOTP(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'mobile_no' => 'required|max:10|unique:drivers_profile',
                'isdCode' => 'required',
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $Profile=new DriversProfileService();
            //======= send OTP using==================
            $ProfileRetun=$Profile->accessSendOTPFirst($request);
            if($ProfileRetun['statusCode']!==201){
                return response(['message'=>$ProfileRetun['message'],"data"=>(object)[],"errors"=>array("exception"=>["SMS Gateway error"],"e"=>$ProfileRetun)],400);
            }
            return response(['message'=>trans("api.SYSTEM_MESSAGE.PHONE_NUMBER_VERIFED"),"data"=>(object)["OTP"=>$ProfileRetun['data']->otp],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }

    }

}
