<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PayStackServices;
use Illuminate\Support\Facades\Auth;
use App\Model\Profiles\DriverSubaccount;
use App\Model\Profiles\DriverProfiles;

class SubaccountController extends Controller
{
    //============================not used===================================
    //**************CREATE DRIVER'S PAYMENT GATEWAY SUB-ACCOUNT**************//
    public function createSubaccount(Request $request)
    {
        try {
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule = [
                "timeZone"=>"required",
                "account_bank" => "required",
                "account_number" => "required",
                "business_name" => "required",
                "business_email" => "required"
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ 
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); 
            };
            $request->country = "NG";
            $request->business_contact = $request->business_contact_name;
            $request->business_contact_mobile = $request->business_contact_mobile;
            $request->business_mobile = $request->business_mobile;
            $request->split_type = "percentage";
            $request->split_value = 0;
            $user_id = Auth::user()->id;

            $driverSubaccount = DriverSubaccount::where('user_id',$user_id)->first();
            if(isset($driverSubaccount)){
                return response(['status'=>"false",'message'=>"Driver already created subaccount.","data"=>(object)[],"errors"=>""],400);
            } else {
                $PayStackServices = new PayStackServices();
                $result = $PayStackServices->makeSubaccount($request);

                if($result['statusCode']==400){
                    return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
                } else {
                    //insert DriverSubaccount
                    $DriverSubaccount = new DriverSubaccount;
                    $DriverSubaccount->user_id = $user_id;
                    $DriverSubaccount->id = $result['data']['id'];
                    $DriverSubaccount->subaccount_id = $result['data']['subaccount_id'];
                    $DriverSubaccount->save();

                    $DriverProfiles = DriverProfiles::where('user_id',$user_id)->first();
                    $DriverProfiles->isPaymentGatewaySubaccountExist = 1;
                    $DriverProfiles->save();

                    return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
                }
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }

    //***********GET DRIVER'S PAYMENT GATEWAY SUB-ACCOUNT******************//
    public function getSubaccount(Request $request)
    {
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ 
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422);
            };
            $user_id = Auth::user()->id;
            $driverSubaccount = DriverSubaccount::where('user_id',$user_id)->first();
            if(isset($driverSubaccount)){
                $request->id = $driverSubaccount->id;
                $PayStackServices = new PayStackServices();
                $result = $PayStackServices->accessGetSubaccount($request);
                return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
            } else {
                return response(['status'=>"false",'message'=>"Subaccount not found.","data"=>(object)[],"errors"=>""],200);
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    //**************UPDATE DRIVER'S PAYMENT GATEWAY SUB-ACCOUNT**************//
    public function updatePaymentGatewaySubaccount(Request $request)
    {
        try {
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule = [
                "timeZone"=>"required"
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); 
            };
            $request->account_bank = $request->account_bank;
            $request->account_number = $request->account_number;
            $request->business_name = $request->business_name;
            $request->business_email = $request->business_email;
            $request->business_mobile = $request->business_mobile;
            $request->business_contact = $request->business_contact_name;
            $request->business_contact_mobile = $request->business_contact_mobile;
            
            $user_id = Auth::user()->id;
            $driverSubaccount = DriverSubaccount::where('user_id',$user_id)->first();
            if(isset($driverSubaccount)){
                $request->id = $driverSubaccount->id;
                $PayStackServices = new PayStackServices();
                $result = $PayStackServices->updateSubaccount($request);

                if($result['statusCode']==400){
                    return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
                } else {
                    return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
                }
                
            } else {
                return response(['status'=>"false",'message'=>"Subaccount not found.","data"=>(object)[],"errors"=>""],200);
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    //==========================================================================

    //**************CREATE DRIVER'S PAYMENT GATEWAY BENEFICIARY**************//
    public function createBeneficiary(Request $request)
    {
        try {
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule = [
                "timeZone"=>"required",
                "account_number" => "required",
                "account_bank" => "required",
                "beneficiary_name" => "required"
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ 
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); 
            };

            $request->account_number = $request->account_number;
            $request->account_bank = $request->account_bank;
            $request->beneficiary_name = $request->beneficiary_name;
            $user_id = Auth::user()->id;

            $driverSubaccount = DriverSubaccount::where('user_id',$user_id)->first();

            if(isset($driverSubaccount)){
                return response(['status'=>"false",'message'=>"Beneficiary already added to your account.","data"=>(object)[],"errors"=>""],400);
            } else {
                
                $PayStackServices = new PayStackServices();
                $result = $PayStackServices->makeBeneficiary($request);
                if($result['statusCode']==400){
                    return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
                } else {
                    //insert DriverSubaccount
                    $DriverSubaccount = new DriverSubaccount;
                    $DriverSubaccount->user_id = $user_id;
                    $DriverSubaccount->id = $result['data']['id'];
                    $DriverSubaccount->save();

                    $DriverProfiles = DriverProfiles::where('user_id',$user_id)->first();
                    $DriverProfiles->isPaymentGatewaySubaccountExist = 1;
                    $DriverProfiles->save();

                    return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
                }
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }

    //***********GET DRIVER'S PAYMENT GATEWAY BENEFICIARY******************//
    public function getBeneficiary(Request $request)
    {
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ 
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422);
            };
            $user_id = Auth::user()->id;
            $driverSubaccount = DriverSubaccount::where('user_id',$user_id)->first();
            if(isset($driverSubaccount)){
                $request->id = $driverSubaccount->id;
                $PayStackServices = new PayStackServices();
                $result = $PayStackServices->accessGetBeneficiary($request);
                return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
            } else {
                return response(['status'=>"false",'message'=>"Beneficiary not found.","data"=>(object)[],"errors"=>""],200);
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    //***********GET BANK DETAILS******************//
    public function getBankDetails(Request $request)
    {
        try{
            $PayStackServices = new PayStackServices();
            $result = $PayStackServices->accessGetBankDetails();
            
            return response(['status'=>$result['status'],'message'=>$result['message'],"data"=>$result['data'],"errors"=>""],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

}
