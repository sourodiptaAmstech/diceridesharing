<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Services\MessageService;
use App\Services\ServiceRequestService;
use App\Services\NotificationServices;
use App\Services\PaymentService;
use App\Services\AdminSuportChatService;
use Validator;
use App\Services\SettingServices;


class MessageController extends Controller
{
    public function getMessage(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");

            $rule=[
                'request_id'=>'required',
                'timeZone'=>'required',
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422);
            };
            $request->user_id=Auth::user()->id;
            $MessageService=new MessageService();
            $mes=$MessageService->accessGetMessageOnRide($request->request_id,$request['timeZone']);







            return response(['message'=>'message',"data"=>$mes,"errors"=>[]],(int)200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function sendMessage(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");

            $rule=[
                'request_id'=>'required',
                'timeZone'=>'required',
                'user_scope'=>'required|in:driver-service,passenger-service',
                'message'=>'required'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422);
            };
            $request->user_id=Auth::user()->id;

            $MessageService=new MessageService();
            $mes=$MessageService->accessCreateMessage($request);
            // find reciver user id
            // get request id
            $ServiceRequestService = new ServiceRequestService();
            $requestDetails= $ServiceRequestService->accessGetRequestByID($request);

            $SettingServices=new SettingServices();
            $notification=$SettingServices->getStatusByKey("notification");

            $pushDate=(object)[];
            if($request->user_scope=="driver-service"){
                // find passenger id
              //  print_r($requestDetails->passenger_id);exit;
                $pushDate=[
                    "title"=>trans("api.NOTIFICATION.TITLE.PASSENGER_CHAT"),
                    "text"=>$request->message,
                    "body"=>$request->message,
                    "type"=>"CHATNORMAL",
                    "user_id"=>$requestDetails->passenger_id,
                    "setTo"=>'SINGLE'
                ];
                $noteDate=[
                    "message_type"=>"CHATNORMAL",
                    "message"=>$request->message,
                    "recepient_user_id"=>$requestDetails->passenger_id,
                    "user_type"=>'passenger'
                ];

            }
            else{
                // find driver
                    $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.DRIVER_CHAT"),
                        "text"=>$request->message,
                        "body"=>$request->message,
                        "type"=>"CHATNORMAL",
                        "user_id"=>$requestDetails->driver_id,
                        "setTo"=>'SINGLE'
                    ];
                    $noteDate=[
                        "message_type"=>"CHATNORMAL",
                        "message"=>$request->message,
                        "recepient_user_id"=>$requestDetails->driver_id,
                        "user_type"=>'driver'
                    ];
                    //$NotificationServices->sendPushNotification((object)$pushDate);

            }
            if($notification == "ON"){
                $NotificationServices =new NotificationServices();
                $NotificationServices->insertPushNotification((object)$noteDate);
                $NotificationServices->sendPushNotification((object)$pushDate);
            }

            return response(['message'=>trans("api.SYSTEM_MESSAGE.MESSAGE_SENT"),"data"=>(object)[],"errors"=>[]],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }


    public function sendMessageForSupport(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'thread_id'=>'required_if:user_scope,admin-service',
                'timeZone'=>'required',
                'user_scope'=>'required|in:driver-service,passenger-service,admin-service',
                'message'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422);
            };
            $request->user_id=Auth::user()->id;
            if($request->user_scope!="admin-service"){
                $request->thread_id= $request->user_id;
            }

            $AdminSuportChatService=new AdminSuportChatService();
            $mes=$AdminSuportChatService->accessCreateMessageAdminSupport($request);
            // find reciver user id
            // get request id

            $SettingServices=new SettingServices();
            $notification=$SettingServices->getStatusByKey("notification");

            $pushDate=[];
            if($request->user_scope=="admin-service"){
                // find passenger id
              //  print_r($requestDetails->passenger_id);exit;
                $pushDate=[
                    "title"=>trans("api.NOTIFICATION.TITLE.ADMIN_CHAT"),
                    "text"=>$request->message,
                    "body"=>$request->message,
                    "type"=>"CHATADMIN",
                    "user_id"=>$request->thread_id,
                    "setTo"=>'SINGLE'
                ];
                $noteDate=[
                    "message_type"=>"CHATADMIN",
                    "message"=>$request->message,
                    "recepient_user_id"=>$request->thread_id,
                    "user_type"=>'passenger'
                ];

            }
            if(count($pushDate)>0){
                if($notification == "ON"){
                    $NotificationServices =new NotificationServices();
                    $NotificationServices->insertPushNotification((object)$noteDate);
                    $NotificationServices->sendPushNotification((object)$pushDate);
                }
            }
            return response(['message'=>trans("api.SYSTEM_MESSAGE.MESSAGE_SENT"),"data"=>(object)[],"errors"=>[]],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function getMessageForSupport(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");


            $rule=[
                'thread_id'=>'required_if:user_scope,admin-service',
                'timeZone'=>'required',
                'user_scope'=>'required|in:driver-service,passenger-service,admin-service'

            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422);
            };
            $request->user_id=Auth::user()->id;
            if($request->user_scope!="admin-service"){
                $request->thread_id= $request->user_id;
            }
            $AdminSuportChatService=new AdminSuportChatService();
            $mes=$AdminSuportChatService->accessGetMessageForSupport($request->thread_id,$request['timeZone']);

            return response(['message'=>'message',"data"=>$mes,"errors"=>[]],(int)200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }



}
