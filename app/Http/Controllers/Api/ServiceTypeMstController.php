<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ServiceTypeMst;

use Validator;
class ServiceTypeMstController extends Controller
{
    public function get(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $ServiceTypeMst=new ServiceTypeMst();
            $ServiceTypeMstData=$ServiceTypeMst->accessGet();
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SERVICE_LIST"),"data"=>$ServiceTypeMstData,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
    public function getForLeasingList(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $ServiceTypeMst=new ServiceTypeMst();
            $ServiceTypeMstData=$ServiceTypeMst->accessGetLeasing();
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SERVICE_LIST"),"data"=>$ServiceTypeMstData,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
}
