<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Services\PassengersProfileService;
use App\Services\EmergencyContact;
use App\Services\UserService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use Illuminate\Support\Facades\Hash;
use App\Services\UserTrasactionEmailServices;
use App\Services\FamilyAccountServices;
use Validator;

class ProfileController extends Controller
{
    private function addEmergencyNo($data){
        $emergencyNumber=[];
        $ruleOne=[
            'emergOne_contact_no' => 'required',
            'emergOne_name'=>'required',
            'emergOne_isdCode'=>'required'
        ];
        $ruleTwo=[
            'emergTwo_contact_no' => 'required',
            'emergTwo_name'=>'required',
            'emergTwo_isdCode'=>'required'
        ];
        $EmergencyContact=new EmergencyContact();
        $validator=$this->requestValidation($data->all(),$ruleOne);
        if($validator->status!=="false"){
            if($data->emergOne_id==0)
                $EmergencyContact->accessCreateContact((object)["contact_no"=>$data->emergOne_contact_no,"name"=>$data->emergOne_name,"user_id"=>$data->user_id,"isd_code"=>$data->emergOne_isdCode]);
            else
                $EmergencyContact->accessUpdateContact((object)["contact_no"=>$data->emergOne_contact_no,"name"=>$data->emergOne_name,"user_id"=>$data->user_id,"emergency_id"=>$data->emergOne_id,"isd_code"=>$data->emergOne_isdCode]);
        };
        $validator=$this->requestValidation($data->all(),$ruleTwo);
        if($validator->status!=="false"){
            if($data->emergTwo_id==0)
                $EmergencyContact->accessCreateContact((object)["contact_no"=>$data->emergTwo_contact_no,"name"=>$data->emergTwo_name,"user_id"=>$data->user_id,"isd_code"=>$data->emergTwo_isdCode]);
            else
                $EmergencyContact->accessUpdateContact((object)["contact_no"=>$data->emergTwo_contact_no,"name"=>$data->emergTwo_name,"user_id"=>$data->user_id,"emergency_id"=>$data->emergTwo_id,"isd_code"=>$data->emergTwo_isdCode]);

        }
        return true ;
    }

    public function updateMobileNo(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            //$request['device_type']=$request->header("deviceType");
           // $request['device_latitude']=$request->header("deviceLatitude");
           // $request['device_longitude']=$request->header("deviceLongitude");
            $rule=[
                'mobile_no' => ['required',Rule::unique('passengers_profile')->where(function($query){
                    return $query->where("user_id","!=",Auth::user()->id);
                })],
                'timeZone'=>'required',
                'isdCode'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;

            $this->addEmergencyNo($request);

            $Profile=new PassengersProfileService();
            $ProfileRetun=$Profile->accessUpdateOnlyMobile($request);
            return response(['message'=>$ProfileRetun['message'],"data"=>$ProfileRetun['data'],"errors"=>$ProfileRetun['errors']],(int)$ProfileRetun['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
    public function emergencyContact(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            //$request['device_type']=$request->header("deviceType");
           // $request['device_latitude']=$request->header("deviceLatitude");
           // $request['device_longitude']=$request->header("deviceLongitude");
            $rule=[
               // 'email_id' => 'required|email|max:255|unique:passengers_profile',
                'timeZone'=>'required',
                "emergOne_isdCode"=>'required',
                "emergOne_contact_no"=>'required',
                "emergOne_name"=>'required',
                "emergOne_id"=>'required',
                "emergTwo_isdCode"=>'required',
                "emergTwo_contact_no"=>'required',
                "emergTwo_name"=>'required',
                "emergTwo_id"=>'required'
            ];
            if($request->email_id!=="" &&$request->email_id!==null ){
                $rule['email_id']=['required',Rule::unique('passengers_profile')->where(function($query){
                    return $query->where("user_id","!=",Auth::user()->id);
                })];
            }
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;

            $this->addEmergencyNo($request);

            $Profile=new PassengersProfileService();
            $ProfileRetun=$Profile->accessUpdateOnlyEmail($request);





            $EmergencyContact=new EmergencyContact();
            $UserAuth=new AuthService();
            $request->login_by=Auth::user()->login_type;
            //logut all access user
            $UserAuth->accesslogoutTempUser((object)['user_id'=>$request->user_id,"user_scope"=>"passenger-service"]);
            $ProfileData=$Profile->accessGetProfile($request);
            $EmergencyContactData=$EmergencyContact->accessGetContact($request);

            if(!empty($request)){
                if($request->user_id>0){

                    $UserToken=$UserAuth->reLogin((object)['user_id'=>$request->user_id,"user_scope"=>$request->user_scope]);
                    if($UserToken['statusCode']==200){
                        $UserAccessToken=$UserToken['data']->access_token;
                        $token_type=$UserToken['data']->token_type;
                        return response(['message'=>"Profile Update","data"=>(object)["access_token"=>$UserAccessToken,"token_type"=>$token_type,"user_profile"=>$Profile->setProfileData($ProfileData['data'],$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],201);
                    }
                    else{
                        return response(['message'=>$UserToken['message'],"data"=>$UserToken['data'],"errors"=>$UserToken['errors']],$UserToken['statusCode']);
                    }
                }
            }




            return response(['message'=>"Profile Update","data"=>(object)[],"errors"=>$ProfileRetun['errors']],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function verifyMobileNo(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'isValid'=>'required'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $Profile=new PassengersProfileService();
            $EmergencyContact=new EmergencyContact();
            $UserAuth=new AuthService();

            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;
            $request->login_by=Auth::user()->login_type;

            $ProfileRetun=$Profile->accessVerifyMobileNo($request);

            // logout
           //logut all access user
           $UserAuth->accesslogoutTempUser((object)['user_id'=>$request->user_id,"user_scope"=>"passenger-service"]);

           // $UserAuth->accessLogoutTempUser($request,"Passenger-Temp");


            $ProfileData=$Profile->accessGetProfile($request);
            $EmergencyContactData=$EmergencyContact->accessGetContact($request);


            // login
            if(!empty($request)){
                if($request->user_id>0){

                    $UserToken=$UserAuth->reLogin((object)['user_id'=>$request->user_id,"user_scope"=>$request->user_scope]);
                    if($UserToken['statusCode']==200){
                        $UserAccessToken=$UserToken['data']->access_token;
                        $token_type=$UserToken['data']->token_type;
                        return response(['message'=>$ProfileRetun['message'],"data"=>(object)["access_token"=>$UserAccessToken,"token_type"=>$token_type,"user_profile"=>$Profile->setProfileData($ProfileData['data'],$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],201);
                    }
                    else{
                        return response(['message'=>$UserToken['message'],"data"=>$UserToken['data'],"errors"=>$UserToken['errors']],$UserToken['statusCode']);
                    }
                }
            }
            return response(['message'=>$ProfileRetun['message'],"data"=>$ProfileRetun['data'],"errors"=>$ProfileRetun['errors']],(int)$ProfileRetun['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function resetMobileVerificationOtp(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;

            $Profile=new PassengersProfileService();
            $ProfileData=$Profile->accessGetProfile($request);

            if($ProfileData['statusCode']==200){
                $request->mobile_no=$ProfileData['data']->mobile_no;
                $request->isdCode=$ProfileData['data']->isd_code;
            }
         //   print_r($ProfileData); exit;
            $ProfileRetun=$Profile->accessUpdateOnlyMobile($request);

            return response(['message'=>$ProfileRetun['message'],"data"=>$ProfileRetun['data'],"errors"=>$ProfileRetun['errors']],(int)$ProfileRetun['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function getProfile(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
           // $request['device_type']=$request->header("deviceType");
           // $request['device_latitude']=$request->header("deviceLatitude");
           // $request['device_longitude']=$request->header("deviceLongitude");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;
            $request->login_by=Auth::user()->login_type;
            $Profile=new PassengersProfileService();
            $EmergencyContact=new EmergencyContact();
            $ProfileData=$Profile->accessGetProfile($request);
            $EmergencyContactData=$EmergencyContact->accessGetContact($request);
            return response(['message'=>"Profile Data","data"=>(object)["user_profile"=>$Profile->setProfileData($ProfileData['data'],$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function profileImageUpdate(Request $request){
        try{
            $rule=['picture' => 'required|mimes:jpeg,bmp,png'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")])],422); };
            $request->user_id=Auth::user()->id;
            $Profile=new PassengersProfileService();
            $ProfileData=$Profile->accessProfileImageUpdate($request);

            return response(['message'=>$ProfileData['message'],"data"=>$ProfileData['data'],"errors"=>$ProfileData['errors']],$ProfileData['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"User Not Found","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }
    }

    public function updateProfile(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");

            $rule=[
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'gender'=>'sometimes|nullable|in:Male,Female,Transgender,Others',
                'timeZone'=>'required'
            ];
            if($request->email_id!=="" &&$request->email_id!==null ){
                $rule['email_id']=['required',Rule::unique('passengers_profile')->where(function($query){
                    return $query->where("user_id","!=",Auth::user()->id);
                })];
            }
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;
            $request->login_by=Auth::user()->login_type;
            $this->addEmergencyNo($request);
            $Profile=new PassengersProfileService();
            $EmergencyContact=new EmergencyContact();
            $ProfileRetun=$Profile->accessUpdateProfile($request);
            $ProfileData=$Profile->accessGetProfile($request);
            $EmergencyContactData=$EmergencyContact->accessGetContact($request);

            return response(['message'=>$ProfileRetun['message'],"data"=>(object)["user_profile"=>$Profile->setProfileData($ProfileData['data'],$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>$ProfileRetun['errors']],(int)$ProfileRetun['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function changePassword(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
        //    $request['device_type']=$request->header("deviceType");
          //  $request['device_latitude']=$request->header("deviceLatitude");
           // $request['device_longitude']=$request->header("deviceLongitude");
            $rule=[
                'timeZone'=>'required',
                'old_password' => 'required|between:6,255',
                'password' => 'required|between:6,255|confirmed'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            if (!(Hash::check($request->old_password, Auth::user()->password))) {
                return response(['message'=>trans("api.SYSTEM_MESSAGE.OLD_PASSWORD_NOT_MATCH"),"field"=>'old_password',"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422);

	        }
	        if(strcmp($request->old_password, $request->password) == 0){
                return response(['message'=>trans("api.SYSTEM_MESSAGE.NEW_PASSWORD_SAME_AS_OLD"),"field"=>'password',"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422);
            }

            $request->user_id=Auth::user()->id;
            $request->user_scope="passenger-service";
            $UserService=new UserService();
            $UserServiceReturn=$UserService->accessUpdatePassword($request);
            return response(['message'=>$UserServiceReturn['message'],"data"=>$UserServiceReturn['data'],"errors"=>$UserServiceReturn['errors']],$UserServiceReturn['statusCode']);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
    public function updateLang(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
        //    $request['device_type']=$request->header("deviceType");
          //  $request['device_latitude']=$request->header("deviceLatitude");
           // $request['device_longitude']=$request->header("deviceLongitude");
            $rule=[
                'timeZone'=>'required',
                'lang' => 'required|in:en,fr'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $request->user_scope="passenger-service";
            $PassengersProfileService=new PassengersProfileService();
           $returnData= $PassengersProfileService->accessUpdateLang($request);
            return response(['message'=>$returnData['message'],"data"=>(object)[],"errors"=>$returnData['errors']],$returnData['statusCode']);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }


    public function updateTranscationEmail(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'email_id' => ['required',Rule::unique('user_transaction_email')->where(function($query){
                    return $query->where("user_id","!=",Auth::user()->id)->where("isVerified",1);
                })],
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $request->user=Auth::user();
            // find user in the user transaction email
            $UserTrasactionEmailServices =new UserTrasactionEmailServices();
            $find = $UserTrasactionEmailServices->accessTrasactionEmail($request);
            return response(['message'=>"A verification email has been sent to your email id ","data"=>(object)[],"errors"=>[]],201);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }


    public function familyRegister(Request $request){
        try{
               // 'mobile' => 'required|unique:users',
               $request['timeZone']=$timeZone=$request->header("timeZone");
            //   $request['device_type']=$request->header("device_type");
               //$request['device_latitude']=$request->header("deviceLatitude");
              // $request['device_longitude']=$request->header("deviceLongitude");

               $rule=['social_unique_id' => ['required_if:login_by,facebook,google','unique:users'],
               'device_type' => 'required|in:web,android,ios',
               'device_token' => 'required',
               'device_id' => 'required',
               'login_by' => 'required|in:manual,facebook,google',
               'first_name' => 'required|max:255',
               'last_name' => 'required|max:255',
               //'address' => 'required',
              'email_id' => 'required|email|max:255|unique:passengers_profile',
              'mobile_no' => 'required|max:10|unique:passengers_profile',
              'isdCode' => 'required',
              'password' => 'required_if:login_by,manual|between:6,255|confirmed',
              'timeZone'=>'required',
              'isActive'=>'required',
              'relationship'=>'required'

            ];
            $validator=$this->requestValidation($request->all(),$rule);
            $request->parent_account_id=Auth::user()->id;
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
             //======= verify OTP ==================
             if($request->isActive!==1){
                return response(['message'=>"Please verify your phone number.","data"=>(object)[],"errors"=>array("exception"=>["SMS Gateway error"],"e"=>[])],403);
             }

             if($request->login_by=="manual" || isset($_FILES['picture'])){
                $rule=[
                    'picture' => 'sometimes|mimes:jpeg,jpg,bmp,png'
                ];
                $validator=$this->requestValidation($request->all(),$rule);
                if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

             }

             //-======================
            $request->user_scope="passenger-service";
            $request->account_type="CHILD";
            $request->username=$request->mobile_no;
            $User=new UserService();
            $Profile=new PassengersProfileService();
            $UserDevice=new UsersDevices();
            $EmergencyContact=new EmergencyContact();
            $FamilyAccountServices=new FamilyAccountServices();
            $FamilyAccount=$FamilyAccountServices->accessgetFamilyAccount((object)["user_id"=>Auth::user()->id]);
            if(count($FamilyAccount)>=5){
                return response(['message'=>"Maximum 5 family account can be created","data"=>(object)[],"errors"=>array("exception"=>["SMS Gateway error"],"e"=>[])],400);
            }



            $ProfileData=(object)[];
            $DevicesData=(object)[];
            $EmergencyContactData=[];
            $UserData=$User->accessCreateUser($request);
            $request->child_account_id=$UserData->id;
            if((int)$UserData->id>0){
                $request->user_id=(int)$UserData->id;
                $ProfileData=$Profile->accessCreateProfile($request);
                $DevicesData=$UserDevice->accessCreateDevices($request);
                $EmergencyContactData=$EmergencyContact->accessGetContact($request);
                if(($request->login_by=="manual" && isset($_FILES['picture']))|| isset($_FILES['picture'])){
                    $Profile->accessProfileImageUpdate($request);
                    $ProfileData= $Profile->accessGetProfile($request);
                    $ProfileData=$ProfileData['data'];
                }
            }
           // dd($request); exit;
            $FamilyAccountServices->accessFamilyAccount($request);




            return response(['message'=>"Thank you for registering your family member with us!","data"=>(object)["user_profile"=>$Profile->setProfileData($ProfileData,$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],201);
          //  return response(['message'=>"Registration not possible. Contact Administrator.","data"=>(object)[],"errors"=>array("exception"=>["Not Found"],"e"=>[])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function getFamilyDetails(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
           // $request->user=Auth::user();
            // find user in the user transaction email
            $Profile=new PassengersProfileService();
            $FamilyAccountServices=new FamilyAccountServices();
            $FamilyAccount=$FamilyAccountServices->accessgetFamilyAccount($request);
            foreach($FamilyAccount as $key=>$val){
                $ProfileData=$Profile->accessGetProfile((object)["user_id"=>$val['child_account_id']]);
                $ProfileData=$ProfileData['data'];
                $FamilyAccount[$key]['profile_data']=$Profile->setProfileData($ProfileData,Auth::user()->login_type);
            }
            return response(['message'=>"","data"=>(object)["user_profile"=>$FamilyAccount,"count"=>count($FamilyAccount)],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function deleteFamilyDetails(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'family_accounts_id'=>'required',
                'child_account_id'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
           // $request->user=Auth::user();
            // find user in the user transaction email
            $FamilyAccountServices=new FamilyAccountServices();
            $FamilyAccount=$FamilyAccountServices->accessDelete($request);
          //  updateParentProfile
            $Profile=new PassengersProfileService();
            $Profile->updateParentProfile((object)['user_id'=>$request->child_account_id]);


            return response(['message'=>"You have removed your family member from your account.","data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

}
