<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Profiles\PassengersProfile;
use App\Services\NotificationServices;
use App\Services\PassengersProfileService;
use Illuminate\Support\Facades\Auth;
use App\Services\EstimatedFareService;
use App\Model\SplitFare\SplitFare;
use App\Model\Payment\UserCard;
use App\Services\ServiceRequestService;
use App\Model\SplitFare\SplitTransactionLog;


class SplitFareController extends Controller
{
    public function searchPassenger(Request $request)
    {
    	try {
    		$request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'searchQueary'=>'required'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $searchQueary = $request->searchQueary;
            $Passenger = PassengersProfile::select('user_id','first_name','last_name','picture')->where('user_id','!=',Auth::user()->id)->where(function($q)  use ($searchQueary){
                    $q->orwhere('first_name', 'like', '%'.$searchQueary.'%')
                    ->orwhere('last_name', 'like', '%'.$searchQueary.'%');
                })->get()->toArray();
            
            if(!empty($Passenger)){
	        	return response(['message'=>"Passenger found","data"=>(array)$Passenger,"error"=>(object)array() ],200);
	      	} else {
	        	return response(['message'=>"Passenger not found","data"=>array(),"errors"=>(object)array("Not_Found"=>["Passenger not found"])],404);
	      	}
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }

    public function searchPassengerPushnotification(Request $request)
    {
        try {
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'user_id'=>'required',
                'request_id'=>'required'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $user_ids = $request->user_id;
            $request->user_id=Auth::user()->id;
            $request_id = $request->request_id;
            $splitfare=SplitFare::where('request_id',$request_id)->get();
            if (count($splitfare)>0) {
                return response(['message'=>"Split fare request already sent","data"=>[],"error"=>(object)array() ],404);
            } else {

                $EstimatedFareService =new EstimatedFareService();
                $retunEsti=$EstimatedFareService->accessGetSplitFare($request);
                
                $estimated_cost=$retunEsti['estimated_cost'];
                $estimated_fare_perhead = round($estimated_cost/(count($user_ids)+1),2);
                $currency = $retunEsti['currency'];
                // $estimated_cost=$retunEsti['data'][0]['estimated_fare']['estimated_cost'];
                // $estimated_fare_perhead = $estimated_cost/count($user_ids);
                // $currency = $retunEsti['data'][0]['estimated_fare']['currency'];

                foreach ($user_ids as $key => $user_id) {
                    $Profile=new PassengersProfileService();
                    $ProfileData=$Profile->accessGetProfile($request);

                    //Send push
                    $NotificationServices =new NotificationServices();
                    $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.SPLIT_FARE_REQUEST"),
                        "text"=>trans("api.NOTIFICATION.MESSAGE.SPLIT_FARE_REQUEST"),
                        "body"=>trans("api.NOTIFICATION.MESSAGE.SPLIT_FARE_REQUEST"),
                        "type"=>"SPLITFAREREQUEST",
                        "data"=>(object)["user_profile"=>$ProfileData['data'],'request_id'=>$request_id,'split_fare'=>$estimated_fare_perhead,'currency'=>$currency,'user_id'=>$user_id],
                        "user_id"=>$user_id,
                        "setTo"=>'SINGLE'
                        ];
                    $noteDate=[
                        "message_type"=>"SPLITFAREREQUEST",
                        "message"=>trans("api.NOTIFICATION.MESSAGE.SPLIT_FARE_REQUEST"),
                        "recepient_user_id"=>$user_id,
                        "user_type"=>'passenger'
                    ];
                    $NotificationServices->insertPushNotification((object)$noteDate);
                    $NotificationServices->sendPushNotificationForSplit((object)$pushDate);

                    //store SplitFare 
                    $SplitFare = new SplitFare;
                    $SplitFare->request_id=$request_id;
                    $SplitFare->owner_id=$request->user_id;
                    $SplitFare->user_id=$user_id;
                    $SplitFare->split_amount=$estimated_fare_perhead;
                    $SplitFare->save();
                }

                return response(['message'=>"Fare split request sent successfully.","data"=>[],"error"=>(object)array() ],200);
            }

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }


    public function passengerAcceptSplitfareRequest(Request $request)
    {
        try {
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'split_status'=>'required|in:YES,NO',
                'request_id'=>'required',
                'user_id'=>'required'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            
            if($request->split_status=="YES"){
                $ServiceRequestService = new ServiceRequestService();
                $GetRequestByID = $ServiceRequestService->accessGetRequestByID($request);
                if($GetRequestByID->request_status=="ACCEPTED"||$GetRequestByID->request_status=="REACHED"||$GetRequestByID->request_status=="STARTED"){
                    $UserCard=UserCard::where('user_id', $request->user_id)->where("is_default",1)->first();
                    if(isset($UserCard)){
                        $splitfare=SplitFare::where('request_id',$request->request_id)->where('user_id',$request->user_id)->first();
                        $splitfare->is_splitted = 1;
                        $splitfare->save();

                        //Send push information to owner passenger
                        $NotificationServices = new NotificationServices();
                        
                        $pushDate=[
                            "title"=>trans("api.NOTIFICATION.TITLE.ACCEPT_SPLIT_FARE_REQUEST"),
                            "text"=>trans("api.NOTIFICATION.MESSAGE.ACCEPT_SPLIT_FARE_REQUEST"),
                            "body"=>$UserCard->passenger_profile->first_name." ".$UserCard->passenger_profile->last_name." has accepted your split fare request.",
                            "type"=>"ACCEPTSPLITFAREREQUEST",
                            "user_id"=>$splitfare->owner_id,
                            "setTo"=>'SINGLE'
                        ];
                        $noteDate=[
                            "message_type"=>"ACCEPTSPLITFAREREQUEST",
                            "message"=>$UserCard->passenger_profile->first_name." ".$UserCard->passenger_profile->last_name." has accepted your split fare request.",
                            "recepient_user_id"=>$splitfare->owner_id,
                            "user_type"=>'passenger'
                        ];
                        $NotificationServices->insertPushNotification((object)$noteDate);
                        $NotificationServices->sendPushNotification((object)$pushDate);

                        return response(['message'=>"Successfully accepted split fare request. Split amount will be deducted from your card.","data"=>[],"error"=>(object)array() ],200);
                    } else {
                        return response(['message'=>"Default card not found.","data"=>[],"error"=>(object)array() ],404);
                    }
                } else {
                    return response(['message'=>"Ride completed. You are not able to accept request.","data"=>[],"error"=>(object)array() ],406);
                }

            } else {
                return response(['message'=>"Split fare request cancelled.","data"=>[],"error"=>(object)array() ],406);
            }

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }
    }

    public function getSplitTransaction(Request $request)
    {
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[ 'timeZone'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ 
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],
                "errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); 
            };
            $request->user_id=Auth::user()->id;
            $SplitTransactionLog = SplitTransactionLog::join('service_requests as sr', 'sr.request_id','=','split_transaction_log.request_id')
            ->select("sr.request_status","sr.request_id","sr.request_no","sr.payment_method","sr.created_at","split_transaction_log.cost","split_transaction_log.currency")
            ->where('split_transaction_log.status','COMPLETED')
            ->where("split_transaction_log.split_user_id",$request->user_id)
            ->orderBy('split_transaction_log.request_id', 'desc')
            ->paginate(10)->toArray();

            foreach($SplitTransactionLog["data"] as $key=>$val){
                $SplitTransactionLog["data"][$key]['created_at']=$this->checkNull($SplitTransactionLog["data"][$key]['created_at']);
                if($SplitTransactionLog["data"][$key]['created_at']!=""){
                    $SplitTransactionLog["data"][$key]['created_at']=$this->convertFromUTC($SplitTransactionLog["data"][$key]['created_at'],$request->timeZone);
                }
            }

            return response(['message'=>"Split Transaction List","data"=>$SplitTransactionLog,"errors"=>array("exception"=>["Everything is OK"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }

}
