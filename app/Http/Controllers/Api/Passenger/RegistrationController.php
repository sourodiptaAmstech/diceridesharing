<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\PassengersProfileService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use App\Services\EmergencyContact;
use App\Model\Referral\ReferralAmount;
use App\Services\ReferralCodeService;
use Validator;

class RegistrationController extends Controller
{
   //Passenger registration
   public function register(Request $request){
    try{
        $request['timeZone']=$timeZone=$request->header("timeZone");

        $rule=['social_unique_id' => ['required_if:login_by,facebook,google,apple','unique:users'],
            'device_type' => 'required|in:web,android,ios',
            'device_token' => 'required',
            'device_id' => 'required',
            'login_by' => 'required|in:manual,facebook,google,apple',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email_id' => 'required|email|max:255|unique:passengers_profile',
            'mobile_no' => 'required|max:10|unique:passengers_profile',
            'isdCode' => 'required',
            'password' => 'required_if:login_by,manual|between:6,255|confirmed',
            'timeZone'=>'required',
            'isActive'=>'required'
        ];

        $validator=$this->requestValidation($request->all(),$rule);
        if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
        //======= verify OTP ==================
         if((int)$request->isActive!==1){
            return response(['message'=>"Please verify your phone number.","data"=>(object)[],"errors"=>array("exception"=>["SMS Gateway error"],"e"=>[])],403);
         }

         if($request->login_by=="manual" || isset($_FILES['picture'])){
            $rule=[
                'picture' => 'sometimes|mimes:jpeg,jpg,bmp,png'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

         }

        //======================
        $request->user_scope="passenger-service";
        $request->account_type="PARENT";
        $request->username=$request->mobile_no;
        $User=new UserService();
        $Profile=new PassengersProfileService();
        $UserDevice=new UsersDevices();
        $EmergencyContact=new EmergencyContact();


        $ProfileData=(object)[];
        $DevicesData=(object)[];
        $EmergencyContactData=[];
        $UserData=$User->accessCreateUser($request);
        if((int)$UserData->id>0){
            $request->user_id=(int)$UserData->id;
            $ProfileData=$Profile->accessCreateProfile($request);
            $DevicesData=$UserDevice->accessCreateDevices($request);
            $EmergencyContactData=$EmergencyContact->accessGetContact($request);
            if(($request->login_by=="manual" && isset($_FILES['picture']))|| isset($_FILES['picture'])){
                $Profile->accessProfileImageUpdate($request);
                $ProfileData= $Profile->accessGetProfile($request);
                $ProfileData=$ProfileData['data'];
            }
        }
        if(!empty($UserData)){
            if($UserData->id>0){
                //autologin to the system.
                $UserAuth=new AuthService();
                $UserToken=$UserAuth->accessAutoLogin((object)['user_id'=>$UserData->id,"user_scope"=>$request->user_scope]);
                if($UserToken['statusCode']==200){
                    //referral code
                    $ReferralAmount = ReferralAmount::where('user_type','passenger')->first();
                    $request->referral_amount_id=$ReferralAmount->referral_amount_id;
                    $ReferralCodeService = new ReferralCodeService();
                    $ReferralCodeService->accessInsertReferralCode($request);

                    $UserAccessToken=$UserToken['data']->access_token;
                    $token_type=$UserToken['data']->token_type;
                    return response(['message'=>"Thank you for registering with us!","data"=>(object)["access_token"=>$UserAccessToken,"token_type"=>$token_type,"user_profile"=>$Profile->setProfileData($ProfileData,$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],201);
                }
                else{
                    return response(['message'=>$UserToken['message'],"data"=>$UserToken['data'],"errors"=>$UserToken['errors']],$UserToken['statusCode']);
                }
            }
        }
        return response(['message'=>"Registration not possible. Contact Administrator.","data"=>(object)[],"errors"=>array("exception"=>["Not Found"],"e"=>[])],404);
    }
    catch(\Illuminate\Database\QueryException  $e){
        return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
    }
}

//Send OTP for OTP verification
public function onlyOTP(Request $request){
    try{
        $request['timeZone']=$timeZone=$request->header("timeZone");
        $rule=[
            'mobile_no' => 'required|max:10|unique:passengers_profile',
            'isdCode' => 'required',
            'timeZone'=>'required'
        ];
        $validator=$this->requestValidation($request->all(),$rule);
        if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
        $Profile=new PassengersProfileService();
        //======= send OTP using==================
        $ProfileRetun=$Profile->accessSendOTPFirst($request);
        if($ProfileRetun['statusCode']!==201){
            return response(['message'=>$ProfileRetun['message'],"data"=>(object)[],"errors"=>array("exception"=>["SMS Gateway error"],"e"=>$ProfileRetun)],400);
        }
        return response(['message'=>"Thank you for verifing your phone no with us!","data"=>(object)["OTP"=>$ProfileRetun['data']->otp],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],201);
    }
    catch(\Illuminate\Database\QueryException  $e){
        return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
    }

}

}
