<?php

namespace App\Services;

use App\Model\ServiceType\MstServiceType;

class ServiceTypeMst
{
    private function create($data){
        return true;
    }
    private function update($data){
        return true;

    }
    private function get(){
        $service=MstServiceType::where("status",1)->get()->toArray();
        return $service;
    }
    private function getLeasing(){
        $service=MstServiceType::where("status",1)->orWhere("status",-1)->get()->toArray();
        return $service;
    }


    private function getNameByID($data){
        //echo $data->service_type_id ; exit;
        $service =MstServiceType::select("name","provider_name")->where("id",(int)$data->service_type_id)->first();
        return $service;
    }
    public function getNameImageID($data){
        $service =MstServiceType::select("name","provider_name","image")->where("id",(int)$data->service_type_id)->first();
        return $service;
    }
    private function getNameByAllID($data){
        $service =MstServiceType::where("id",(int)$data->service_type_id)->get()->toArray();
        return $service;
    }
    public function accessCreate($data){
        return $this->create($data);
    }
    public function accessGet(){
        return $this->get();
    }
    public function accessUpdate($data){
        return $this->update($data);
    }
    public function accessGetNameByID($data){
        return $this->getNameByID($data);
    }
    public function accessGetNameByAllID($data){
        return $this->getNameByAllID($data);
    }
    public function accessGetLeasing(){
        return $this->getLeasing();
    }
}



