<?php

namespace App\Services;

use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use App\Model\Promocode\Promocode;
use App\Model\Promocode\PromocodeUsage;
use App\Model\Request\ServiceRequest;


class PromoCodeService
{
    //get completed ride count of a passenger
    private function getCompleteRideCount($data){
        return ServiceRequest::where('passenger_id',$data->user_id)->where('request_status','COMPLETED')->count();
    }

    //insert to use promocode
    private function insertToUsePromocode($data,$dt){
        $PromocodeUsage=new PromocodeUsage();
        $PromocodeUsage->user_id=$data->user_id;
        $PromocodeUsage->promocode_id=$dt['data'][0]['promocodes_id'];
        $PromocodeUsage->status="ADDED";
        $PromocodeUsage->save();
        return ["status"=>200, "promocode_usage_id"=>$PromocodeUsage->promocode_usages_id,"message"=>trans("api.SYSTEM_MESSAGE.Promo_Code_Added")];
    }

    // check promocde for the experi date
    private function CheckPromoCodeExperyDateBeforeAdd($data){
        $getPromo=Promocode::where("promo_code",$data->promo_code)->whereRaw("expiration >= STR_TO_DATE(?, '%Y-%m-%d %H:%i:%s')",[date("Y-m-d H:i:s")])->get()->toArray();
        if(count($getPromo)>0){
            return ["status"=>"true", "data"=>$getPromo];
        }
        return ["status"=>"false", "data"=>[]];
    }

    //update promocode code to use
    private function UpdatePromoCodeToUsed($data){
        $PromocodeUsage=PromocodeUsage::where("promocode_usages_id",$data->promocode_usages_id)->first();
        $PromocodeUsage->status="USED";
        return $PromocodeUsage->save();
    }
    
    //used promocode by rider
    private function UsePromocode($data){
        $dt=$this->CheckPromoCodeExperyDateBeforeAdd($data);
        $return=["status"=>400, "promocode_usage_id"=>0,"message"=>trans("api.SYSTEM_MESSAGE.Promo_Code_Not_Exit")];

        if($dt['status']=="true"){
            // check code is already in used!
            $checkCode=PromocodeUsage::where("promocode_id",$dt['data'][0]['promocodes_id'])->where("user_id",$data->user_id)->get()->toArray();
            if(count($checkCode)>0){
                $return=["status"=>422, "promocode_usage_id"=>0,"message"=>trans("api.SYSTEM_MESSAGE.Promo_Code_Not_Exit_Redeemed")];
            }
            else{
                //check firstTime and anyTime
                if($dt['data'][0]['promocode_type']=='anyTime'){
                    $completedRide = $this->getCompleteRideCount($data);
                    if($completedRide>(integer)$dt['data'][0]['rideCount']){
                        $return=$this->insertToUsePromocode($data,$dt);
                    } else {
                        $return=["status"=>422, "promocode_usage_id"=>0,"message"=>"This promocode is applicable only after ".(integer)$dt['data'][0]['rideCount']." complete rides."];
                    }

                } else {
                    $completedRide = $this->getCompleteRideCount($data);
                    if($completedRide==(integer)$dt['data'][0]['rideCount']){
                        $return=$this->insertToUsePromocode($data,$dt);
                    } else {
                        $return=["status"=>422, "promocode_usage_id"=>0,"message"=>"This promocode is applicable before taking any ride"];
                    }
                }

            }
        }
        return $return;
    }

    private function FindPromoCode($data){
        //find passenger id from request id
        $getPromo=DB::select('SELECT p.promo_code,p.discount,p.expiration,p.status,pu.promocode_usages_id FROM service_requests sr
        left join promocode_usages pu on pu.user_id=sr.passenger_id and pu.status="ADDED"
        left join promocodes p on DATE(p.expiration)>=STR_TO_DATE(?, "%Y-%m-%d")
        where p.promocodes_id=pu.promocode_id and sr.request_id=? limit 1',[date("Y-m-d H:i:s"), $data->request_id]);
        if(count($getPromo)>0){
            $return=["status"=>200, "getPromo"=>$getPromo,"message"=>"Promo code"];
        }
        else{
            $return=["status"=>404, "getPromo"=>$getPromo,"message"=>trans("api.SYSTEM_MESSAGE.Promo_Code_Not_Exit")];
        }
        return $return;
    }

    //get promocode list after used by rider
    private function GetPromocodeList($data){
        $getPromo=DB::select("select p.promo_code,p.discount,p.expiration,pu.status,pu.promocode_usages_id,p.currency
        from promocodes p
        left join promocode_usages pu on pu.promocode_id=p.promocodes_id and pu.status='ADDED' and pu.user_id=? where pu.user_id=?",[$data->user_id,$data->user_id]);
        $return=["status"=>200, "getPromo"=>$getPromo,"message"=>trans("api.SYSTEM_MESSAGE.List_Promo_Codes")];
        return $return;
    }

    public function accessGetPromocodeList($data){

        return  $this->GetPromocodeList($data);
    }

    public function accesFindPromoCode($data){
        return $this->FindPromoCode($data);
    }

    public function accessUsePromocode($data){
        return  $this->UsePromocode($data);
    }

    public function accesUpdatePromoCodeToUsed($data){
        return $this->UpdatePromoCodeToUsed($data);
    }

}
