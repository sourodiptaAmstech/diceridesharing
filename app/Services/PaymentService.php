<?php

namespace App\Services;


use App\Model\Setting\Setting;
use App\Model\Payment\UserCard;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\PayStackServices;
use App\Model\Request\ServiceRequest;
use App\Model\SplitFare\SplitFare;
use App\Model\SplitFare\SplitTransactionLog;


class PaymentService
{
    private $stripe;
    public function __construct(){
        $Setting=Setting::where("key","stripe_mode")->select("value")->first();
    }
    private function listCard($data){
        try{
            $cards=UserCard::select("user_cards_id","last_four","brand","is_default","status","email")->where('user_id',$data->user_id)->where('status','<>',"Delete")->orderBy('created_at','desc')->get()->toArray();
            if(!empty($cards))
            return ['message'=>trans("api.SYSTEM_MESSAGE.Card_List"),"data"=>$cards,"errors"=>array("exception"=>["card found"],"error"=>[]),"statusCode"=>200];
            else
            return ['message'=>trans("api.SYSTEM_MESSAGE.No_Card_Found"),"data"=>[],"errors"=>array("exception"=>["card resource not found"],"error"=>[]),"statusCode"=>204];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Fetch_Card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Fetch_Card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.No_Card_Found"),"data"=>(object)[],"errors"=>array("exception"=>["card resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }

    private function addCard($data){
        try{
            $exist=UserCard::where('user_id',$data->user_id)->where('status','<>',"Delete")->count();
            $create_card = new UserCard;
            $create_card->user_id = $data->user_id;
            $create_card->email = $data->email;
            $create_card->card_token = $data->card_token;
            $create_card->card_expiry = $data->card_expiry;
            $create_card->last_four = $data->last_four;
            $create_card->device_fingerprint = $data->device_fingerprint;
            $create_card->brand = $data->brand;
            $create_card->channel = $data->channel;
            $create_card->country = $data->country;
            $create_card->status = $data->status;
            //$create_card->first_six = $data->first_six;
            $create_card->tx_ref = $data->tx_ref;
            if($exist == 0){
               $create_card->is_default = 1;
            }else{
                $create_card->is_default = 0;
            }
            $create_card->save();
            return ['message'=>trans("api.SYSTEM_MESSAGE.Card_Added"),"data"=>$create_card,"errors"=>array("exception"=>["Resource Created"],"error"=>[]),"statusCode"=>201];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Add_Card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Add_Card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Add_Card"),"data"=>(object)[],"errors"=>array("exception"=>["stripe resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }
    private function setDefaultCard($data){
        try{

            $UserCardUnSetDefault=UserCard::where('user_id', $data->user_id)->update(array('is_default' => 0));
            $UserCardSetDefault=UserCard::where('user_cards_id', $data->user_cards_id)->update(array('is_default' => 1));

            return response(['message'=>"Default card updated","data"=>[],"errors"=>array("exception"=>["Card Updated"])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Update_Card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Update_Card"),"data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.No_Card_Found"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.No_Card_Found")],"error"=>$e),"statusCode"=>404];
        }
    }
    private function getDefaultCard($data){
        try{

            $UserCardUnSetDefault=UserCard::where('user_id', $data->user_id)->where('is_default',0)->get()->toArray();


            return $UserCardUnSetDefault;
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Update_Card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Update_Card"),"data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.No_Card_Found"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.No_Card_Found")],"error"=>$e),"statusCode"=>404];
        }
    }
    private function deleteCard($data){
        try{
            $UserCardUnSetDefault=UserCard::where('user_id', $data->user_id)->where("user_cards_id",$data->user_cards_id)->first();

            // $PayStackServices = new PayStackServices();
            // $verifyArray=$PayStackServices->deleteCards($UserCardUnSetDefault);
            
            UserCard::where('user_id', $data->user_id)->where("user_cards_id",$data->user_cards_id)->update(array('status' => "Delete"));
            return response(['message'=>trans("api.SYSTEM_MESSAGE.Card_deleted"),"data"=>[],"errors"=>array("exception"=>["Card Updated"])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.cannot_delete_card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.cannot_delete_card"),"data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.No_Card_Found"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.No_Card_Found")],"error"=>$e),"statusCode"=>404];
        }
    }

    private function splitCardPayment($PayStackServices,$UserCardUnSetDefault,$requestedRide,$amount){

        $paymentObject=[
            "card_token"=>$UserCardUnSetDefault->card_token,
            "amount"=>$amount,
            "request_no"=>$requestedRide->request_no,
            "email"=>$UserCardUnSetDefault->email,
            "currency"=>"NGN",
            "country"=>$UserCardUnSetDefault->country,
            "tx_ref"=>"DICE".mt_rand(1000000000000000,9999999999999999)
        ];
        // $UserCardUnSetDefault->tx_ref

        $makePayment=$PayStackServices->makePayment((object)$paymentObject);

        $data=[];$statuCode=200;
        if($makePayment['statusCode']==200){
            if($makePayment['data']['status']=="success"){
                // verify transcation
                $verifyPayment= $PayStackServices->verifyTranscationByRefrence((object)['reference'=>$makePayment['data']['data']['flw_ref'],'normalize'=>1,'SECKEY'=>env("SECKEY")]);

                $gateWayFee=$verifyPayment['data']['data']['appfee'];
               // $refernceTrasaction=$makePayment['data']['data']['reference'];
                $data=[
                    'reference'=>$makePayment['data']['data']['flw_ref'],
                    'fees'=>($gateWayFee/100),
                    'status'=>$verifyPayment['data']['data']['status']
                ];
            }
            else{
                // verify transaction
                $verifyPayment= $PayStackServices->verifyTranscationByRefrence((object)['reference'=>$makePayment['data']['data']['flw_ref'],'normalize'=>1,'SECKEY'=>env("SECKEY")]);
                $statuCode=400;
            }
        }
        else{
            $statuCode=400;
        }

        return ['message'=>"Payment","data"=>$data,"errors"=>array("exception"=>["Payment"]),"statusCode"=>$statuCode];
    }


    private function makeCardPayment($data,$requestedRide){
        try{

            $UserCardUnSetDefault=UserCard::where('user_id', $requestedRide->user_id)->where("user_cards_id",$requestedRide->user_cards_id)->first();
            $PayStackServices = new PayStackServices();

            $amount_bal = (float)$data->cost-(float)$data->promo_code_value;

            //split payment part
            $SplitFares = SplitFare::where('request_id',$requestedRide->request_id)->where('is_splitted',1)->get();

            if (count($SplitFares)>0) {
                //SPLIT PAYMENT SECTION
                $split_amount = round($amount_bal/(count($SplitFares)+1),2);

                $split_fail_amount = 0;

                foreach ($SplitFares as $key => $SplitFare) {
                    $splitedUserDefaultCard=UserCard::where('user_id', $SplitFare->user_id)->where("is_default",1)->first();

                    $splitcardPayment = $this->splitCardPayment($PayStackServices,$splitedUserDefaultCard,$requestedRide,$split_amount);

                    if ($splitcardPayment['statusCode']==200) {
                        $SplitTransactionLog = new SplitTransactionLog;
                        $SplitTransactionLog->request_id = $requestedRide->request_id;
                        $SplitTransactionLog->split_user_id = $SplitFare->user_id;
                        $SplitTransactionLog->cost = $split_amount;
                        $SplitTransactionLog->currency = '₦';
                        $SplitTransactionLog->payment_gateway_charge = $splitcardPayment['data']['fees'];
                        $SplitTransactionLog->payment_gateway_transaction_id = $splitcardPayment['data']['reference'];
                        $SplitTransactionLog->status = 'COMPLETED';
                        $SplitTransactionLog->save();
                    }
                    if ($splitcardPayment['statusCode']==400) {
                        $SplitTransactionLog = new SplitTransactionLog;
                        $SplitTransactionLog->request_id = $requestedRide->request_id;
                        $SplitTransactionLog->split_user_id = $SplitFare->user_id;
                        $SplitTransactionLog->cost = $split_amount;
                        $SplitTransactionLog->currency = '₦';
                        $SplitTransactionLog->status = 'PENDING';
                        $SplitTransactionLog->save();

                        $split_fail_amount = $split_fail_amount + $split_amount;
                    }
                }
                
                $total_split_amount = $split_amount + $split_fail_amount;
                //ADJAST REFERRAL BALANCE for main user..
                if ((float)$total_split_amount>(float)$data->referral_balance) {
                    $amount = ((float)$total_split_amount-(float)$data->referral_balance);

                    $ServiceRequest = ServiceRequest::where('request_id',$requestedRide->request_id)->first();
                    $ServiceRequest->referral_value = $data->referral_balance;
                    $ServiceRequest->save();

                } else {

                    $ServiceRequest = ServiceRequest::where('request_id',$requestedRide->request_id)->first();
                    $ServiceRequest->referral_value = $total_split_amount;
                    $ServiceRequest->save();

                    $amount = 0.00;
                }
                //.........................

                //split payment function for main user...
                return $this->splitCardPayment($PayStackServices,$UserCardUnSetDefault,$requestedRide,$amount);
                //.......................................
                  
            } else {
                //PAYMENT SECTION WITHOUT SPLIT
                //ADJAST REFERRAL BALANCE..
                if ((float)$amount_bal>(float)$data->referral_balance) {
                    $amount = ((float)$amount_bal-(float)$data->referral_balance);

                    $ServiceRequest = ServiceRequest::where('request_id',$requestedRide->request_id)->first();
                    $ServiceRequest->referral_value = $data->referral_balance;
                    $ServiceRequest->save();

                } else {

                    $ServiceRequest = ServiceRequest::where('request_id',$requestedRide->request_id)->first();
                    $ServiceRequest->referral_value = $amount_bal;
                    $ServiceRequest->save();

                    $amount = 0.00;
                }
                //.........................

                //payment function.........
                return $this->splitCardPayment($PayStackServices,$UserCardUnSetDefault,$requestedRide,$amount);
                //.........................
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.cannot_make_payment_from_card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.cannot_make_payment_from_card"),"data"=>(object)[],"errors"=>array("exception"=>["Bad request"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.cannot_make_payment_from_card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.No_Card_Found")],"error"=>$e),"statusCode"=>404];
        }
    }

    private function getCardDetailsByCardId($data){
        try{
            $cards=UserCard::select("user_cards_id","last_four","brand","is_default","status","email")->where('user_cards_id',$data->user_cards_id)->first();
            if(!empty($cards))
            return ['message'=>trans("api.SYSTEM_MESSAGE.Card_List"),"data"=>$cards,"errors"=>array("exception"=>["card found"],"error"=>[]),"statusCode"=>200];
            else
            return ['message'=>trans("api.SYSTEM_MESSAGE.No_Card_Found"),"data"=>[],"errors"=>array("exception"=>["card resource not found"],"error"=>[]),"statusCode"=>204];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Fetch_Card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.Cannot_Fetch_Card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.No_Card_Found"),"data"=>(object)[],"errors"=>array("exception"=>["card resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }

    public function accessListCard($data){
        return $this->listCard($data);
    }
    public function accessSetDefaultCard($data){
     return $this->setDefaultCard($data);
    }
    public function accessAddCards($data){
        return $this->addCard($data);
    }
    public function accessDeleteCard($data){
        return $this->deleteCard($data);
    }
    public function accessGetDefaultCard($data){
        return $this->getDefaultCard($data);
    }

    public function accesssMakeCardPayment($data,$requestedRide){
        try{
            return $this->makeCardPayment($data,$requestedRide);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.cannot_make_payment_from_card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.cannot_make_payment_from_card"),"data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.No_Card_Found"),"data"=>(object)[],"errors"=>array("exception"=>["No card found."],"error"=>$e),"statusCode"=>404];
        }

    }

    public function accessGetCardDetailsByCardId($data){
        return $this->getCardDetailsByCardId($data);
    }
}



