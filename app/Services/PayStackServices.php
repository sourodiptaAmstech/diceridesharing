<?php

namespace App\Services;

use App\Model\Setting\Setting;
use App\Model\Payment\UserCard;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PayStackServices
{
    public function __construct(){
        // $Setting=Setting::where("key","payment_gateway_mode")->select("value")->first();
    }
    private function curlMethods($data){
        try{
            $curl = curl_init();

                if($data->methord=="POST"||$data->methord=="PUT"){
                      curl_setopt_array($curl,array(
                    CURLOPT_URL => $data->url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $data->methord,
                    CURLOPT_POSTFIELDS=>$data->CURLOPT_POSTFIELDS,
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer FLWSECK_TEST-f70ca951a3f429b6199800d2c65dae38-X",
                        "Content-Type: application/json"
                    )));
                } else {
                    curl_setopt_array($curl,array(
                        CURLOPT_URL => $data->url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => $data->methord,
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: Bearer FLWSECK_TEST-f70ca951a3f429b6199800d2c65dae38-X"
                        )));
                }

                $response = curl_exec($curl);
               // print_r($response); exit;
                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                
                curl_close($curl);
                $response=json_decode($response,true);
               
                return ['message'=>"","data"=>$response,"errors"=>"",'statusCode'=>$httpcode];
        }
        catch(\Illuminate\Database\QueryException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
    }

    // public function verifyTranscationByRefrence($data){
    //     return $this->curlMethods((object)[
    //         'url'=>'transactions/'.$data->reference.'/verify',
    //         'methord'=>'GET'
    //         ]);
    // }

    public function verifyTranscationByRefrence($data){
        $CURLOPT_POSTFIELDS=array("flw_ref"=>$data->reference,"normalize"=>$data->normalize, "SECKEY"=>$data->SECKEY);
        return $this->curlMethods((object)[
            'url'=>'https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/verify',
            'methord'=>'POST',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
    }

    public function deleteCards($data){
        $CURLOPT_POSTFIELDS=array("authorization_code"=>$data->authorization_code);
        return $this->curlMethods((object)[
            'url'=>'customer/deactivate_authorization',
            'methord'=>'POST',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
    }

    public function makePayment($data){
        $CURLOPT_POSTFIELDS=array("token"=>$data->card_token,"email"=>$data->email, "amount"=>$data->amount,"currency"=>$data->currency,"country"=>$data->country,"tx_ref"=>$data->tx_ref);
        return $this->curlMethods((object)[
            'url'=>'https://api.flutterwave.com/v3/tokenized-charges',
            'methord'=>'POST',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
    }

    //===================not used=================================
    //Driver Create payment wateway Sub-Account
    public function makeSubaccount($data){
        $CURLOPT_POSTFIELDS=array(
            "account_bank" => $data->account_bank,
            "account_number" => $data->account_number, 
            "business_name" => $data->business_name,
            "business_email" => $data->business_email,
            "country" => $data->country,
            "business_mobile" => $data->business_mobile,
            "business_contact" => $data->business_contact,
            "business_contact_mobile" => $data->business_contact_mobile,
            "split_type" => $data->split_type,
            "split_value" => $data->split_value
        );
        $returnResponse = $this->curlMethods((object)[
            'url'=>'https://api.flutterwave.com/v3/subaccounts',
            'methord'=>'POST',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
        return ['status'=>$returnResponse['data']['status'],'message'=>$returnResponse['data']['message'],"data"=>$returnResponse['data']['data'],"errors"=>"",'statusCode'=>$returnResponse['statusCode']];
    }

    //Get Driver's payment gateway Sub-Account
    public function accessGetSubaccount($data)
    {
        $returnResponse = $this->curlMethods((object)[
            'url'=>'https://api.flutterwave.com/v3/subaccounts/'.$data->id,
            'methord'=>'GET'
        ]);

        return ['status'=>$returnResponse['data']['status'],'message'=>$returnResponse['data']['message'],"data"=>$returnResponse['data']['data'],"errors"=>"",'statusCode'=>$returnResponse['statusCode']];
    }

    //Driver update payment wateway Sub-Account
    public function updateSubaccount($data){
        $CURLOPT_POSTFIELDS=array(
            "account_bank" => $data->account_bank,
            "account_number" => $data->account_number, 
            "business_name" => $data->business_name,
            "business_email" => $data->business_email,
            "business_mobile" => $data->business_mobile,
            "business_contact" => $data->business_contact,
            "business_contact_mobile" => $data->business_contact_mobile
        );
        $returnResponse = $this->curlMethods((object)[
            'url'=>'https://api.flutterwave.com/v3/subaccounts/'.$data->id,
            'methord'=>'PUT',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
        return ['status'=>$returnResponse['data']['status'],'message'=>$returnResponse['data']['message'],"data"=>$returnResponse['data']['data'],"errors"=>"",'statusCode'=>$returnResponse['statusCode']];
    }
    //===============================================================

    //Driver Create payment wateway Beneficiary
    public function makeBeneficiary($data){
        $CURLOPT_POSTFIELDS=array(
            "account_number" => $data->account_number, 
            "account_bank" => $data->account_bank,
            "beneficiary_name" => $data->beneficiary_name
        );
        $returnResponse = $this->curlMethods((object)[
            'url'=>'https://api.flutterwave.com/v3/beneficiaries',
            'methord'=>'POST',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
        return ['status'=>$returnResponse['data']['status'],'message'=>$returnResponse['data']['message'],"data"=>$returnResponse['data']['data'],"errors"=>"",'statusCode'=>$returnResponse['statusCode']];
    }

    //Get Driver's payment gateway Beneficiary
    public function accessGetBeneficiary($data)
    {
        $returnResponse = $this->curlMethods((object)[
            'url'=>'https://api.flutterwave.com/v3/beneficiaries/'.$data->id,
            'methord'=>'GET'
        ]);

        return ['status'=>$returnResponse['data']['status'],'message'=>$returnResponse['data']['message'],"data"=>$returnResponse['data']['data'],"errors"=>"",'statusCode'=>$returnResponse['statusCode']];
    }

    //Transfer Payout to Beneficiary
    public function transferBeneficiary($data){
        $CURLOPT_POSTFIELDS=array(
            "account_number" => $data->account_number, 
            "account_bank" => $data->account_bank,
            "amount" => $data->amount,
            "currency" => $data->currency,
            "callback_url" => $data->callback_url,
            "debit_currency" => $data->debit_currency
        );
        $returnResponse = $this->curlMethods((object)[
            'url'=>'https://api.flutterwave.com/v3/transfers',
            'methord'=>'POST',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
        return ['status'=>$returnResponse['data']['status'],'message'=>$returnResponse['data']['message'],"data"=>$returnResponse['data']['data'],"errors"=>"",'statusCode'=>$returnResponse['statusCode']];
    }

    //Get Flutterwave admin balance
    public function accessGetBalance()
    {
        $returnResponse = $this->curlMethods((object)[
            'url'=>'https://api.flutterwave.com/v3/balances/NGN',
            'methord'=>'GET'
        ]);

        return ['status'=>$returnResponse['data']['status'],'message'=>$returnResponse['data']['message'],"data"=>$returnResponse['data']['data'],"errors"=>"",'statusCode'=>$returnResponse['statusCode']];
    }

    //Get Flutterwave bank details
    public function accessGetBankDetails()
    {
        $returnResponse = $this->curlMethods((object)[
            'url'=>'https://api.flutterwave.com/v3/banks/NG',
            'methord'=>'GET'
        ]);

        return ['status'=>$returnResponse['data']['status'],'message'=>$returnResponse['data']['message'],"data"=>$returnResponse['data']['data'],"errors"=>"",'statusCode'=>$returnResponse['statusCode']];
    }
}



