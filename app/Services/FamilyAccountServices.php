<?php

namespace App\Services;

use App\Model\FamilyAccount\FamilyAccount;

class FamilyAccountServices
{
    private function createFamilyAccount($data){
        $FamilyAccount=new FamilyAccount();
        $FamilyAccount->parent_account_id=$data->parent_account_id;
        $FamilyAccount->child_account_id=$data->child_account_id;
        $FamilyAccount->relationship=$data->relationship;
        $FamilyAccount->save();
        return $FamilyAccount;
    }
    private function getFamilyAccount($data){
        $FamilyAccount=FamilyAccount::where("parent_account_id",$data->user_id)->get()->toArray();
        return $FamilyAccount;
    }
    private function deleteFamilyAccount($data){
        $FamilyAccount=FamilyAccount::destroy($data->family_accounts_id);
        return $FamilyAccount;
    }
    public function accessFamilyAccount($data){
        return $this->createFamilyAccount($data);
    }
    public function accessgetFamilyAccount($data){
        return $this->getFamilyAccount($data);
    }
    public function accessDelete($data){
        return $this->deleteFamilyAccount($data);
    }
}


