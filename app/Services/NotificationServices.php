<?php

namespace App\Services;

use App\Notifications\PushNotification;
use Illuminate\Support\Facades\DB;
use App\Services\TwilioSMS;
use Illuminate\Support\Facades\Log;
use App\Model\Notification\Notification;


class NotificationServices
{

    public function insertPushNotification($data){
        $Notification = new Notification;
        $Notification->message_type = $data->message_type;
        $Notification->message = $data->message;
        $Notification->recepient_user_id = $data->recepient_user_id;
        $Notification->user_type = $data->user_type;
        $Notification->save();
        return true;
    }

    private function getSingleUser($data){
        $db=DB::select('SELECT * FROM user_devices where user_devices.token NOT IN("NOT-FOUND","","device_token") and
        user_devices.device_type IN("android","IOS","ios","web") and user_devices.user_id='.$data->user_id.'  ORDER BY user_devices.device_id ASC' );
        $FCMTokens=[];
        $PushNotification="";
        $pushnotificationsData = [];
        if(count($db)>0){
            foreach($db as $key=>$val){
                //print_r($val); exit;
                if($val->device_type=="android"){
                    $FCMTokens['Android'][]=$val->token;
                }
                if($val->device_type=="ios"){
                    $FCMTokens['IOS'][]=$val->token;
                }
                if($val->device_type=="web"){
                    $FCMTokens['WEB'][]=$val->token;
                }
            }
        }
        return $FCMTokens;
    }

    private function getPassengerPhoneNo($data){
        $phNo=0;$isdCode=0;
        $db=DB::select('SELECT * FROM passengers_profile where mobile_no<>""
        and mobile_no<> 0 and LENGTH(mobile_no)>= 5 and user_id='.$data->user_id);
        if(count($db)>0){
            foreach($db as $key=>$val){
                $phNo=$val->mobile_no;
                $isdCode=$val->isd_code;
            }
        }
        return ["phNo"=>$phNo,"isdCode"=>$isdCode];
    }

    private function getAllUser($data){
        $db=DB::select('SELECT * FROM user_devices where user_devices.token NOT IN("NOT-FOUND","","device_token") and
        user_devices.device_type IN("android","IOS","ios")  ORDER BY user_devices.device_id ASC' );
        $FCMTokens=[];
        $PushNotification="";
        $pushnotificationsData = [];
        if(count($db)>0){
            foreach($db as $key=>$val){
                if($val->device_type=="android"){
                    $FCMTokens['Android'][]=$val->device_token;
                }
                if($val->device_type=="ios"){
                    $FCMTokens['IOS'][]=$val->device_token;
                }
            }
        }
        return $FCMTokens;;

    }

    private function setDataForSingleUser($data){
        $FCMTokens=$this->getSingleUser($data);
        $pushnotificationsData['to'] = $FCMTokens;
        $pushnotificationsData['notification'] = array(
            'title' => $data->title,
            'text' => $data->text,
            'click_action' => '',
            'body'=>$data->body,
        );
        $pushnotificationsData['data'] = [
            "type"=>$data->type
        ];
       // $PushClass = new PushNotification();
      // print_r($pushnotificationsData); exit;
        $PushNotification=PushNotification::PushNotifications($pushnotificationsData);
        return true;
    }

    private function setDataForSingleUserForSplit($data){
        $FCMTokens=$this->getSingleUser($data);
        $pushnotificationsData['to'] = $FCMTokens;
        $pushnotificationsData['notification'] = array(
            'title' => $data->title,
            'text' => $data->text,
            'click_action' => '',
            'body'=>$data->body,
        );
        $pushnotificationsData['data'] = [
            "type"=>$data->type,
            "data"=>$data->data
        ];
        $PushNotification=PushNotification::PushNotifications($pushnotificationsData);
        return true;
    }

    private function sendSMSToPassenger($data){
        $phArray=$this->getPassengerPhoneNo($data);
        $TwilioSMS=new TwilioSMS();
        $TwilioSMS->accessSendSMS((object)["body"=>$data->body,"isdCode"=>$phArray['isdCode'],"mobile_no"=>$phArray['phNo']]);
        return true;
    }

    public function sendPushNotification($data){
        if($data->setTo=="SINGLE"){
            $this->setDataForSingleUser($data);
        }
        return true;
    }

    public function sendPushNotificationForSplit($data){
        if($data->setTo=="SINGLE"){
            $this->setDataForSingleUserForSplit($data);
        }
        return true;
    }


    public function accessSendSMSToPassenger($data){
        $this->sendSMSToPassenger($data);
        return true;
    }
}


