<?php

namespace App\Services;

use App\Model\Setting\Setting;

class SettingServices
{
    public function getValueByKey($key){
        $value=Setting::where("key",$key)->first();
        return $value->value;
    }
    public function getSurgePriceByKey($key){
        $value=Setting::where("key",$key)->where("status","Y")->first();
        if(isset($value)){
            return $value->value;
        }else{
            return 0.00;
        }
    }
    public function getStatusByKey($key){
        $value=Setting::where("key",$key)->first();
        return $value->status;
    }
    public function GetValuePassenger(){
        $contents = Setting::where('key','condition_privacy')
                    ->orWhere('key','page_privacy')
                    ->get();
        return $contents;

    }
    public function GetValueDriver(){
        $contents = Setting::where('key','condition_privacy_driver')
                    ->orWhere('key','page_privacy_driver')
                    ->get();
        return $contents;
    }
}



