<?php

namespace App\Services;

use App\Model\Profiles\PassengersProfile;
use App\Services\TwilioSMS;
use Storage;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PassengersProfileService
{
    private $Profile;
    private function checkNull($field){
        if($field===null)
        return "";
        else
        return $field;
    }
    private function createProfile($data){
        $Profile = new PassengersProfile();
        $Profile->user_id=$data->user_id;
        $Profile->email_id=$data->email_id;
        $Profile->first_name=$data->first_name;
        $Profile->last_name=$data->last_name;
        $Profile->isMobileverified=$data->isActive;
        $Profile->picture=$data->picture;

        $Profile->isd_code=$data->isdCode;
        $Profile->mobile_no=$data->mobile_no;
        $Profile->account_type=$data->account_type;
      /*  $Profile->dob=$data->dob;
        $Profile->gender=$data->gender;

        $Profile->address_line1=$data->address_line1;
        $Profile->address_line2=$data->address_line2;
        $Profile->address_line3=$data->address_line3;
        $Profile->state=$data->state;
        $Profile->city=$data->city;
        $Profile->pincode=$data->pincode;
        $Profile->country=$data->country;
        $Profile->mobile_verification_code=$data->mobile_verification_code;
        $Profile->isMobileverified=$data->isMobileverified;
        $Profile->verification_code_generated_on=$data->verification_code_generated_on;
        */
        $Profile->save();
        return $Profile;
    }
    private function updateProfile($data){
        try{
            $Profile=PassengersProfile::where("user_id",$data->user_id)->firstOrFail();
            $Profile->email_id=$data->email_id;
            $Profile->first_name=$data->first_name;
            $Profile->last_name=$data->last_name;
            $Profile->gender=$data->gender;
            $Profile->dob=$data->dob;
            $Profile->save();
            return ['message'=>trans("api.SYSTEM_MESSAGE.profile_updated"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>200];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.profile_cannot_updated"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    public function updateParentProfile($data){
        try{
            $Profile=PassengersProfile::where("user_id",$data->user_id)->firstOrFail();
            $Profile->account_type="PARENT";
            $Profile->save();
            return ['message'=>trans("api.SYSTEM_MESSAGE.profile_updated"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>200];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.profile_cannot_updated"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function updateLang($data){
        try{
            $Profile=PassengersProfile::where("user_id",$data->user_id)->firstOrFail();
            $Profile->lang=$data->lang;
            $Profile->save();
            return ['message'=>"Your application language is updated to your preferred language","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>200];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Your application language cannot be updated to your preferred language","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function updateOnlyMobile($data){
        try{
            // generate otp
            $dateOfGen=date('Y-m-d H:i:s');
            $digits_otp = 4;
            $otp=1111; //rand(pow(10, $digits_otp-1), pow(10, $digits_otp)-1);
            $Profile=PassengersProfile::where("user_id",$data->user_id)->update(['isd_code'=>$data->isdCode,'mobile_no'=>$data->mobile_no,'mobile_verification_code'=>$otp,'verification_code_generated_on'=>$dateOfGen]);
            $data->body="Your one time password for your mobile number verification is ".$otp;
            $data->otp=$otp;
            $TwilioSMS=new TwilioSMS();
            $TwilioSMSReturn=$TwilioSMS->accessSendSMS($data);
            return ['message'=>$TwilioSMSReturn['message'],"data"=>(object)["otp"=>$otp],"errors"=>$TwilioSMSReturn['errors'],"statusCode"=>$TwilioSMSReturn['statusCode']];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.System_Error"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function verifyMobileNo($data){
        try{
            $Profile=PassengersProfile::where("user_id",$data->user_id)->update(['isMobileverified'=>$data->isValid,'mobile_verification_code'=>'']);
            if($data->isValid==1)
            return ['message'=>"Thank you for verifying your mobile number","data"=>(object)[],"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];
            else
            return ['message'=>"Sorry cannot verify your mobile number","data"=>(object)[],"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];


        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.System_Error"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }


    }
    private function getProfile($data){
        try{
            $Profile=PassengersProfile::where("user_id",$data->user_id)->first();

            return ['message'=>"Profile Data","data"=>$Profile,"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.System_Error"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function profileImageUpdate($data){
        try{
            $Profile = PassengersProfile::where("user_id",$data->user_id)->firstOrFail();
            if ($data->picture != "") {
                $Storage=Storage::delete($Profile->picture);
                $Profile->picture = $data->picture->store('public/users/'.$data->user_id.'/profile');
                $Profile->picture=str_replace("public", "storage", $Profile->picture);
                $Profile->picture=env("baseURL").$Profile->picture;
            }
            $Profile->save();

            return ['message'=>"Thank you for uploading your profile image!","data"=>(object)["picture"=>$Profile->picture],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>201];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.System_Error"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function sendOTPFirst($data){
        try{
            // generate otp
            $dateOfGen=date('Y-m-d H:i:s');
            $digits_otp = 4;
            $otp=1111; //rand(pow(10, $digits_otp-1), pow(10, $digits_otp)-1);
            $otp=(int)$otp;
            //$Profile=DriverProfiles::where("user_id",$data->user_id)->update(['isd_code'=>$data->isdCode,'mobile_no'=>$data->mobile_no,'mobile_verification_code'=>$otp,'verification_code_generated_on'=>$dateOfGen]);
            $data->body="Your one-time verification code for DICE new account is ".$otp;
            $data->otp=$otp;
            $TwilioSMS=new TwilioSMS();
            $TwilioSMSReturn=$TwilioSMS->accessSendSMS($data);
            return ['message'=>$TwilioSMSReturn['message'],"data"=>(object)["otp"=>$otp],"errors"=>$TwilioSMSReturn['errors'],"statusCode"=>$TwilioSMSReturn['statusCode']];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.System_Error"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function getProfileLoginWithPhoneNo($data){
        try{
            $PassengersProfile=PassengersProfile::where("mobile_no",$data->username)->where("isd_code",$data->isd_code)->firstOrFail();
            return ['message'=>"","data"=>(object)["user_id"=>$PassengersProfile->user_id],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }

    }
    private function updateOnlyEmail($data){
        try{
            // generate otp
            $Profile=PassengersProfile::where("user_id",$data->user_id)->update(['email_id'=>$data->email_id]);
            return ['message'=>"Email Id is Updated","data"=>(object)["otp"=>$data->email_id],"errors"=>[],"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.System_Error"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }






    public function accessCreateProfile($data){
        return $this->createProfile($data);
    }
    public function accessUpdateProfile($data){
        return $this->updateProfile($data);
    }
    public function accessUpdateOnlyMobile($data){
        return $this->updateOnlyMobile($data);
    }
    public function accessVerifyMobileNo($data){
        return $this->verifyMobileNo($data);
    }
    public function accessGetProfile($data){
        return $this->getProfile($data);
    }

    public function setProfileData($data,$login_by){
        //print_r($data); exit;
        if($login_by=="manual"){
            if($this->checkNull($data->picture)!==""){
                $data->picture=$data->picture;
            }
        }
        if($this->checkNull($data->lang)==""){
            $data->lang="en";
        }
        return [
            "first_name"=>$data->first_name,
            "last_name"=>$data->last_name,
            "email_id"=>$this->checkNull($data->email_id) ,
            "mobile"=>$this->checkNull($data->mobile_no),
            "isdCode"=>$this->checkNull($data->isd_code),
            "picture"=>$this->checkNull($data->picture),
            "isMobileVerified"=>$data->isMobileverified,
            "gender"=>$this->checkNull($data->gender),
            "dob"=>$this->checkNull($data->dob),
            "login_by"=>$login_by,
            "lang"=>$this->checkNull($data->lang),
            'account_type'=>$this->checkNull($data->account_type),
           // "scope"=>$user_scope
        ];
    }
    public function accessProfileImageUpdate($data){
        return $this->profileImageUpdate($data);
    }


    // modification due to client alteration of the flow
    public function accessSendOTPFirst($data){
        return $this->sendOTPFirst($data);
    }
    public function accessUpdateOnlyEmail($data){
        return $this->updateOnlyEmail($data);
    }

    public function accessUpdateLang($data){
        return $this->updateLang($data);
    }
    public function accessGetLoginWithPhoneNo($data){
        return $this->getProfileLoginWithPhoneNo($data);
    }


}
