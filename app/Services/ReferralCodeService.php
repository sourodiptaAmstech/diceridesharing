<?php

namespace App\Services;

use App\Model\Referral\ReferralCode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Model\Referral\ReferralCodeUse;
use App\Model\Request\ServiceRequest;
use App\Model\Referral\PassengerReferralBalance;
use App\Model\Referral\PassengerReferralUse;
use App\Model\Referral\DriverReferralBalance;
use App\Model\Referral\DriverReferralUse;


class ReferralCodeService
{
    //insert PassengerReferralUse when referral balance added to sender account after passenger completing his first ride 
    public function updatePassengerReferralUse($sender_id,$used_by_id){
        $PassengerReferralUse = new PassengerReferralUse;
        $PassengerReferralUse->sender_id = $sender_id;
        $PassengerReferralUse->used_by_id = $used_by_id;
        $PassengerReferralUse->flag = 1;
        $PassengerReferralUse->save();

        return true;
    }

    //insert DriverReferralUse when referral balance added to sender account after driver completing his first ride 
    public function updateDriverReferralUse($sender_id,$used_by_id){
        $DriverReferralUse = new DriverReferralUse;
        $DriverReferralUse->sender_id = $sender_id;
        $DriverReferralUse->used_by_id = $used_by_id;
        $DriverReferralUse->flag = 1;
        $DriverReferralUse->save();

        return true;
    }

    //get function to check if sender get ferral balance
    public function getPassengerReferralUse($used_by_id){
        $PassengerReferralUse = PassengerReferralUse::where('used_by_id',$used_by_id)->where('flag',1)->first();

        return $PassengerReferralUse;
    }

    //get function to check if sender get ferral balance
    public function getDriverReferralUse($used_by_id){
        $DriverReferralUse = DriverReferralUse::where('used_by_id',$used_by_id)->where('flag',1)->first();

        return $DriverReferralUse;
    }

    //passenger referral balance get
    public function getPassengerReferralBalance($passenger_id)
    {
        $PassengerReferralBalance = PassengerReferralBalance::where('user_id',$passenger_id)->first();
        return $PassengerReferralBalance;
    }

    //driver referral balance get
    public function getDriverReferralBalance($driver_id)
    {
        $DriverReferralBalance = DriverReferralBalance::where('user_id',$driver_id)->first();
        return $DriverReferralBalance;
    }

    //gererate 6 digits referral code
    private function generateReferralCode($length) {
        $random_string = '';
        for($i = 0; $i < $length; $i++) {
            $number = random_int(0, 36);
            $character = base_convert($number, 10, 36);
            $random_string .= $character;
        }
        return strtoupper($random_string);
    }

    //insert referral code after passenger & driver registration
    private function insertReferralCode($data)
    {
        $referralCode = new ReferralCode;
        $referralCode->user_id = $data->user_id;
        $referralCode->referral_code = $this->generateReferralCode(6);
        $referralCode->referral_amount_id = $data->referral_amount_id;
        $referralCode->save();
        return true;
    }

    //get referral code for passenger
    private function getReferralCodePassenger()
    {
        $user_id = Auth::user()->id;
        
        $referralCode = DB::select('SELECT rc.`user_id`, rc.`referral_code`, ra.referral_amount_sent_by, ra.referral_amount_used_by FROM `referral_codes` AS rc INNER JOIN referral_amounts AS ra ON rc.referral_amount_id=ra.referral_amount_id WHERE `user_id`=?',[$user_id]);
        if(count($referralCode)>0){
            $referralCode[0]->currency='₦';
            $PassengerReferralBalance = PassengerReferralBalance::where('user_id',$user_id)->first();
            if(isset($PassengerReferralBalance)){
                $referralCode[0]->referral_balance = $PassengerReferralBalance->referral_balance;
            }else{
                $referralCode[0]->referral_balance = "0.00";
            }
            $referralCode = $referralCode[0];
        }
        return $referralCode;

    }

    //get referral code for driver
    private function getReferralCodeDriver()
    {
        $user_id = Auth::user()->id;
        $referralCode = DB::select('SELECT rc.`user_id`, rc.`referral_code`, ra.referral_amount_sent_by, ra.referral_amount_used_by FROM `referral_codes` AS rc INNER JOIN referral_amounts AS ra ON rc.referral_amount_id=ra.referral_amount_id WHERE `user_id`=?',[$user_id]);
        if(count($referralCode)>0){
            $referralCode[0]->currency='₦';
            $DriverReferralBalance = $this->getDriverReferralBalance($user_id);
            if(isset($DriverReferralBalance)){
                $referralCode[0]->referral_balance = $DriverReferralBalance->referral_balance;
            }else{
                $referralCode[0]->referral_balance = "0.00";
            }
            $referralCode = $referralCode[0];
        }
        return $referralCode;

    }

    //insert and update referral balance for passenger
    public function updatePassengerReferralBalance($data){
        $PassengerReferralBalance = $this->getPassengerReferralBalance($data->user_id);
        if(isset($PassengerReferralBalance)){
            $PassengerReferralBalance->referral_balance = (float)$PassengerReferralBalance->referral_balance + $data->referral_balance;
            $PassengerReferralBalance->save();
        } else {
            $PassengerReferralBalance = new PassengerReferralBalance;
            $PassengerReferralBalance->user_id = $data->user_id;
            $PassengerReferralBalance->referral_balance = $data->referral_balance;
            $PassengerReferralBalance->save();
        }
        return true;
    }

    //insert and update referral balance for driver
    public function updateDriverReferralBalance($data){
        $DriverReferralBalance = $this->getDriverReferralBalance($data->user_id);
        if(isset($DriverReferralBalance)){
            $DriverReferralBalance->referral_balance = (float)$DriverReferralBalance->referral_balance + $data->referral_balance;
            $DriverReferralBalance->save();
        } else {
            $DriverReferralBalance = new DriverReferralBalance;
            $DriverReferralBalance->user_id = $data->user_id;
            $DriverReferralBalance->referral_balance = $data->referral_balance;
            $DriverReferralBalance->save();
        }
        return true;
    }


    public function ReferralCodeUse($data){
        $ReferralCodeUse = ReferralCodeUse::where('used_by_id',$data->user_id)->get();
        return $ReferralCodeUse;
    }

    public function getReferralCodeToSendBY($data){
        $ReferralCode = ReferralCode::join('referral_amounts','referral_codes.referral_amount_id', '=', 'referral_amounts.referral_amount_id')->select('referral_codes.referral_code_id','referral_codes.user_id','referral_codes.referral_code','referral_amounts.referral_amount_sent_by','referral_amounts.user_type')->where('referral_codes.referral_code_id',$data->referral_code_id)->first();
        return $ReferralCode;
    }


    //apply referral code function by driver or passenger
    private function useReferralCode($data)
    {
        try {
            if($data->user_type=='passenger'){
                $passengerID = $data->user_id;
                $ServiceRequest = ServiceRequest::where('passenger_id',$passengerID)->where('request_status','COMPLETED')->get();
            }
            if($data->user_type=='driver'){
                $driverID = $data->user_id;
                $ServiceRequest = ServiceRequest::where('driver_id',$driverID)->where('request_status','COMPLETED')->get();
            }

            $ReferralCode = ReferralCode::join('referral_amounts','referral_codes.referral_amount_id', '=', 'referral_amounts.referral_amount_id')->where('referral_codes.referral_code',$data->referral_code)->where('referral_amounts.user_type',$data->user_type)->get();
            if(count($ReferralCode)>0){
                if($ReferralCode[0]->user_id == $data->user_id){
                    
                    return ['message'=>'Invalid referral code.','data'=>(object)[],'errors'=>array('exception'=>['Bad Request.'],'error'=>[]),'statusCode'=>400];
                } else {
                    
                    $ReferralCodeUse = $this->ReferralCodeUse($data);
                    if(count($ReferralCodeUse)<=0){

                        if(count($ServiceRequest)<=0){

                            $ReferralCodeUse = new ReferralCodeUse;
                            $ReferralCodeUse->referral_code_id = $ReferralCode[0]->referral_code_id;
                            $ReferralCodeUse->used_by_id = $data->user_id;
                            $ReferralCodeUse->save();
                            if($data->user_type=='passenger'){

                                $data['referral_balance'] = (float)$ReferralCode[0]->referral_amount_used_by;
                                $this->updatePassengerReferralBalance($data);

                            }
                            if ($data->user_type=='driver') {

                                $data['referral_balance'] = (float)$ReferralCode[0]->referral_amount_used_by;
                                $this->updateDriverReferralBalance($data);

                            }
                            return ['message'=>'Successfully applied referral code.','data'=>(object)[],'errors'=>array('exception'=>['Everything is OK.'],'error'=>[]),'statusCode'=>200];

                        } else {
                            return ['message'=>'Already completed a ride. Referral code not usable.','data'=>(object)[],'errors'=>array('exception'=>['Bad Request.'],'error'=>[]),'statusCode'=>400];
                        }

                    } else {
                        return ['message'=>'Already used referral code.','data'=>(object)[],'errors'=>array('exception'=>['Bad Request.'],'error'=>[]),'statusCode'=>400];
                    }
                }

            } else {
                return ['message'=>'Invalid referral code.','data'=>(object)[],'errors'=>array('exception'=>['Bad Request.'],'error'=>[]),'statusCode'=>400];
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.profile_cannot_updated"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    public function accessUseReferralCode($data){
        return $this->useReferralCode($data);
    }

    public function accessInsertReferralCode($data){
        return $this->insertReferralCode($data);
    }

    public function accessGetReferralCodeDriver(){
        return $this->getReferralCodeDriver();
    }

    public function accessGetReferralCodePassenger(){
        return $this->getReferralCodePassenger();
    }
}