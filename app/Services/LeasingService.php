<?php

namespace App\Services;

use App\Model\Leasing\Leasing;

class LeasingService
{
    private function checkNull($field){
        if($field===null)
        return "";
        else
        return $field;
    }
    private function createRequest($data){
        $Leasing=new Leasing();
        $Leasing->passenger_id=$data->passenger_id;
        $Leasing->from_datetime=$data->from_datetime;
        $Leasing->to_datetime=$data->to_datetime;
        $Leasing->services_type_id=$data->services_type_id;
        $Leasing->address=$data->address;
        $Leasing->longitude=$data->longitude;
        $Leasing->latitude=$data->latitude;
        $Leasing->no_passenger=$data->no_passenger;
        $Leasing->details=$data->details;
        $Leasing->lease_type=$data->lease_type;
        $Leasing->dateOfJourny=$data->dateOfJourny;
        $Leasing->vehicle_equipment=$this->checkNull($data->vehicle_equipment);
        
        $Leasing->save();
        return $Leasing;
    }
    private function updateRequest($data){
        $Leasing=Leasing::where("leasing_id",$data->leasing_id)->first();
        //$Leasing->user_id=$data->user_id;
        $Leasing->from_datetime=$data->from_datetime;
        $Leasing->to_datetime=$data->to_datetime;
        $Leasing->services_type_id=$data->services_type_id;
        $Leasing->address=$data->address;
        $Leasing->longitude=$data->longitude;
        $Leasing->latitude=$data->latitude;
        $Leasing->no_passenger=$data->no_passenger;
        $Leasing->details=$data->details;
        $Leasing->lease_type=$data->lease_type;
        $Leasing->dateOfJourny=$data->dateOfJourny;
        $Leasing->vehicle_equipment=$this->checkNull($data->vehicle_equipment);
        $Leasing->save();
        return $Leasing;

    }
    private function getRequest($data){
        $leasing=Leasing::where("passenger_id",$data->passenger_id)->get()->toArray();
        return $leasing;
    }
    private function getOneRequest($data){
        $leasing=Leasing::where("passenger_id",$data->passenger_id)->where("leasing_id",$data->leasing_id)->first();
        return $leasing;
    }
    public function accessCreateRequest($data){
        return $this->createRequest($data);
    }
    public function accessUpdateRequest($data){
        return $this->updateRequest($data);
    }
    public function accessGetRequest($data){
        return $this->getRequest($data);
    }
    public function accessGetOneRequest($data){
        return $this->getOneRequest($data);
    }
}


